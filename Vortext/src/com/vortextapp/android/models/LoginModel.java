package com.vortextapp.android.models;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.models.tables.ContactType;
import com.vortextapp.android.models.tables.Contacts;
import com.vortextapp.android.models.tables.EarningsLog;
import com.vortextapp.android.models.tables.Messages;
import com.vortextapp.android.models.tables.Threads;
import com.vortextapp.android.models.tables.UserAccount;

/**
 * Sample Android UI activity which displays a text window when it is run.
 * 
 * <p>
 * <b>NOTE:</b> This does <i>not</i> extend the {@link OrmLiteBaseActivity} but instead manages the helper itself
 * locally using the {@link #databaseHelper} field, the {@link #getHelper()} private method, and the call to
 * {@link OpenHelperManager#releaseHelper()} inside of the {@link #onDestroy()} method.
 * </p>
 */
public class LoginModel {

	private final String TAG = "VorText: LoginModel";

	private DatabaseHelper databaseHelper = null;
	private Context context;

	public LoginModel(Context c){
		context = c;
		if(Globals.DEBUG) Log.v(TAG, "Constructed");
	}
	
	/** 
	 * release database helper when done
	 */
	public void close(){
		if (databaseHelper != null) {
			OpenHelperManager.releaseHelper();
			databaseHelper = null;
		}
	}

	/**
	 * You'll need this in your class to get the helper from the manager once per class.
	 */
	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
		}
		return databaseHelper;
	}

//////
	public Boolean checkIsLoggedIn() {
		
		Boolean result = null;

		UserAccount user_account = new UserAccount();
		user_account.setId(1);
		user_account.setIsLoggedIn(true);
		
		// get our database access object
		Dao<UserAccount, Integer> UserAccountDao;
		
		try {
			UserAccountDao = getHelper().getUserAccountDao();
			List<UserAccount> results = UserAccountDao.queryForMatching(user_account);
			
			if(results.isEmpty()==true){
				result = false;
				if(Globals.DEBUG) Log.v(TAG, "checkIsLoggedIn returns false");
			} else {
				result = true;
				if(Globals.DEBUG) Log.v(TAG, "checkIsLoggedIn returns true");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	//TODO need to update login status to 1 if account exists
	public Boolean login(String hash_id, String email){
		
		Boolean result = null;
		UserAccount does_acct_exist = doesAccountAlreadyExist(hash_id, email);
		
		if(does_acct_exist!=null){
			try{
				Dao<UserAccount, Integer> userAccountDao;
				userAccountDao = getHelper().getUserAccountDao();	
				does_acct_exist.setIsLoggedIn(true);
				int row_affected = userAccountDao.update(does_acct_exist);
				result = ((row_affected>0)?true:false);
				
				//result = updateToLoggedIn(hash_id, email);
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		} else {
			if(Globals.DEBUG) Log.v(TAG, "New account created");
			result = _login(hash_id, email);
		}
		
		return result;
		
	}
	
	private UserAccount doesAccountAlreadyExist(String hash_id, String email){
		UserAccount user_account = new UserAccount();
		Dao<UserAccount, Integer> userAccountDao;		
		try{
			userAccountDao = getHelper().getUserAccountDao();
			String sql = "select * from useraccount where hash_id = '"+hash_id+ "'";
			GenericRawResults<Object[]> rawResults = userAccountDao.queryRaw(sql, new DataType[] { DataType.INTEGER });
			for (Object[] resultArray : rawResults) {
				if(resultArray == null){
					
					if(Globals.DEBUG) Log.v(TAG, "this account does not exist yet");
					return  null;
				} else {
					
					if(Globals.DEBUG) Log.v(TAG, "this account already exists");
					user_account.setEmail((resultArray[1]== null) ? "0" :(String)resultArray[1] );
					Contacts contact = new Contacts();
					contact.setId(( resultArray[0] ==null) ? 0: (Integer) resultArray[0]);
					user_account.setContact(contact);
					user_account.setId(( resultArray[3] ==null) ? 0: (Integer) resultArray[0]);
					user_account.setHashId(hash_id);
					user_account.setIsLoggedIn(true);
					return user_account;
				}
			}
			
		
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		
		return null;
		

	}
	
	private Boolean updateToLoggedIn(String hash_id, String email){
		
		UserAccount user_account = new UserAccount();
		user_account.setId(1);
		user_account.setIsLoggedIn(true);
		int rows_affected = 0;
		
		try {
			Dao<UserAccount, Integer> userAccountDao = getHelper().getUserAccountDao();
			rows_affected = userAccountDao.update(user_account);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(rows_affected>0){
			return true;
		} else {
			return false;
		}
		
	}
	
	private Boolean _login(String hash_id, String email){
		
		Boolean result = null;

		try {
			
			// setup contact
			Contacts contact = new Contacts();
			
			TelephonyManager telephone_manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			String myphone_number = telephone_manager.getLine1Number();
			
			if(myphone_number!=null){
				contact.setPhone1(myphone_number);
			} else {
				contact.setPhone1("0");
			}
			
			//CellLocation cell_location = telephone_manager.getCellLocation();
			
			contact.setEmail1(email);
			contact.setFirstName("Me");
			contact.setServerHashId(hash_id);
			contact.setTypeId(ContactType.OWNER);
			
			// check to see if contact exists, if not, create a new one
			Dao<Contacts, Integer> contactDao = getHelper().getContactsDao();
			contactDao.createIfNotExists(contact);

			
			// check to see if this contact corresponds to a user account
			Dao<UserAccount, Integer> userAccountDao = getHelper().getUserAccountDao();
			UserAccount user_account = new UserAccount(hash_id, email, contact);
			userAccountDao.createIfNotExists(user_account);
			
			if(user_account.getId()>0){
				result = true;
			} else {
				result = false;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		} 
		
		return result;
	}
	
	public Boolean logout(){
		PreferencesModel prefs = PreferencesModel.getInstance();
		UserAccount user_account = new UserAccount();
		user_account.setHashId(prefs.getAccountHashId());
		
		//user_account.setIsLoggedIn(false);
		int rows_affected = 0;
		
		//List<UserAccount> account_list = new ArrayList<UserAccount>();
		
		Dao<UserAccount, Integer> userAccountDao;
		
		try{
			userAccountDao = getHelper().getUserAccountDao();
		String sql = "select * from useraccount where hash_id = '"+prefs.getAccountHashId()+ "'";
		GenericRawResults<Object[]> rawResults = userAccountDao.queryRaw(sql, new DataType[] { DataType.INTEGER });
		// page through the results 
		rows_affected = 0;
		for (Object[] resultArray : rawResults) {
			
			user_account.setEmail((resultArray[1]== null) ? "0" :(String)resultArray[1] );
			Contacts contact = new Contacts();
			contact.setId(( resultArray[0] ==null) ? 0: (Integer) resultArray[0]);
			user_account.setContact(contact);
			user_account.setId(( resultArray[3] ==null) ? 0: (Integer) resultArray[0]);
			user_account.setIsLoggedIn(false);
			rows_affected += userAccountDao.update(user_account);
			
		} 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		if(rows_affected>0){
			return true;
		} else {
			return false;
		}
		
	}
	
	public String getAccountHash(){
		
		String result = null;
		
		// get our database access object
		Dao<UserAccount, Integer> UserAccountDao;
		
		try {
			UserAccountDao = getHelper().getUserAccountDao();
			List<UserAccount> user_account = UserAccountDao.queryForAll(); // there should only be one row

			if(user_account.isEmpty()){
				result = "";
			} else {
				result = user_account.get(0).getHashId();
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		
		return result;
		
	}

	/**
	 * Do our sample database stuff as an example.
	 
	private void doSampleDatabaseStuff(String action, TextView tv) {
		try {
			// get our dao
			Dao<SimpleData, Integer> simpleDao = getHelper().getSimpleDataDao();
			// query for all of the data objects in the database
			List<SimpleData> list = simpleDao.queryForAll();
			// our string builder for building the content-view
			StringBuilder sb = new StringBuilder();
			sb.append("got ").append(list.size()).append(" entries in ").append(action).append("\n");

			// if we already have items in the database
			int simpleC = 0;
			for (SimpleData simple : list) {
				sb.append("------------------------------------------\n");
				sb.append("[").append(simpleC).append("] = ").append(simple).append("\n");
				simpleC++;
			}
			sb.append("------------------------------------------\n");
			for (SimpleData simple : list) {
				simpleDao.delete(simple);
				sb.append("deleted id ").append(simple.id).append("\n");
				if(Globals.DEBUG) Log.i(LOG_TAG, "deleting simple(" + simple.id + ")");
				simpleC++;
			}

			int createNum;
			do {
				createNum = new Random().nextInt(3) + 1;
			} while (createNum == list.size());
			for (int i = 0; i < createNum; i++) {
				// create a new simple object
				long millis = System.currentTimeMillis();
				SimpleData simple = new SimpleData(millis);
				// store it in the database
				simpleDao.create(simple);
				if(Globals.DEBUG) Log.i(LOG_TAG, "created simple(" + millis + ")");
				// output it
				sb.append("------------------------------------------\n");
				sb.append("created new entry #").append(i + 1).append(":\n");
				sb.append(simple).append("\n");
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					// ignore
				}
			}

			tv.setText(sb.toString());
			if(Globals.DEBUG) Log.i(LOG_TAG, "Done with page at " + System.currentTimeMillis());
		} catch (SQLException e) {
			if(Globals.DEBUG) Log.e(LOG_TAG, "Database exception", e);
			tv.setText("Database exeption: " + e);
			return;
		}
		
	}
	*/
}