package com.vortextapp.android.models;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.vortextapp.android.R;
import com.vortextapp.android.config.Database;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.models.tables.*;

/**
 * Database helper which creates and upgrades the database and provides the DAOs for the app.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	
	public static final String TAG = "VorText: DatabaseHelper";

	/************************************************/

	private Dao<AdActivity, Long> ad_activity;
	private Dao<AdCategories, Integer> ad_categories;
	private Dao<Ads, Long> ads;
	private Dao<CategoriesInContact, Integer> categories_in_contact;
	private Dao<Contacts, Integer> contacts;
	private Dao<EarningsLog, Long> earnings_log;
	private Dao<EarningsReport, Integer> earnings_report;
	private Dao<Folders, Integer> folders;
	private Dao<Messages, Long> messages;
	private Dao<Threads, Integer> threads;
	private Dao<UserAccount, Integer> user_account;

	public DatabaseHelper(Context context) {
		super(context, Database.NAME, null, Database.VERSION, R.raw.ormlite_config);
	}

	/************************************************/

	
	public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
		try {
			TableUtils.createTableIfNotExists(connectionSource, AdActivity.class);
			TableUtils.createTableIfNotExists(connectionSource, AdCategories.class);
			TableUtils.createTableIfNotExists(connectionSource, Ads.class);
			TableUtils.createTableIfNotExists(connectionSource, CategoriesInContact.class);
			TableUtils.createTableIfNotExists(connectionSource, Contacts.class);
			TableUtils.createTableIfNotExists(connectionSource, EarningsLog.class);
			TableUtils.createTableIfNotExists(connectionSource, EarningsReport.class);
			TableUtils.createTableIfNotExists(connectionSource, Folders.class);
			TableUtils.createTableIfNotExists(connectionSource, Messages.class);
			TableUtils.createTableIfNotExists(connectionSource, Threads.class);
			TableUtils.createTableIfNotExists(connectionSource, UserAccount.class);
			
			initAdCategoriesData();
			initFolderData();
			
		} catch (SQLException e) {
			if(Globals.DEBUG) Log.e(TAG, "Unable to create databases", e);
		}
	}

	
	public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
		try {
			//TODO need to write upgrade script
			//throw new SQLException("upgrade script is not defined yet");
			TableUtils.dropTable(connectionSource, AdActivity.class, true);
			TableUtils.dropTable(connectionSource, AdCategories.class, true);
			TableUtils.dropTable(connectionSource, Ads.class, true);
			TableUtils.dropTable(connectionSource, CategoriesInContact.class, true);
			TableUtils.dropTable(connectionSource, Contacts.class, true);
			TableUtils.dropTable(connectionSource, EarningsLog.class, true);
			TableUtils.dropTable(connectionSource, EarningsReport.class, true);
			TableUtils.dropTable(connectionSource, Folders.class, true);
			TableUtils.dropTable(connectionSource, Messages.class, true);
			TableUtils.dropTable(connectionSource, Threads.class, true);
			TableUtils.dropTable(connectionSource, UserAccount.class, true);
			onCreate(sqliteDatabase, connectionSource);
		} catch (SQLException e) {
			if(Globals.DEBUG) Log.e(DatabaseHelper.class.getName(), "Unable to upgrade database from version " + oldVer + " to new "
					+ newVer, e);
		}
	}

	private void initAdCategoriesData(){
		
		//TODO would be good to implement the following way for this: 
		/*final List<Account> accountsToInsert = new ArrayList<Account>();
		...
		accountDao.callBatchTasks(new Callable<Void>() {
		public Void call() throws Exception {
		for (Account account : accountsToInsert) {
		accountDao.create(account);
		}
		}
		});*/
		insertAdCategoryRow("Accessories");
		insertAdCategoryRow("Articles");
		insertAdCategoryRow("Art/Photo/Music");
		insertAdCategoryRow("Automotive");
		insertAdCategoryRow("Beauty");
		insertAdCategoryRow("Books/Media");
		insertAdCategoryRow("Business");
		insertAdCategoryRow("Buying and Selling");
		insertAdCategoryRow("Careers");
		insertAdCategoryRow("Clothing/Apparel");
		insertAdCategoryRow("Computer & Electronics");
		insertAdCategoryRow("Department Stores/Malls");
		insertAdCategoryRow("Education");
		insertAdCategoryRow("Entertainment");
		insertAdCategoryRow("Environmental");
		insertAdCategoryRow("Family");
		insertAdCategoryRow("Financial Services");
		insertAdCategoryRow("Food & Drinks");
		insertAdCategoryRow("Games & Toys");
		insertAdCategoryRow("Gifts & Flowers");
		insertAdCategoryRow("Health and Wellness");
		insertAdCategoryRow("Home & Garden");
		insertAdCategoryRow("Legal/Insurance");
		insertAdCategoryRow("Marketing");
		insertAdCategoryRow("Non-Profit");
		insertAdCategoryRow("Online Services");
		insertAdCategoryRow("Recreation & Leisure");
		insertAdCategoryRow("Seasonal");
		insertAdCategoryRow("Sports & Fitness");
		insertAdCategoryRow("Telecommunications");
		insertAdCategoryRow("Tools and Equipment");
		insertAdCategoryRow("Travel");
	}
	
	private void insertAdCategoryRow(String name){
		AdCategories ad_categories = new AdCategories(name);
		Dao<AdCategories, Integer> AdCategoriesDao;
		try {
			AdCategoriesDao = getAdCategoriesDao();
			AdCategoriesDao.create(ad_categories);
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			//
		}
	}
	
	private void initFolderData(){
		Folders folder = new Folders("default");
		Dao<Folders, Integer> folderDao;
		try {
			folderDao = getFoldersDao();
			folderDao.create(folder);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Dao<AdActivity, Long> getAdActivityDao() throws SQLException {
		if (ad_activity == null) {
			ad_activity = getDao(AdActivity.class);
		}
		return ad_activity;
	}
	
	public Dao<AdCategories, Integer> getAdCategoriesDao() throws SQLException {
		if (ad_categories == null) {
			ad_categories = getDao(AdCategories.class);
		}
		return ad_categories;
	}
	
	public Dao<Ads, Long> getAdsDao() throws SQLException {
		if (ads == null) {
			ads = getDao(Ads.class);
		}
		return ads;
	}
	
	public Dao<CategoriesInContact, Integer> getCategoriesInContactDao() throws SQLException {
		if (categories_in_contact == null) {
			categories_in_contact = getDao(CategoriesInContact.class);
		}
		return categories_in_contact;
	}
	
	
	
	public Dao<Contacts, Integer> getContactsDao() throws SQLException {
		if (contacts == null) {
			contacts = getDao(Contacts.class);
		}
		return contacts;
	}

	public Dao<EarningsLog, Long> getEarningsLogDao() throws SQLException {
		if (earnings_log == null) {
			earnings_log = getDao(EarningsLog.class);
		}
		return earnings_log;
	}

	public Dao<EarningsReport, Integer> getEarningsReportDao() throws SQLException {
		if (earnings_report == null) {
			earnings_report = getDao(EarningsReport.class);
		}
		return earnings_report;
	}

	public Dao<Folders, Integer> getFoldersDao() throws SQLException {
		if (folders == null) {
			folders = getDao(Folders.class);
		}
		return folders;
	}


	public Dao<Messages, Long> getMessagesDao() throws SQLException {
		if (messages == null) {
			messages = getDao(Messages.class);
		}
		return messages;
	}
	
	public Dao<Threads, Integer> getThreadsDao() throws SQLException {
		if (threads == null) {
			threads = getDao(Threads.class);
		}
		return threads;
	}

	public Dao<UserAccount, Integer> getUserAccountDao() throws SQLException {
		if (user_account == null) {
			user_account = getDao(UserAccount.class);
		}
		return user_account;
	}
	
}
