package com.vortextapp.android.models;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.query.OrderBy;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.tables.Ads;
import com.vortextapp.android.models.tables.EarningsLog;
import com.vortextapp.android.models.tables.EarningsReport;
import com.vortextapp.android.models.tables.Messages;

public class EarningsModel {

	private final String TAG = "VorText: EarningsModel";

	private DatabaseHelper databaseHelper = null;
	private Context context;
	private PreferencesModel prefs = PreferencesModel.getInstance();

	public EarningsModel(Context c){
		context = c;
		if(Globals.DEBUG) Log.v(TAG, "Constructed");
	}
	
	/** 
	 * release database helper when done
	 */
	public void close(){
		if (databaseHelper != null) {
			OpenHelperManager.releaseHelper();
			databaseHelper = null;
		}
	}

	/**
	 * You'll need this in your class to get the helper from the manager once per class.
	 */
	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
		}
		return databaseHelper;
	}
	
	public Boolean insertNew(EarningsLog cash){
		
		Boolean result = false;
		
		try {

			Dao<EarningsLog, Long> earningsDao = getHelper().getEarningsLogDao();
			
			int row_result = earningsDao.create(cash);
			
			if(row_result>0){
				if(Globals.DEBUG) Log.v(TAG, "inserted earnings");
				result = true;
			} else {
				result = false;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public Double getTotalEarnings(){

		Double amount = 0.00;
		String hash_id = prefs.getAccountHashId();
		try {
			
			String sql = "select sum("+EarningsLog.AMOUNT+") from "+EarningsLog.TABLE + " where hash_id = '"+hash_id+ "'";

			Dao<EarningsLog, Long> earningsDao = getHelper().getEarningsLogDao();			
			
			// return the orders with the sum of their amounts per account 
			GenericRawResults<Object[]> rawResults = earningsDao.queryRaw(sql, new DataType[] { DataType.DOUBLE });
			// page through the results 
			for (Object[] resultArray : rawResults) {
				amount = (Double) resultArray[0];
				System.out.println("total: " + resultArray[0]);
			} 
			rawResults.close();			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return amount;
		
	}
	
	public Double compileEarnings(){

		Double total = getTotalEarnings();

		try {

			Dao<EarningsReport, Integer> earningsReportDao = getHelper().getEarningsReportDao();
			EarningsReport report = new EarningsReport();
			
			Dao<EarningsLog, Long> earningsLogDao = getHelper().getEarningsLogDao();
			List<EarningsLog> earningsLog = earningsLogDao.queryForAll();
			
			// TODO need to implement transactions for this

			String compiled_ids = "";
			for(EarningsLog item: earningsLog){ 
				compiled_ids += item.getId().toString()+",";
			}
			report.setAdIdsClaimed(compiled_ids);
			
			report.setAmount(total);
			report.setDate(TimeManager.getCurrentSyncronizedTimeStamp());
			
			int row_result = earningsReportDao.create(report);

			if(row_result>0){
				//
				if(Globals.DEBUG) Log.v(TAG, "inserted earnings");
				// delete earnings
				DeleteBuilder<EarningsLog, Long> deleteBuilder = earningsLogDao.deleteBuilder();
				earningsLogDao.delete(deleteBuilder.prepare());
				
			} else {
				//
				if(Globals.DEBUG) Log.e(TAG, "did not insert earnings");
				total = 0d;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return total;
		
	}
	//@Hai
	//Add synced balance to the local database so that the earning button would display correctly
	public Boolean add_synced_balance(Double synced_balance){
		try {
			Dao<EarningsLog, Long> earningsLogDao = getHelper().getEarningsLogDao();
			EarningsLog earningsLog = new EarningsLog();
			List<EarningsLog> earningsLogList = earningsLogDao.queryBuilder().orderBy(EarningsLog.AD_ID,true).query();
			earningsLog.setAmount(synced_balance -  getTotalEarnings());
			earningsLog.setDate(TimeManager.getCurrentSyncronizedTimeStamp());
			Ads ad = new Ads();
/*			if(earningsLogList.size() != 0)
			{
				earningsLog = earningsLogList.get(0);
				ad = earningsLog.getAd();
				earningsLog.setId(earningsLog.getId()-1);
				if (ad!= null){
				//subtract the total of local amount from the total of all amount synced from server/
					//subtract by 1 so the record is unique
				
				ad.setId(earningsLog.getAd().getId()-1);}
				else{
					earningsLog.setAdId((long) (0-1));
				}
			}
			else{*/
				earningsLog.setId((long)(-1));
				ad.setId((long)-1);
			//}
				
			earningsLog.setAd(ad);
			earningsLog.setHashId(prefs.getAccountHashId());
			earningsLogDao.create(earningsLog);
			//earningsLog.AD_ID = "12";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}

}