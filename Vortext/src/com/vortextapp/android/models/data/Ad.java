package com.vortextapp.android.models.data;


public class Ad {
		
		private long id;
		private int advertiser_id;
		private String for_hash;
		private String message;
		private double reward;
		private String read_expires;
		public void setId(long id) {
			this.id = id;
		}
		public long getId() {
			return id;
		}
		public void setAdvertiserId(int advertiser_id) {
			this.advertiser_id = advertiser_id;
		}
		public int getAdvertiserId() {
			return advertiser_id;
		}
		public void setForHash(String for_hash) {
			this.for_hash = for_hash;
		}
		public String getForHash() {
			return for_hash;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public String getMessage() {
			return message;
		}
		public void setReward(double reward) {
			this.reward = reward;
		}
		public double getReward() {
			return reward;
		}
		public void setReadExpires(String read_expires) {
			this.read_expires = read_expires;
		}
		public String getReadExpires() {
			return read_expires;
		}

		
}

