package com.vortextapp.android.models.data;

/**
 * This should only be instantiated once by PreferenceModel
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class PreferencesDefaultData {
	
	private static final String TAG = "VorText: data.PreferenceDefaultData";
	
	private boolean notificationsEnabled;
    private String dayNoAds;
    
    private Context context;
    private SharedPreferences prefs;
    
    public PreferencesDefaultData(Context c){
    	context = c;
    	// Get the xml/preferences.xml preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        
        setNotificationsEnabled(prefs.getBoolean("notificationsEnabled", true));
        setDayNoAds(prefs.getString("displaySchedule", "7"));
        
    }

	private void setNotificationsEnabled(boolean notificationsEnabled) {
		this.notificationsEnabled = notificationsEnabled;
	}

	public boolean isNotificationsEnabled() {
		//@Hai
		Boolean  notificationsEnabled = prefs.getBoolean("notificationsEnabled", true);
		setNotificationsEnabled(notificationsEnabled);
		Log.i(TAG, "notificationsEnabled value: "+notificationsEnabled);
		return notificationsEnabled;
	}

	private void setDayNoAds(String dayNoAds) {
		this.dayNoAds = dayNoAds;
	}

	public String getDayNoAds() {
		setDayNoAds(prefs.getString("displaySchedule", "7"));
		return dayNoAds;
	}
    
    
   
}
