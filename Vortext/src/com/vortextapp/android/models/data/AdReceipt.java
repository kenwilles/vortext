package com.vortextapp.android.models.data;

@SuppressWarnings("unused")
public class AdReceipt {
	
	private String agent_hash_id;
	private String recipient_hash_id;
	private long ad_id;
	private int advertiser_id;
	private int ad_receipt_type;
	private long time_occurred;
	
	/**
	 * 
	 * @param agent_hash_id 
	 * @param recipient_hash_id is the agent_hash_id for now until we get the whole opt-in thing working
	 * @param ad_id
	 * @param advertiser_id
	 * @param ad_receipt_type use Globals constants to set
	 * @param time_occurred use the TimeManager to set
	 */
	public AdReceipt(String agent_hash_id, String recipient_hash_id, long ad_id, int advertiser_id, int ad_receipt_type, long time_occurred){
		this.agent_hash_id = agent_hash_id;
		this.recipient_hash_id = recipient_hash_id;
		this.ad_id = ad_id;
		this.advertiser_id = advertiser_id;
		setAdReceiptType(ad_receipt_type);
		setTimeOccurred(time_occurred);
	}
	
	private void setAdReceiptType(int ad_receipt_type) {
		this.ad_receipt_type = ad_receipt_type;
	}
	
	/**
	 * 
	 * @param time_occurred make sure it is in seconds since epoch, not milliseconds. PHP doesn't like storing timestamps in milliseconds
	 */
	//TODO need to check if milliseconds was set instead of seconds
	private void setTimeOccurred(long time_occurred) {
		this.time_occurred = time_occurred;
	}
	
}
