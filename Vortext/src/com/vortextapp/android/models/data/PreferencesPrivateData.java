package com.vortextapp.android.models.data;

/**
 * This should only be instantiated once by PreferenceModel
 */

import com.vortextapp.android.config.Globals;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

public class PreferencesPrivateData {
	
	private static final String TAG = "VorText: data.PreferencePrivateData";

	public static final String NAME = "private_prefs";
	
	public static final int NOTIFICATION_SINGLE_ID = 1;
    public static final String NOTIFICATION_ID = "notification_id";
    private int notification_id;
    
    public static final String NOTIFICATION_COUNT = "notification_count";
    private int notification_count;
	
	public static final String LOCATION_START_TIME = "time_marker";
    private long time_marker;
    
    public static final String LOGGED_IN = "logged_in";
    private boolean logged_in;
    
    public static final String FB_LOGGED_IN = "fb_logged_in";
    private boolean fb_logged_in;
    
    public static final String VERSION = "version";
    private String version;
    
    public static final String ACCOUNT_HASH_ID = "account_hash_id";
    private String account_hash_id;
    
    public static final String ACCOUNT_FACEBOOK_ID = "account_facebook_id";
    private String account_facebook_id;
    
    public static final String ACCOUNT_EMAIL = "account_email";
    private String account_email;
    
    public static final String FRESH_INSTALL = "fresh_install";
    private boolean fresh_install;

    public static final String BETA_NOTICE_SHOWN = "beta_notice_shown";
    private boolean beta_notice_shown;
    
    //@Hai
    public static final String ARE_RECEIPTS_SYNCED = "are_receipts_synced";
    private boolean are_receipts_synced;
    
    public static final String NEED_ADS_IMMEDIATELY = "need_ads_immediately";
    private boolean need_ads_immediately;
    
    public static final String ARE_CONTACT_PICTURE_SYNCED = "are_contact_pictures_synced";
    private boolean are_contact_pictures_synced;
    
    public static final String IS_CHECKING_EMAIL = "is_checking_email";
    private boolean is_checking_email;
    
    //****Hai****
    
    private Context context;
    private SharedPreferences prefs;
    
    public PreferencesPrivateData(Context c){

    	context = c;
    	prefs = context.getSharedPreferences(NAME, Activity.MODE_PRIVATE);
    	
    	setNotificationId(prefs.getInt(NOTIFICATION_ID, NOTIFICATION_SINGLE_ID));
    	setNotificationCount(prefs.getInt(NOTIFICATION_COUNT, 0));
    	setStartTime(prefs.getLong(LOCATION_START_TIME, 0));
    	setLoggedIn(prefs.getBoolean(LOGGED_IN, false));
    	setFBLoggedIn(prefs.getBoolean(FB_LOGGED_IN, false));
    	setAccountHashId(prefs.getString(ACCOUNT_HASH_ID, ""));
    	setAccountEmail(prefs.getString(ACCOUNT_EMAIL, ""));
    	setIsFreshInstall(prefs.getBoolean(FRESH_INSTALL, true));
    	
    	//@Hai
    	setBetaNoticeShown(prefs.getBoolean(BETA_NOTICE_SHOWN,false));
    	setAccountFacebookId(prefs.getString(ACCOUNT_FACEBOOK_ID, ""));
    	setAreReceiptsSynced(prefs.getBoolean(ARE_RECEIPTS_SYNCED,true));
    	setNeedAdsImmediately(prefs.getBoolean(NEED_ADS_IMMEDIATELY, true));
    	setAreContactPicturesSynced(prefs.getBoolean(ARE_CONTACT_PICTURE_SYNCED, false));
    	setIsCheckEmail(prefs.getBoolean(IS_CHECKING_EMAIL, false));
    	try {
			setVersion(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//****Hai****
    	
    	
    }

// Each incoming sms could get its own notification... but we don't like that... still here in case we want it again
//    public int getNextNotificationId() {
//
//        ++notification_id;
//        if (notification_id > 32765) {
//        	notification_id = 1;     // wrap around before it gets dangerous
//        }
//
//        saveNotificationId(notification_id);
//        return notification_id;
//
//    }

	private void setNotificationCount(int notification_count) {
		this.notification_count = notification_count;
		
	}
	
	public void incrementNotificationCount(){
		++notification_count;
		if (notification_count > 32765) {
			notification_count = 1;     // wrap around before it gets dangerous
        }
		saveNotificationCount(notification_count);
	}

	public int getNotificationCount() {
		return notification_count;
	}
	
	public void resetNotificationCount(){
		saveNotificationCount(0);
	}
	
	private void saveNotificationCount(int notification_id){
		setNotificationCount(notification_id);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(NOTIFICATION_COUNT, notification_count);
        editor.commit(); // later versions may use apply instead
	}

	private void setLoggedIn(boolean logged_in) {
		this.logged_in = logged_in;
	}
	
	public void saveLoggedInStatus(boolean logged_in_status){
		setLoggedIn(logged_in_status);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(LOGGED_IN, logged_in);
        editor.commit(); // later versions may use apply instead
        //@Hai double check
        if ( isLoggedIn() != logged_in_status)
        {
        	editor.putBoolean(LOGGED_IN, logged_in_status);
            editor.commit();
        }
        	
	}
	public boolean isLoggedIn() {
		//@Hai
		Boolean is_logged_in = prefs.getBoolean(LOGGED_IN, false);
		Log.i(TAG, "logged_in value: "+is_logged_in);
		return is_logged_in;
	}
	

	public boolean isCheckingEmail() {
		//@Hai
		Boolean is_checking_email = prefs.getBoolean(IS_CHECKING_EMAIL, false);
		Log.i(TAG, "IS_CHECKING_EMAIL value: "+is_checking_email);
		return is_checking_email;
	}
	
	
	
	
	private void setIsCheckEmail(boolean is_checking_email) {
		this.is_checking_email = is_checking_email;
	}
	
	public void saveIsCheckEmail(boolean is_checking_email){
		setIsCheckEmail(is_checking_email);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(IS_CHECKING_EMAIL, is_checking_email);
        editor.commit(); // later versions may use apply instead
        //@Hai double check
        if ( isCheckingEmail() != is_checking_email)
        {
        	editor.putBoolean(LOGGED_IN, is_checking_email);
            editor.commit();
        }
        	
	}


	
	
	
	
	private void setFBLoggedIn(boolean fb_logged_in) {
		this.fb_logged_in = fb_logged_in;
	}
	
	public void saveFBLoggedInStatus(boolean fb_logged_in_status){
		setFBLoggedIn(fb_logged_in_status);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(FB_LOGGED_IN, fb_logged_in);
        editor.commit(); // later versions may use apply instead
        //@Hai double check
        if ( isFBLoggedIn() != fb_logged_in_status)
        {
        	editor.putBoolean(LOGGED_IN, fb_logged_in_status);
            editor.commit();
        }
        	
	}
	public boolean isFBLoggedIn() {
		//@Hai
		Boolean is_fb_logged_in = prefs.getBoolean(FB_LOGGED_IN, false);
		Log.i(TAG, "fb_logged_in value: "+is_fb_logged_in);
		return is_fb_logged_in;
	}
	

	private void setNotificationId(int notification_id) {
		this.notification_id = notification_id;
	}
	
	public int getNotificationId(){
		return notification_id;
	}
	
	//	private void saveNotificationId(int notification_id){
	//		// Save the updated notificationId in SharedPreferences
	//        SharedPreferences.Editor editor = prefs.edit();
	//        editor.putInt(NOTIFICATION_ID, notification_id);
	//        editor.commit(); // later versions may use apply instead
	//        if(Globals.DEBUG) Log.d(TAG, "notification_id: " + notification_id);
	//	}

	private void setStartTime(long start_time) {
		this.time_marker = start_time;
	}

	public long getStartTime() {
		return time_marker;
	}
	
	public void saveStartTime(long new_time){
		// Save the updated time_started in SharedPreferences
		setStartTime(new_time);
        SharedPreferences.Editor editor = prefs.edit();
       	editor.putLong(LOCATION_START_TIME, time_marker);
        editor.commit(); // later versions may use apply instead
      
	}

	private void setAccountHashId(String account_hash_id) {
		this.account_hash_id = account_hash_id;
	}
	
	private void setAccountEmail(String email) {
		this.account_email = email;
	}
	
	public String getAccountEmail(){
		return this.account_email;
	}
	
	public void saveAccountEmail(String email) {
		setAccountEmail(email);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ACCOUNT_EMAIL, account_email);
        editor.commit(); // later versions may use apply instead
        
	}
	
	public void saveAccountHashId(String hash_id) {
		setAccountHashId(hash_id);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ACCOUNT_HASH_ID, account_hash_id);
        editor.commit(); // later versions may use apply instead
        //@Hai double check
        if ( getAccountHashId() != hash_id)
        {
        	editor.putString(ACCOUNT_HASH_ID, account_hash_id);
            editor.commit(); 
        }
	}
	private void setAccountFacebookId(String account_facebook_id) {
		this.account_facebook_id = account_facebook_id;
	}
	
	public void saveAccountFacebookId(String facebook_id) {
		setAccountFacebookId(facebook_id);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ACCOUNT_FACEBOOK_ID, account_facebook_id);
        editor.commit(); // later versions may use apply instead
        //@Hai double check
        if ( getAccountFacebookId() != facebook_id)
        {
        	editor.putString(ACCOUNT_FACEBOOK_ID, account_facebook_id);
            editor.commit(); 
        }
	}

	public String getAccountFacebookId() {
		return account_facebook_id;
	}
	
	public String getAccountHashId() {
		return account_hash_id;
	}

	private void setIsFreshInstall(boolean is_fresh_install) {
		this.fresh_install = is_fresh_install;
	}

	/**
	 * This should only be called once by the location service
	 * @return
	 */
	public boolean isFreshInstall() {
		//@Hai
		Boolean fresh_install = prefs.getBoolean(FRESH_INSTALL, true);
		Log.i(TAG, "fresh_install value: "+fresh_install);
		if(fresh_install==true){
			SharedPreferences.Editor editor = prefs.edit();
	        editor.putBoolean(FRESH_INSTALL, false);
	        editor.commit(); // later versions may use apply instead
	        this.fresh_install = false;
	        return true;
		} else {
			return false;
		}

	}
	
	private void setVersion(String current_version)
	{
		this.version = current_version;
		
	}
	
	public void saveVersion(String version_to_be_set)
	{
		setVersion(version_to_be_set);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(VERSION,version);
		editor.commit();
		//@Hai double check
        if ( !getVersion().equals(version_to_be_set) )
        {
    		editor.putString(VERSION,version_to_be_set);
    		editor.commit();
        }
		
	}
	
	public String getVersion()
	{
		String current_version = prefs.getString(VERSION, "0");
		this.version = current_version;
		return current_version;
		
	}
	
	private void setAreReceiptsSynced(boolean val) {
		this.are_receipts_synced = val;
		
	}
	
	public void saveAreReceiptsSynced(boolean val){
		setAreReceiptsSynced(val);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ARE_RECEIPTS_SYNCED, are_receipts_synced);
        editor.commit(); // later versions may use apply instead
        //@Hai double check
        if ( AreReceiptsSynced() != val)
        {
        	editor.putBoolean(ARE_RECEIPTS_SYNCED, val);
            editor.commit();
        }
	}

	public boolean AreReceiptsSynced() {
		//
		//@Hai
		boolean are_receipts_synced  = prefs.getBoolean(ARE_RECEIPTS_SYNCED,true);
		Log.i(TAG, "are_receipts_synced value: "+are_receipts_synced);
		return are_receipts_synced;
		//return are_receipts_synced;
	}
	
	private void setNeedAdsImmediately(boolean need_ads){
		this.need_ads_immediately = need_ads;
		
	}
	
	public void saveNeedAdsImmediately(boolean need_ads){
		setNeedAdsImmediately(need_ads);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(NEED_ADS_IMMEDIATELY, need_ads_immediately);
        editor.commit(); // later versions may use apply instead
        //@Hai double check
        if ( needAdsImmediately() != need_ads){
        	editor.putBoolean(NEED_ADS_IMMEDIATELY, need_ads);
            editor.commit();
        }
	}
	
	public boolean needAdsImmediately(){
		//@Hai
		Boolean need_ads_immediately = prefs.getBoolean(NEED_ADS_IMMEDIATELY, true);
		Log.i(TAG, "need_ads_immediately value: "+need_ads_immediately);
		return need_ads_immediately;
	}

	private void setBetaNoticeShown(boolean beta_notice_shown) {
		this.beta_notice_shown = beta_notice_shown;
	}
	
	public void saveBetaNoticeShown(boolean notice_shown){
		setBetaNoticeShown(notice_shown);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(BETA_NOTICE_SHOWN, beta_notice_shown);
        editor.commit(); // later versions may use apply instead
        //@Hai Double Check
        //TODO: Need to fix this so that double checking is eliminated 
        if (hasBetaNoticeShown() != beta_notice_shown)
        {
        	editor.putBoolean(BETA_NOTICE_SHOWN, notice_shown);
            editor.commit(); // later versions may use apply instead
        }
        	
	}

	public boolean hasBetaNoticeShown() {
		Boolean beta_notice_shown = prefs.getBoolean(BETA_NOTICE_SHOWN,false);
		Log.i(TAG, "beta_notice_shown value: "+beta_notice_shown);
		return beta_notice_shown;
	}
	
	//*******Hai********
	
	public boolean areContactPicturesSynced(){
		Boolean are_contact_pictures_synced = prefs.getBoolean(ARE_CONTACT_PICTURE_SYNCED, false);
		Log.i(TAG, "are_contact_pictures_synced value: "+are_contact_pictures_synced);
		return are_contact_pictures_synced;
	}
	private void setAreContactPicturesSynced(boolean pictures_synced){
		this.are_contact_pictures_synced = pictures_synced;
	}
	
	
	public void saveAreContactPicturesSynced(boolean pictures_synced){
		setAreContactPicturesSynced(pictures_synced);
		SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(ARE_CONTACT_PICTURE_SYNCED, are_contact_pictures_synced);
        editor.commit(); // later versions may use apply instead
       //@Hai double check
        if (areContactPicturesSynced() != pictures_synced)
        {
        	editor.putBoolean(ARE_CONTACT_PICTURE_SYNCED, pictures_synced);
            editor.commit();
        }
	}
/*
	public void setFirst_notice_shown(boolean first_notice_shown) {
		this.first_notice_shown = first_notice_shown;
	}

	public boolean isFirst_notice_shown() {
		return first_notice_shown;
	}
*/   

}
