package com.vortextapp.android.models;

/**
 * There are two preferences, default and private. This class gives access to the data behind both
 */

import com.vortextapp.android.VorTextApp;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.data.PreferencesDefaultData;
import com.vortextapp.android.models.data.PreferencesPrivateData;
import android.content.Context;
import android.util.Log;

public class PreferencesModel {
	
	private static final String TAG = "VorText: models.PreferenceModel";
	
	private PreferencesPrivateData privateData;
	private PreferencesDefaultData defaultData;
    private Context context;
    
    private static PreferencesModel singletonPref;

	private PreferencesModel(Context c) {
		
		if(Globals.DEBUG) Log.i(TAG, "Instantiated once");

		context = c;
    	
    	privateData = new PreferencesPrivateData(c);
    	defaultData = new PreferencesDefaultData(c);
    	
	}
	
	public static synchronized PreferencesModel getInstance() {
		if (singletonPref == null) {
			singletonPref = new PreferencesModel(VorTextApp.getApp().getBaseContext());
		}
		return singletonPref;
	}
	
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
	
	//	public int getNextNotificationId(){
	//		return privateData.getNextNotificationId();
	//	}
	
	public int getNotificationId(){
		return privateData.getNotificationId();
	}
	
	public synchronized boolean AreReceiptsSynced(){
		return privateData.AreReceiptsSynced();
	}
	
	public synchronized void setAreReceiptsSynced(boolean are_receipts_synced){
		privateData.saveAreReceiptsSynced(are_receipts_synced);
	}
	
	/**
	 * incrementNotificationCount and getNotificationCount go together usually.
	 */
	public synchronized void incrementNotificationCount(){
		privateData.incrementNotificationCount();
	}
	public int getNotificationCount(){
		return privateData.getNotificationCount();
	}
	
	public synchronized void resetNotificationCount(){
		privateData.resetNotificationCount();
	}

	public boolean areNotificationsEnabled() {
		return defaultData.isNotificationsEnabled();
	}
	
	public boolean areAdsEnabled(){
		String weekday = String.valueOf(TimeManager.getCurrentDay());
		String day_for_no_ads = defaultData.getDayNoAds();
		if(weekday.equals(day_for_no_ads)==true){
			return false;
		} else {
			return true;
		}
	}
	
	public boolean isLoggedIn(){
		return privateData.isLoggedIn();
	}
	
	public boolean isFBLoggedIn(){
		return privateData.isFBLoggedIn();
	}
	
	public void saveAsLoggedIn(String hash_id, String email){
		saveAccountHashId(hash_id);
		saveAccountEmail(email);
		privateData.saveLoggedInStatus(true);
	}
	
	public void saveIsCheckingEmail(Boolean is_check_email){
		privateData.saveIsCheckEmail(is_check_email);
		}
	
	public boolean isCheckingEmail(){
		return privateData.isCheckingEmail();
	}
	
	public void saveAsFBLoggedIn(String facebook_id, String email){
		saveAccountFacebookId(facebook_id);
		saveAccountEmail(email);
		privateData.saveFBLoggedInStatus(true);
	}
	public void setVersion(String version_to_set)
	{
		privateData.saveVersion(version_to_set);
	}
	public void saveAsLoggedOut(){
		privateData.saveLoggedInStatus(false);
		privateData.saveFBLoggedInStatus(false);
	}
	
	private void saveAccountHashId(String hash_id){
		privateData.saveAccountHashId(hash_id);
	}
	
	public String getAccountHashId(){
		return privateData.getAccountHashId();
	}
	
	public String getAccountFacebookId(){
		return privateData.getAccountFacebookId();
	}
	
	public void saveAccountEmail(String email){
		privateData.saveAccountEmail(email);
	}
	
	public void saveAccountFacebookId(String facebook_id){
		privateData.saveAccountFacebookId(facebook_id);
	}
	
	public String getAccountEmail(){
		return privateData.getAccountEmail();
	}
	
	/**
	 * marks in milliseconds
	 */
	public synchronized void markCurrentTime(){
		privateData.saveStartTime(TimeManager.getCurrentSyncronizedTimeStamp());
	}
	
	/**
	 * gets time that was marked in milliseconds
	 * @return
	 */
	public synchronized long getMarkedTime(){
		return privateData.getStartTime();
	}
	
	public Boolean isFreshInstall(){
		return privateData.isFreshInstall();
	}

	public boolean needAdsImmediately(){
		return privateData.needAdsImmediately();
	}

    public synchronized void setNeedAdsImmediately(boolean need_ads){
    	privateData.saveNeedAdsImmediately(need_ads);
    }

    public boolean hasBetaNoticeShown() {
    	return privateData.hasBetaNoticeShown();
    }
    
    public synchronized void setBetaNoticeShown(boolean is_showing){
    	privateData.saveBetaNoticeShown(is_showing);
    }
    
    public boolean areContactPicturesSynced(){
    	return privateData.areContactPicturesSynced();
    }
    
    public synchronized void setAreContactPicturesSynced(boolean pictures_synced){
    	privateData.saveAreContactPicturesSynced(pictures_synced);
    }

}
