package com.vortextapp.android.models;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.vortextapp.android.VorTextApp;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.tables.AdActivity;
import com.vortextapp.android.models.tables.AdActivityTypes;
import com.vortextapp.android.models.tables.Ads;
import com.vortextapp.android.models.data.AdReceipt;
import com.vortextapp.android.models.data.Ad;

public class AdsModel {

	private final String TAG = "VorText: AdsModel";

	private DatabaseHelper databaseHelper = null;
	private Context context;
	private PreferencesModel prefs = PreferencesModel.getInstance();

	public AdsModel(Context c){
		context = c;
		if(Globals.DEBUG) Log.v(TAG, "Constructed");
	}
	
	/** 
	 * release database helper when done
	 */
	public void close(){
		if (databaseHelper != null) {
			OpenHelperManager.releaseHelper();
			databaseHelper = null;
		}
	}

	/**
	 * You'll need this in your class to get the helper from the manager once per class.
	 */
	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
		}
		return databaseHelper;
	}
	
	public void recordAdActivity(AdActivity ad_activity){
		try {
			Dao<AdActivity, Long> adActivityDao = getHelper().getAdActivityDao();
			adActivityDao.create(ad_activity);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<AdActivity> getUnsynchronizedActivities(){

		List<AdActivity> ad_activities = new ArrayList<AdActivity>();

		try {
			Dao<AdActivity, Long> adActivityDao = getHelper().getAdActivityDao();
			ad_activities = adActivityDao.queryForEq(AdActivity.IS_POSTED_TO_SERVER, false);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		
		return ad_activities;
		
	}
	
	public boolean setActivitiesToSynchronized(List<AdActivity> ad_activities){
		
		//List<Long> ids = new ArrayList<Long>();
		//for(AdActivity activity : ad_activities){
			//ids.add(activity.getId());
		//}
		
		Dao<AdActivity, Long> adActivityDao;
		boolean result = false;
		try {
			adActivityDao = getHelper().getAdActivityDao();
			for(AdActivity activity : ad_activities){
				activity.setPostedToServer(true);
				adActivityDao.update(activity);
			}
			result = true;
			/*UpdateBuilder<AdActivity, Long> updateBuilder = adActivityDao.updateBuilder();
			// update the password to be "none"
			updateBuilder.updateColumnValue(AdActivity.IS_POSTED_TO_SERVER, true);
			// only update the rows where password is null
			updateBuilder.where().in(AdActivity.ID, ids);
			adActivityDao.update(updateBuilder.prepare());*/
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		}
		
		return result;
		
	}
	//need to do one for hash_id based to deal with multiuser
	public List<Ads> getUnreadAds(){
		Ads ad = new Ads();
		ad.setIsEngaged(false);
		Dao<Ads, Long> adsDao;
		List<Ads> ad_list = new ArrayList<Ads>();
		
		try {
			adsDao = getHelper().getAdsDao();
			
			QueryBuilder<Ads, Long> queryBuilder = adsDao.queryBuilder();
			// get the WHERE object to build our query 
			queryBuilder.where().gt(Ads.READ_EXPIRES, TimeManager.getOneHourAgo()).and().eq(Ads.IS_ENGAGED, false); 
			ad_list = queryBuilder.query();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ad_list;
		
	}
	
	//@Hai: this one is for dealing with multiuser
	public List<Ads> getUnreadAdsForHashId(String hash_id){
		Ads ad = new Ads();
		ad.setIsEngaged(false);
		Dao<Ads, Long> adsDao;
		List<Ads> ad_list = new ArrayList<Ads>();
		
		try {
			adsDao = getHelper().getAdsDao();
			
			QueryBuilder<Ads, Long> queryBuilder = adsDao.queryBuilder();
			// get the WHERE object to build our query 
			queryBuilder.where().gt(Ads.READ_EXPIRES, TimeManager.getOneHourAgo()).and().eq(Ads.IS_ENGAGED, false).and().eq(Ads.FOR_HASH_ID, hash_id); 
			ad_list = queryBuilder.query();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ad_list;
		
	}
	
	public long getUnreadAdsNumber(){
		Ads ad = new Ads();
		ad.setIsEngaged(false);
		Dao<Ads, Long> adsDao;
		long count = 0;
		
		try {
			adsDao = getHelper().getAdsDao();
			//adsDao.countOf();
			QueryBuilder<Ads, Long> queryBuilder = adsDao.queryBuilder();
			// get the WHERE object to build our query 
			queryBuilder.where().gt(Ads.READ_EXPIRES, TimeManager.getOneHourAgo()).and().eq(Ads.IS_ENGAGED, false); 
			//adsDao.queryForEq(Ads.IS_ENGAGED, true);
			//queryBuilder.query();
			queryBuilder.setCountOf(true);
			count = adsDao.countOf(queryBuilder.prepare());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return count;
	}
	//@Hai: for multiuser
	public long getUnreadAdsNumberForHashId(String hash_id){
		Ads ad = new Ads();
		ad.setIsEngaged(false);
		Dao<Ads, Long> adsDao;
		long count = 0;
		
		try {
			adsDao = getHelper().getAdsDao();
			//adsDao.countOf();
			QueryBuilder<Ads, Long> queryBuilder = adsDao.queryBuilder();
			// get the WHERE object to build our query 
			queryBuilder.where().gt(Ads.READ_EXPIRES, TimeManager.getOneHourAgo()).and().eq(Ads.IS_ENGAGED, false).and().eq(Ads.FOR_HASH_ID, hash_id);
			//adsDao.queryForEq(Ads.IS_ENGAGED, true);
			//queryBuilder.query();
			queryBuilder.setCountOf(true);
			count = adsDao.countOf(queryBuilder.prepare());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return count;
	}
	
	//@Hai
	public long getReadAdsNumber(){
		Ads ad = new Ads();
		ad.setIsEngaged(false);
		Dao<Ads, Long> adsDao;
		long count = 0;
		
		try {
			adsDao = getHelper().getAdsDao();
			//adsDao.countOf();
			QueryBuilder<Ads, Long> queryBuilder = adsDao.queryBuilder();
			// get the WHERE object to build our query 
			queryBuilder.where().gt(Ads.READ_EXPIRES, TimeManager.getOneHourAgo()).and().eq(Ads.IS_ENGAGED, true); 
			//adsDao.queryForEq(Ads.IS_ENGAGED, true);
			//queryBuilder.query();
			queryBuilder.setCountOf(true);
			count = adsDao.countOf(queryBuilder.prepare());
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return count;
	}
	
	//@Hai for multiuser
		public long getReadAdsNumberForHashId(String hash_id){
			Ads ad = new Ads();
			ad.setIsEngaged(false);
			Dao<Ads, Long> adsDao;
			long count = 0;
			
			try {
				adsDao = getHelper().getAdsDao();
				//adsDao.countOf();
				QueryBuilder<Ads, Long> queryBuilder = adsDao.queryBuilder();
				// get the WHERE object to build our query 
				queryBuilder.where().gt(Ads.READ_EXPIRES, TimeManager.getOneHourAgo()).and().eq(Ads.IS_ENGAGED, true).and().eq(Ads.FOR_HASH_ID, hash_id);
				//adsDao.queryForEq(Ads.IS_ENGAGED, true);
				//queryBuilder.query();
				queryBuilder.setCountOf(true);
				count = adsDao.countOf(queryBuilder.prepare());
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return count;
		}
	
	public void clearExpired(){
		int rows_affected = 0;
		Ads ad = new Ads();
		ad.setIsEngaged(false);
		Dao<Ads, Long> adsDao;
		DeleteBuilder<Ads, Long> deleteBuilder;
		
		try {
			
			adsDao = getHelper().getAdsDao();
			deleteBuilder = adsDao.deleteBuilder();
			// only delete the rows where password is null 
			deleteBuilder.where().lt(Ads.READ_EXPIRES, TimeManager.getOneHourAgo()).and().eq(Ads.IS_ENGAGED, false);
			rows_affected = adsDao.delete(deleteBuilder.prepare());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(rows_affected>0){
			if(Globals.DEBUG) Log.v(TAG, rows_affected+" ads deleted because they were expired");
		} else {
			if(Globals.DEBUG) Log.w(TAG, "no ads were deleted");
		}

	}
	
	/*
	public void removeExpiredAd(Ads ad){
		Dao<Ads, Long> adsDao;
		try {
			adsDao = getHelper().getAdsDao();
			int affected_row = adsDao.delete(ad);
			if(Globals.DEBUG) Log.v(TAG, "expired ad was removed, affected rows "+affected_row+", ad that was removed: "+ad.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(Globals.DEBUG) Log.e(TAG, "expired ad could not be removed");
			e.printStackTrace();
			
		}
		
	}
	*/
	
	public void markAdRead(Ads ad){
		Dao<Ads, Long> adsDao;
		try {
			adsDao = getHelper().getAdsDao();
			ad.setIsEngaged(true);
			int affected_row = adsDao.update(ad);
			if(Globals.DEBUG) Log.v(TAG, "ad was marked as read, affected rows "+affected_row+", ad that was read: "+ad.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			if(Globals.DEBUG) Log.e(TAG, "ad could not be marked as read");
			e.printStackTrace();
			
		}
	}
	
	public Boolean insertNewAds(List<Ad> ads){
		
		Boolean result = false;
		
		try {
			Dao<Ads, Long> adDao = getHelper().getAdsDao();
			int row_result = 1;
			
			for (Ad ad : ads) {
				
				Ads new_ad = new Ads();
				
				new_ad.setServerAdId(ad.getId());
				new_ad.setAdvertiserId(ad.getAdvertiserId());
				new_ad.setForHashId(ad.getForHash());
				new_ad.setMessage(ad.getMessage());
				new_ad.setReward(ad.getReward());
				new_ad.setIsEngaged(false);
				
				long expires = TimeManager.convertServerTimeToTimestamp(ad.getReadExpires());
				new_ad.setReadExpires(expires);
				long delivery_time = TimeManager.getCurrentSyncronizedTimeStamp();
				new_ad.setTimeDelivered(delivery_time);				
				if(Globals.DEBUG) Log.v(TAG, "expires:"+expires+", delivery_time:"+delivery_time+", milliseconds left:"+(expires-delivery_time));
				row_result = adDao.create(new_ad);
				
				if(row_result>0){

					//TODO need to get agent_hash_id when we have opt-in process working
					AdActivity ad_activity = new AdActivity(new_ad, new_ad.getServerAdId(), AdActivityTypes.CACHED, 
														TimeManager.getCurrentSyncronizedTimeStamp(), false);
					recordAdActivity(ad_activity);
					prefs.setAreReceiptsSynced(false);
					
					result = true;
					
				} else {
					result = false;
				}
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	private void breathe(){
		int milliseconds = 5;
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}