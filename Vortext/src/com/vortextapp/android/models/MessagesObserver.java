package com.vortextapp.android.models;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.util.Log;

import com.vortextapp.android.VorTextApp;
import com.vortextapp.android.config.Globals;

public class MessagesObserver extends Observable {
	
	public static final String TAG = "VorText: MessagesObserver";
	private static MessagesObserver singletonMessageObserver;
	private Context context;

	private MessagesObserver(Context c) {
		
		if(Globals.DEBUG) Log.i(TAG, "Instantiated once");

		context = c;
    	
	}
	
	public static synchronized MessagesObserver getInstance() {
		if (singletonMessageObserver == null) {
			singletonMessageObserver = new MessagesObserver(VorTextApp.getApp().getBaseContext());
		}
		return singletonMessageObserver;
	}
	
	public void startNotify(){
		if(super.countObservers()>0){
			super.setChanged();
			super.notifyObservers();
		} else {
			if(Globals.DEBUG) Log.e(TAG, "There are no content observers for this object");
		}
	}
	
	public void addObserver(Observer o){
		super.addObserver(o);
		if(Globals.DEBUG) Log.i(TAG, "Observer added, name: "+o.getClass().getName());
	}
	
}
