package com.vortextapp.android.models;

public final class SampleAds {
	public static final String collection[] = { 
		"*Hungry? Make it a Domino's night! Ooltewah 423-396-4444", 
		"*Artisan Pizzas only $7.99 at Collegedale Dominos!", 
		"*Take a study break! Order Domino's Pizza. 423-396-4444", 
		"*Get in Shape! Earn money! P90X2, Insanity isaacws.com", 
		"*$2.00 off purchase of $10.00 or more at El Matador!", 
		"*$3.00 off purchase of $20.00 or more at El Matador!", 
		"*Get paid $25 a month to TXT! http://bit.ly/pWFC2i", 
		"*Get paid while texting-Download VorText! Vortextapp.com", 
		"*Unique Marketing Branding through Sound RMDSsound.biz", 
		"*show production, sound, pyro, lighting RMDSsound.biz"
	};
	
	public static final String getRandom(){
	    int selected = (int) Math.floor(Math.random()*SampleAds.collection.length);
		return SampleAds.collection[selected];
	}
}
