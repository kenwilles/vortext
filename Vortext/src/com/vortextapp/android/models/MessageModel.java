package com.vortextapp.android.models;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import providers.ContactsOnPhone;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import com.vortextapp.android.VorTextApp;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.data.AdReceipt;
import com.vortextapp.android.models.tables.AdActivity;
import com.vortextapp.android.models.tables.AdActivityTypes;
import com.vortextapp.android.models.tables.Ads;
import com.vortextapp.android.models.tables.Contacts;
import com.vortextapp.android.models.tables.EarningsLog;
import com.vortextapp.android.models.tables.Messages;
import com.vortextapp.android.models.tables.Threads;

public class MessageModel {

	private final String TAG = "VorText: MessageModel";

	private DatabaseHelper databaseHelper = null;
	private Context context;
	private PreferencesModel prefs = PreferencesModel.getInstance();
	private MessagesObserver messagesObserver;
	private static MessageModel singletonMessageModel;
	
	public List<Messages> list;

	private MessageModel(Context c){
		context = c;
		list = new ArrayList<Messages>();
		if(Globals.DEBUG) Log.v(TAG, "Constructed");
		prefs = PreferencesModel.getInstance();
		messagesObserver = MessagesObserver.getInstance();
	}
	
	public static synchronized MessageModel getInstance() {
		if (singletonMessageModel == null) {
			singletonMessageModel = new MessageModel(VorTextApp.getApp().getBaseContext());
		}
		return singletonMessageModel;
	}
	
	
	
	/** 
	 * release database helper when done
	 */
	public void close(){
		if (databaseHelper != null) {
			OpenHelperManager.releaseHelper();
			databaseHelper = null;
		}
	}

	/**
	 * You'll need this in your class to get the helper from the manager once per class.
	 */
	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
		}
		return databaseHelper;
	}
	
	/**
	 * This will be called when a new text message gets created
	 * @param phone_number
	 * @param txt
	 * @param is_read
	 * @return Boolean
	 */
	public Boolean insertNewIncoming(String phone_number, String txt, Boolean is_read,Boolean is_sent){

		Boolean result = null;
		
		try {
			
			// get name from phone number
			ContactsOnPhone person = new ContactsOnPhone(context.getContentResolver());
			String person_name = person.getContactName(phone_number);
			
			// create contact if one doesn't exist like this
			// TODO need to accommidate more than one phone number for the same name (unlikely but could happen)
			Contacts contact = new Contacts(phone_number, person_name);
			Dao<Contacts, Integer> contactsDao = getHelper().getContactsDao();
			List<Contacts> contacts_result_list = contactsDao.queryForMatching(contact);
			if(contacts_result_list.isEmpty()){
				contact.setDateCreated();
				contact.setDateModified();
				contactsDao.create(contact);
			} else {
				contact = contacts_result_list.get(0);
			}
			
			// see if there is a conversation thread with this contact person, if not, create one.
			Threads threads = new Threads(contact);
			Dao<Threads, Integer> threadsDao = getHelper().getThreadsDao();
			List<Threads> threads_result_list = threadsDao.queryForMatching(threads);
			if(threads_result_list.isEmpty()){
				threads.setDateCreated();
				threadsDao.create(threads);
			} else {
				threads = threads_result_list.get(0);
			}
			
			Messages message;
			//Ads ads;
			
			if(is_read==false){
				message = new Messages(contact, threads, txt, is_read,"Recieved");
				message.setDate();
			} else {
				// set ad to display with message and mark that ad has been consumed
				// TODO implement with read status
				throw new Error("haven't implemented what to do if a new message is created with read status");
			}

			Dao<Messages, Long> messagesDao = getHelper().getMessagesDao();
			messagesDao.create(message);
			
			if(message.getId()>0){
				result = true;
				
				list.add(message);
				messagesObserver.startNotify();
				
			} else {
				result = false;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(result==null){
				result=false;
			}
		}
		
		return result;
		
	}
	
	/**
	 * This will be called when a new text message gets created
	 * @param phone_number
	 * @param txt
	 * @return Long
	 */
	public Long insertNewOutgoing(String to_phone_number, String txt){

		//Boolean result = null;
		Long message_ID = null;
		
		try {
			
			// get name from phone number
			ContactsOnPhone person = new ContactsOnPhone(context.getContentResolver());
			String person_name = person.getContactName(to_phone_number);
			
			// create contact if one doesn't exist like this
			// TODO need to accommidate more than one phone number for the same name (unlikely but could happen)
			Contacts contact = new Contacts(to_phone_number, person_name);
			Dao<Contacts, Integer> contactsDao = getHelper().getContactsDao();
			List<Contacts> contacts_result_list = contactsDao.queryForMatching(contact);
			if(contacts_result_list.isEmpty()){
				contact.setDateCreated();
				contact.setDateModified();
				contactsDao.create(contact);
			} else {
				contact = contacts_result_list.get(0);
			}

			// see if there is a conversation thread with this contact person, if not, create one.
			Threads threads = new Threads(contact);
			Dao<Threads, Integer> threadsDao = getHelper().getThreadsDao();
			List<Threads> threads_result_list = threadsDao.queryForMatching(threads);
			if(threads_result_list.isEmpty()){
				threads.setDateCreated();
				threadsDao.create(threads);
			} else {
				threads = threads_result_list.get(0);
			}
			
			Messages message;
			//Ads ads;
			
			Contacts me = new Contacts();
			me = contactsDao.queryForId(1);			
			message = new Messages(me, threads, txt, true,"Sending");
			message.setDate();

			Dao<Messages, Long> messagesDao = getHelper().getMessagesDao();
			messagesDao.create(message);
			message_ID = message.getId();
			if(message_ID>0){
				list.add(message);
					//messagesObserver.startNotify();
				
			} 

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
//			finally {
//			if(result==null){
//				result=false;
//			}
//		}
		
		return message_ID;
		
	}
	//@Hai
	/** 
	 * This one is for updating the contact image
	 * @param image_url, phone_number
	 * @return 0 : no contact found
	 * 1: successfully updated
	 * 2: same url as existed one nothing changed
	 * 3: sql error
	 */	
	
	public int updateContactImage(Uri image_url,String to_phone_number,boolean keep_old_image){
		if(image_url !=null)
		{
		try {
			
			
			// find matching contact entry 
			Contacts contact = new Contacts(to_phone_number);
			Dao<Contacts, Integer> contactsDao = getHelper().getContactsDao();
			List<Contacts> contacts_result_list = contactsDao.queryForMatching(contact);
			if(contacts_result_list.isEmpty()){
				return 0; //No contacts found
			} else {
				contact = contacts_result_list.get(0);
			}
			String current_image_url = contact.getPicture();
			if (current_image_url != null)
			{
				if(keep_old_image){
					return 2;
				}
				if ( current_image_url.equals(image_url.toString())){
					return 2;//Same url as the current one
				}
			}
			 //Needs to update image url
			contact.setPicture(image_url.toString());
			contactsDao.update(contact);
			return 1;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		}
		//SQL exception
		return 3;
		
	}
	/** 
	 * This one is for and individual conversation thread
	 * @param thread_id
	 * @return
	 */
	public void setThreadMessages(Threads thread){
		
		List<Messages> message_list = new ArrayList<Messages>();
		//List<Messages> message_list_formatted = new ArrayList<Messages>();

		Messages messages = new Messages();
		messages.setIsRead(null);
		messages.setThreads(thread);
		
		Dao<Messages, Long> messagesDao;
		
		AdsModel ads = new AdsModel(context);
		EarningsModel earn = new EarningsModel(context);
		String my_hash_id = prefs.getAccountHashId();
		List<Ads> ad_list;
		if((my_hash_id != null) && (!my_hash_id.equals("0")) && (!my_hash_id.equals(""))){
			ad_list = ads.getUnreadAdsForHashId(my_hash_id);
		}else{
			
			//incase hash_id is not found
			ad_list = ads.getUnreadAds();
		}
		Iterator<Ads> ad_itr = ad_list.iterator();
		
		if(ad_list.size()==0){
			prefs.setNeedAdsImmediately(true);
		}
		
		boolean are_new_ads_viewed = false;

		try {

			messagesDao = getHelper().getMessagesDao();
			message_list = messagesDao.queryForMatching(messages);
			ListIterator<Messages> message_itr = message_list.listIterator();
			
			while(message_itr.hasNext()){

				Messages m = message_itr.next();
				
				if(m.getIsRead()==false){
					m.setIsRead(true);
					
					// get ad if there is one and ...
					// + stick it in message 
					// + record earning 
					// + create receipt
					if(ad_itr.hasNext()==true && prefs.areAdsEnabled()==true){

						Ads a = ad_itr.next();

						m.setAds(a);
						m.setAdText(a.getMessage());
						
						// indicate that there are new ads that have been viewed. 
						are_new_ads_viewed = true;
						ads.markAdRead(a); // update ad							
						
						breathe(); ///// ----------------------
						String hash_id = prefs.getAccountHashId();
						EarningsLog reward = new EarningsLog(a, a.getReward(), TimeManager.getCurrentSyncronizedTimeStamp(),hash_id);
						earn.insertNew(reward);
						
						breathe(); ///// ----------------------
						AdActivity ad_activity = new AdActivity(a, a.getServerAdId(), AdActivityTypes.VIEWED, 
																TimeManager.getCurrentSyncronizedTimeStamp(), false);
						ads.recordAdActivity(ad_activity);
						//if(Globals.DEBUG) Log.v(TAG, "ad was consumed");

					}
					
					breathe(); ///// ----------------------
					messagesDao.update(m);
					message_itr.set(m);
				}				
				
			}
			
			if(are_new_ads_viewed==true){
				// do it asynchronous
				if(Globals.DEBUG) Log.v(TAG, "will send view receipts to server");
				prefs.setAreReceiptsSynced(false);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = message_list;
		
		// close out ads and whatever other models
		ads.close();
		earn.close();
	}
	
	/**
	 * This one is for an overview of the conversation thread
	 * @param thread_id
	 * @return
	 */
	public void setThreadsMessages(){
		
		List<Messages> results = new ArrayList<Messages>();
		Dao<Messages, Long> messagesDao;
		
		try {
			messagesDao = getHelper().getMessagesDao();
			
			results = messagesDao.queryBuilder().orderBy(Messages.DATE, false).groupBy(Messages.THREAD_ID).query();
			
			/*
			QueryBuilder<Messages, Long> qb = messagesDao.queryBuilder();
			qb.groupBy(Messages.THREAD_ID);
			String query = qb.prepareStatementString();
			GenericRawResults<String[]> rawResults = messagesDao.queryRaw(query);
			if(Globals.DEBUG) Log.v(TAG, "query to get threads: "+query);
			
			List<String[]> results = rawResults.getResults();
			// the results array should have 1 value
			String[] resultArray = results.get(0);
			// this should print the number of orders that have this account-id
			System.out.println("Account-id 10 has " + resultArray[0] + " orders");
			resultArray.
			*/
			
			// need to use query builder
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = results;
		
	}
	
	/*public String getReplyPhoneForThreadId(Integer id){
		
		String phone = "";
		
		Dao<Threads, Integer> threadsDao;
		Dao<Contacts, Integer> contactsDao;
		
		Contacts contact = new Contacts();
		
		Threads thread = new Threads();
		
		try {
			threadsDao = getHelper().getThreadsDao();
			thread = threadsDao.queryForId(id);
			threadsDao.
			
			/*
			QueryBuilder<Messages, Long> qb = messagesDao.queryBuilder();
			qb.groupBy(Messages.THREAD_ID);
			String query = qb.prepareStatementString();
			GenericRawResults<String[]> rawResults = messagesDao.queryRaw(query);
			if(Globals.DEBUG) Log.v(TAG, "query to get threads: "+query);
			
			List<String[]> results = rawResults.getResults();
			// the results array should have 1 value
			String[] resultArray = results.get(0);
			// this should print the number of orders that have this account-id
			System.out.println("Account-id 10 has " + resultArray[0] + " orders");
			resultArray.
			*/
			/*
			// need to use query builder
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list = results;
		startNotify();
		
		return phone;
		
	}
*/
	
	private void breathe(){
		int milliseconds = 5;
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}