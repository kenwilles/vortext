package com.vortextapp.android.models.tables;

public class ContactType { 
	
	public static final Integer OWNER = 1; 
	public static final Integer VORTEXT_USER = 2; 
	public static final Integer OPTED_IN = 3; 
	public static final Integer REGULAR = 4; 
	
	public static final Integer MAX_TYPE_COUNT = 4;

}
