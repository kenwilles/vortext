/**
 * This database should be emptied when data is stored in as an earnings_report
 */

package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;

import android.util.Log;

public class EarningsLog {
	
	private static final String TAG = "VorText: tables.EarningsLog"; // for debug label
	
	public static final String TABLE = "earningslog"; 
	
	@DatabaseField(generatedId = true)
	private Long id;
	public static final String ID = "id";
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Ads ad;
	public static final String AD_ID = "ad_id";
	
	@DatabaseField
	private Double amount;
	public static final String AMOUNT = "amount";
	
	@DatabaseField
	private Long date;
	public static final String DATE = "date";
	
	@DatabaseField
	private String hash_id;
	public static final String HASH_ID = "hash_id";
	
	public EarningsLog() {
		if(Globals.DEBUG) Log.v(TAG, "Constructor called");
	}
	
	public EarningsLog(Ads ad, Double amount, Long date,String hash_id){
		setAd(ad);
		setAmount(amount);
		setDate(date);
		setHashId(hash_id);
	}
	public EarningsLog(Ads ad, Double amount, Long date){
		setAd(ad);
		setAmount(amount);
		setDate(date);
	}

	public Long getId() {
		return id;
	}

	public Ads getAd() {
		return ad;
	}

	public Double getAmount() {
		return amount;
	}

	public Long getDate() {
		return date;
	}
	
	public String getHashId() {
		return hash_id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public void setAd(Ads ad) {
		this.ad = ad;
	}
	public void setAdId(Long ad_id){
		this.ad.setId(ad_id);
	}
	
	
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setDate(Long date) {
		this.date = date;
	}
	
	public void setHashId(String hash_id) {
		this.hash_id = hash_id;
	}

	/*
	
	public long createNew(int ad_id, double amount){

		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    
	    values.put(AD_ID, ad_id);
	    values.put(AMOUNT, amount);
	    values.put(DATE, System.currentTimeMillis());
	    
	    try {
	    	last_id = db.insertOrThrow(TABLE, null, values);
	    	if(Globals.DEBUG) Log.v(TAG, "table row saved");
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
	    
	    return last_id;

	}
	
	public double getTotalEarned(){
		
		SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = null;
	    double total = 0.0;
	    
	    try {
	    	String sql = "SELECT total(amount) FROM "+TABLE;
	    	cursor = db.rawQuery(sql, null);
	    	if(cursor.moveToFirst()==true){
	    		total = cursor.getDouble(0);
	    	} else {
	    		total = 0.0;
	    	}
	    	if(Globals.DEBUG) Log.v(TAG, "total amount earned is "+total);
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
		
		return total;
		
	}
	
	public Boolean transferToReport(EarningsReport report){

		//TODO need to get this method working at some point
		// needs to clear out log and put in report
		
		String ids = "";
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = null; 
		
		try {
			cursor = db.query(TABLE, COLUMNS, null, null, null, null, null);
			while(cursor.moveToNext()){
				if(ids==""){
					ids = cursor.getDouble(1)+"";
				} else {
					ids = ids + ", " + cursor.getDouble(1);
				}
			}
			if(Globals.DEBUG) Log.v(TAG, "got all results");
		} catch(Error e) {
			if(Globals.DEBUG) Log.v(TAG, "There was an error querying the db");
		} finally {
			db.close();
		}
		
		long status = report.createNew(getTotalEarned(), ids);
		
		if(status>0){
			// clear earnings log out
			SQLiteDatabase db2 = this.getWritableDatabase();
			try {
				db2.delete(TABLE, null, null);
			} catch(Error e){
				if(Globals.DEBUG) Log.v(TAG, "delete of rows didn't work");
			} finally  {
				db2.close();
			}
		}
		
		return true;
	}
	
	// TODO need to return contact types list eventually
	/*public List<String> getContactTypes(){
		
	}*/

}
