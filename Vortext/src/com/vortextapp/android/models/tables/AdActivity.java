/** This class acts as more of a log for analytics purposes. 
 * CREATE  TABLE IF NOT EXISTS `mydb`.`ad_activity` (
  `id` INT NOT NULL ,
  `ad_id` INT NULL ,
  `activity_type_id` INT NULL ,
  `date` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_conversion_type_id` (`activity_type_id` ASC) ,
  INDEX `fk_ad_id` (`ad_id` ASC) ,
  CONSTRAINT `fk_conversion_type_id`
    FOREIGN KEY (`activity_type_id` )
    REFERENCES `mydb`.`ad_activity_types` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ad_id`
    FOREIGN KEY (`ad_id` )
    REFERENCES `mydb`.`ads` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
 */

package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;

import android.util.Log;

public class AdActivity  {
	
	private static final String TAG = "VorText: tables.AdActivity"; // for debug label
	
	// may need to adjust depending on level of activity, ugh
	// calculated this based on 3000 average texts sent per month/30 days / 12 hours = 
	// 8 per hour that they could potentially be exposed to. 
	// set it a little lower for cache to update slightly more. 
	public static final int MIN_FRESH_ADS = 5; 
	
	public static final String TABLE = "ad_activity"; 
	
	//id, ad_id, activity_type_id, date
	
	@DatabaseField(generatedId = true)
	private Long id;
	public static final String ID = "id";
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Ads ad;
	public static final String AD_ID = "ad_id";
	
	@DatabaseField
	private Long server_ad_id;
	public static final String SERVER_AD_ID = "server_ad_id";
	
	// uses constants
	@DatabaseField
	private Integer activity_type_id;
	public static final String ACTIVITY_TYPE_ID = "activity_type_id";
	
	@DatabaseField(defaultValue = "false")
	private Boolean is_posted_to_server;
	public static final String IS_POSTED_TO_SERVER = "is_posted_to_server";
	
	@DatabaseField
	private Long date;
	public static final String DATE = "date";

	public AdActivity() {
		//if(Globals.DEBUG) Log.v(TAG, "Constructor called");
		setPostedToServer(false);
	}
	
	public AdActivity(Ads ad, Long server_ad_id, Integer activity_type_id, Long date, Boolean is_posted_to_server){
		setAd(ad);
		setServerAdId(server_ad_id);
		setActivityTypeId(activity_type_id);
		setDate(date);
		setPostedToServer(is_posted_to_server);
	}

	public Long getId() {
		return id;
	}

	public Ads getAd() {
		return ad;
	}

	public Long getServerAdId() {
		return server_ad_id;
	}

	public Integer getActivityTypeId() {
		return activity_type_id;
	}

	public Long getDate() {
		return date;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAd(Ads ad) {
		this.ad = ad;
	}

	public void setServerAdId(Long server_ad_id) {
		this.server_ad_id = server_ad_id;
	}

	public void setActivityTypeId(Integer activity_type_id) {
		this.activity_type_id = activity_type_id;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public void setPostedToServer(Boolean is_posted_to_server) {
		this.is_posted_to_server = is_posted_to_server;
	}

	public Boolean isPostedToServer() {
		return is_posted_to_server;
	}

}
