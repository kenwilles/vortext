package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;

import android.util.Log;

public class CategoriesInContact {
	
	private static final String TAG = "VorText: tables.CategoriesInContact"; // for debug label

	public static final String TABLE = "categories_in_contacts"; 
	
	@DatabaseField(generatedId = true)
	private Integer id;
	public static final String ID = "id";
	
	@DatabaseField(foreign = true)
	private Contacts contact;
	public static final String CONTACT_ID = "contact_id";
	
	@DatabaseField(foreign = true)
	private AdCategories ad_category;
	public static final String AD_CATEGORY_ID = "ad_category_id";
	
	public CategoriesInContact() {
		if(Globals.DEBUG) Log.v(TAG, "Constructor called");
	}

	public Integer getId() {
		return id;
	}

	public Contacts getContact() {
		return contact;
	}

	public AdCategories getAdCategory() {
		return ad_category;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setContact(Contacts contact) {
		this.contact = contact;
	}

	public void setAdCategory(AdCategories ad_category) {
		this.ad_category = ad_category;
	}
	
	/*
	public long createNew(int contact_id, int ad_category_id){

		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    
	    values.put(CONTACT_ID, contact_id);
	    values.put(AD_CATEGORY_ID, ad_category_id);
	    
	    try {
	    	last_id = db.insertOrThrow(TABLE, null, values);
	    	if(Globals.DEBUG) Log.v(TAG, "table row saved");
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
	    
	    return last_id;
	}
	*/

}
