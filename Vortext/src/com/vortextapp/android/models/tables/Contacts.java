package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;

import android.util.Log;

public class Contacts {
	
	private static final String TAG = "VorText: tables.Contacts"; // for debug label
	
	public static final String TABLE = "contacts"; 
	
	@DatabaseField(generatedId = true)
	private Integer id;
	public static final String ID = "id";
	
	@DatabaseField(canBeNull = true)
	private String server_hash_id;
	public static final String SERVER_HASH_ID = "server_hash_id";
	
	@DatabaseField(canBeNull = true)
	private String first_name;
	public static final String FIRST_NAME = "first_name";
	
	@DatabaseField(canBeNull = true)
	private String last_name;
	public static final String LAST_NAME = "last_name";
	
	@DatabaseField(canBeNull = false)
	private String phone1;
	public static final String PHONE1 = "phone1";
	
	@DatabaseField(canBeNull = true)
	private String phone2; 
	public static final String PHONE2 = "phone2";
	
	@DatabaseField(canBeNull = true)
	private String phone3;
	public static final String PHONE3 = "phone3";
	
	@DatabaseField(canBeNull = true)
	private String phone4;
	public static final String PHONE4 = "phone4";
	
	@DatabaseField(canBeNull = true)
	private String phone5;
	public static final String PHONE5 = "phone5";
	
	@DatabaseField(canBeNull = true)
	private String email1;
	public static final String EMAIL1 = "email1";
	
	@DatabaseField(canBeNull = true)
	private String email2;
	public static final String EMAIL2 = "email2";
	
	@DatabaseField(canBeNull = true)
	private String email3;
	public static final String EMAIL3 = "email3";
	
	@DatabaseField(canBeNull = true)
	private String picture; // path eventually
	public static final String PICTURE = "picture";
	
	@DatabaseField(canBeNull = true)
	private Integer age;
	public static final String AGE = "age";
	
	@DatabaseField(canBeNull = true)
	private String gender; // reference gender types
	public static final String GENDER = "gender";
	
	@DatabaseField(canBeNull = true)
	private Integer location_id;
	public static final String LOCATION_ID = "location_id";
	
	@DatabaseField(canBeNull = true)
	private Integer lat;
	public static final String LAT = "lat";
	
	@DatabaseField(canBeNull = true)
	private Integer lon;
	public static final String LONG = "lon";
	
	@DatabaseField(canBeNull = true)
	private String city;
	public static final String CITY = "city";
	
	@DatabaseField(canBeNull = true)
	private Integer zip;
	public static final String ZIP = "zip";
	
	@DatabaseField(canBeNull = true)
	private Integer type_id;
	public static final String TYPE_ID = "type_id";
	
	@DatabaseField(canBeNull = true)
	private Long date_created;
	public static final String DATE_CREATED = "date_created";
	
	@DatabaseField(canBeNull = true)
	private Long date_modified;
	public static final String DATE_MODIFIED = "date_modified";
	
	public Contacts() {
		if(Globals.DEBUG) Log.v(TAG, "Constructor called");
		init(null, null, null);
	}
	
	public Contacts(String phone1){
		init(phone1, null, null);
	}
	
	public Contacts(String phone1, String name){
		// split name string
		String name_array[] = name.split(" ");
		String f_name = "";
		String l_name = "";
		if(name_array.length==1){
			f_name = name_array[0];
			l_name = null;
		} else if(name_array.length==2){
			f_name = name_array[0];
			l_name = name_array[1];
		}
		init(phone1, f_name, l_name);
	}
	
	private void init(String phone1, String first_name, String last_name){
		setFirstName(first_name);
		setLastName(last_name);
		setPhone1(phone1);
	}

	public Integer getId() {
		return id;
	}

	public String getServerHashId() {
		return server_hash_id;
	}

	public String getFirstName() {
		return first_name;
	}

	public String getLastName() {
		return last_name;
	}

	public String getPhone1() {
		return phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public String getPhone4() {
		return phone4;
	}

	public String getPhone5() {
		return phone5;
	}

	public String getEmail1() {
		return email1;
	}

	public String getEmail2() {
		return email2;
	}

	public String getEmail3() {
		return email3;
	}

	public String getPicture() {
		return picture;
	}

	public Integer getAge() {
		return age;
	}

	public String getGender() {
		return gender;
	}

	public Integer getLocationId() {
		return location_id;
	}

	public Integer getLat() {
		return lat;
	}

	public Integer getLon() {
		return lon;
	}

	public String getCity() {
		return city;
	}

	public Integer getZip() {
		return zip;
	}

	public Integer getTypeId() {
		return type_id;
	}

	public Long getDateCreated() {
		return date_created;
	}

	public Long getDateModified() {
		return date_modified;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setServerHashId(String server_hash_id) {
		this.server_hash_id = server_hash_id;
	}

	public void setFirstName(String first_name) {
		this.first_name = first_name;
	}

	public void setLastName(String last_name) {
		this.last_name = last_name;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public void setPhone4(String phone4) {
		this.phone4 = phone4;
	}

	public void setPhone5(String phone5) {
		this.phone5 = phone5;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public void setEmail3(String email3) {
		this.email3 = email3;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setLocationId(Integer location_id) {
		this.location_id = location_id;
	}

	public void setLat(Integer lat) {
		this.lat = lat;
	}

	public void setLon(Integer lon) {
		this.lon = lon;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public void setTypeId(Integer type_id) {
		this.type_id = type_id;
	}

	public void setDateCreated() {
		this.date_created = TimeManager.getCurrentSyncronizedTimeStamp();
	}

	public void setDateModified() {
		this.date_modified = TimeManager.getCurrentSyncronizedTimeStamp();
	}

	/*public int createContactOwner(String email, String hash){
		
		int user_id = 0;
		user_id =  create(hash, "Me", "", 
						"", "", "", "", "", 
						email, "", "", 
						0, UNKNOWN_GENDER, "", "", "", "", "", 
						ContactType.OWNER, TimeManager.getCurrentSyncronizedTimeStamp(), TimeManager.getCurrentSyncronizedTimeStamp());
		
		return user_id;

	}
	*/
	
	/*
	public int createNew(String phone_number, String name, int type){
		
		// split name string
		String f_name = name.split(" ")[0];
		String l_name = name.split(" ")[1];
		
		int type_id = (type<=ContactType.MAX_TYPE_COUNT && type>0) ? type : ContactType.REGULAR;

		return create("", f_name, l_name, 
				phone_number, "", "", "", "", 
				"", "", "", 
				0, UNKNOWN_GENDER, "", "", "", "", "", 
				type_id, TimeManager.getCurrentSyncronizedTimeStamp(), TimeManager.getCurrentSyncronizedTimeStamp());
	}
	*/
	
	/*
	// TODO could use generics here but need to review that first
	private int create(String server_hash_id, String first_name, String last_name, 
							String phone1, String phone2, String phone3, String phone4, String phone5, 
							String email1, String email2, String email3, 
							int age, String gender, String location_id, String lat, String lon, String city, String zip, 
							int type_id, long date_created, long date_modified){
		
		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    int last_id = 0;
	    
	    values.put(SERVER_HASH_ID, server_hash_id);

	    values.put(FIRST_NAME, 	first_name);
	    values.put(LAST_NAME, 	last_name);
	    values.put(PHONE1, 		phone1);
	    values.put(PHONE2, 		phone2);
	    values.put(PHONE3, 		phone3);
	    values.put(PHONE4, 		phone4);
	    values.put(PHONE5, 		phone5);
	    values.put(EMAIL1, 		email1);
	    values.put(EMAIL2, 		email2);
	    values.put(EMAIL3, 		email3);
	    //values.put(PICTURE, 	"");
	    values.put(AGE, 		age);
	    values.put(GENDER, 		gender);
	    values.put(LOCATION_ID, location_id);
	    
	    values.put(LAT, 	lat);
	    values.put(LONG, 	lon);
	    values.put(CITY, 	city);
	    values.put(ZIP, 	zip);

	    values.put(TYPE_ID, type_id);
	    
	    values.put(DATE_CREATED, date_created);
	    values.put(DATE_MODIFIED, date_modified);

	    try {
	    	last_id = (int) db.insertOrThrow(TABLE, null, values);
	    	if(Globals.DEBUG) Log.v(TAG, "table row saved");
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
	    
	    return last_id;

	}	
	*/
	
	/*
	public int getContactId(String phone_number){
		return getContactIdSrc(phone_number, "");
	}
	
	public int getContactId(String phone_number, String name){
		return getContactIdSrc(phone_number, name);
	}
	
	private int getContactIdSrc(String phone_number, String name){
		
		SQLiteDatabase db = this.getReadableDatabase();
		String where = PHONE1+"="+phone_number+" OR "+PHONE2+"="+phone_number+" OR "+PHONE3+"="+phone_number+" OR "+ 
						PHONE4+"="+phone_number+" OR "+PHONE5+"="+phone_number;

		int contact_id = 0;

		try {

			Cursor c = db.query(TABLE, COLUMNS, where, null, null, null, ORDER_BY);
		    CursorWrapper cursor = new CursorWrapper(c);

		    if(cursor.getCount()>0){
		    	cursor.moveToFirst();
		    	contact_id = (int) cursor.getInt(cursor.getColumnIndex(_ID));
		    	
		    } else {
		    	if(Globals.DEBUG) Log.v(TAG, "contact was not in db so let's create a new one");
		    	contact_id = createNew(phone_number, name, ContactType.REGULAR);
		    }
		} catch(Error e){
			if(Globals.DEBUG) Log.v(TAG, "could not get or create contact id");
		} finally {
			db.close();
		}

	    return contact_id;

	}
	*/
	
}
