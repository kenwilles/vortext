package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;

import android.util.Log;

public class EarningsReport {
	
	private static final String TAG = "VorText: tables.EarningsReport"; // for debug label
	
	public static final String TABLE = "earnings_report"; 
	
	@DatabaseField(generatedId = true)
	private Integer id;
	public static final String ID = "id";
	
	@DatabaseField
	private Double amount;
	public static final String AMOUNT = "amount";
	
	@DatabaseField
	private String ad_ids_claimed;
	public static final String AD_IDS_CLAIMED = "ad_ids_claimed";
	
	@DatabaseField(defaultValue = "false")
	private Boolean is_posted_to_server;
	public static final String IS_POSTED_TO_SERVER = "is_posted_to_server";
	
	@DatabaseField
	private Long date;
	public static final String DATE = "date";
	
	public EarningsReport() {
		if(Globals.DEBUG) Log.v(TAG, "Constructor called");
		setPostedToServer(false);
	}

	public Integer getId() {
		return id;
	}

	public Double getAmount() {
		return amount;
	}

	public String getAdIdsClaimed() {
		return ad_ids_claimed;
	}

	public Long getDate() {
		return date;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setAdIdsClaimed(String ad_ids_claimed) {
		this.ad_ids_claimed = ad_ids_claimed;
	}

	public void setDate(Long date) {
		this.date = date;
	}
	
	public void setPostedToServer(Boolean is_posted_to_server) {
		this.is_posted_to_server = is_posted_to_server;
	}

	public Boolean isPostedToServer() {
		return is_posted_to_server;
	}
	
/*	public long createNew(double amount, String ad_ids){

		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	   
	    values.put(AMOUNT, amount);
	    values.put(AD_IDS_CLAIMED, ad_ids);
	    values.put(DATE, System.currentTimeMillis());
	    
	    try {
	    	last_id = db.insertOrThrow(TABLE, null, values);
	    	if(Globals.DEBUG) Log.v(TAG, "table row saved");
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
	    
	    return last_id;

	}
*/

}
