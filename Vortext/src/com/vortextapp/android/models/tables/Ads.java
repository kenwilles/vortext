/**
 * Ads
 */

package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;

import android.util.Log;

public class Ads {
	
	private static final String TAG = "VorText: tables.Ads"; // for debug label
	public static final String TABLE = "ads"; 
	//private AdActivity ad_activity;
	
	@DatabaseField(generatedId = true)
	private Long id;
	public static final String ID = "id";
	
	@DatabaseField
	private Long server_ad_id;
	public static final String SERVER_AD_ID = "server_ad_id";
	
	@DatabaseField
	private Integer advertiser_id;
	public static final String ADVERTISER_ID = "advertiser_id";
	
	@DatabaseField
	private String for_hash_id; // who it is intended for, vortext user or opt-in only person
	public static final String FOR_HASH_ID = "for_hash_id";
	
	@DatabaseField
	private String message;
	public static final String MESSAGE = "message";
	
	@DatabaseField
	private Double reward;
	public static final String REWARD = "reward";
	
	@DatabaseField
	private Boolean is_engaged;
	public static final String IS_ENGAGED = "is_engaged"; // has ad been anything other than cached? 
	
	@DatabaseField
	private Long time_delivered;
	public static final String TIME_DELIVERED = "time_delivered";
	
	@DatabaseField
	private Long read_expires;
	public static final String READ_EXPIRES = "read_expires";

	public Ads() {
		//ad_activity = new AdActivity(c);
		//if(Globals.DEBUG) Log.v(TAG, "Constructor called");
	}

	public Long getId() {
		return id;
	}

	public Long getServerAdId() {
		return server_ad_id;
	}

	public void setAdvertiserId(Integer advertiser_id) {
		this.advertiser_id = advertiser_id;
	}

	public Integer getAdvertiserId() {
		return advertiser_id;
	}

	public String getForHashId() {
		return for_hash_id;
	}

	public String getMessage() {
		return message;
	}

	public Double getReward() {
		return reward;
	}

	public Boolean getIsEngaged() {
		return is_engaged;
	}

	public Long getTimeDelivered() {
		return time_delivered;
	}

	public Long getReadExpires() {
		return read_expires;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setServerAdId(Long server_ad_id) {
		this.server_ad_id = server_ad_id;
	}

	public void setForHashId(String for_hash_id) {
		this.for_hash_id = for_hash_id;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setReward(Double reward) {
		this.reward = reward;
	}

	public void setIsEngaged(Boolean is_engaged) {
		this.is_engaged = is_engaged;
	}

	public void setTimeDelivered(Long time_delivered) {
		this.time_delivered = time_delivered;
	}

	public void setReadExpires(Long read_expires) {
		this.read_expires = read_expires;
	}	
	
	/**
	 * This is what retrieves ad from cache to display in thread!
	 * @return
	 
	public String getOne(String for_hash_id){
		
		SQLiteDatabase db = this.getReadableDatabase();
		String where = IS_ENGAGED+"=0 AND "+FOR_HASH_ID+"="+for_hash_id;
		String ad_message = "";

		try {

			Cursor c = db.query(TABLE, COLUMNS, where, null, null, null, ORDER_BY);
		    CursorWrapper cursor = new CursorWrapper(c);

		    if(cursor.getCount()>0){
		    	cursor.moveToFirst();
		    	ad_message = cursor.getString(cursor.getColumnIndex(MESSAGE));
		    	markEngaged(cursor.getInt(cursor.getColumnIndex(_ID)));
		    	ad_activity.createNew(
		    				cursor.getInt(cursor.getColumnIndex(_ID)), 
		    				cursor.getLong(cursor.getColumnIndex(SERVER_AD_ID)), 
		    				AdActivityTypes.VIEWED
		    				);
		    } else {
		    	if(Globals.DEBUG) Log.v(TAG, "was not able to get an ad");
		    }
		} catch(Error e){
			if(Globals.DEBUG) Log.v(TAG, "could not get the next ad available");
		} finally {
			db.close();
		}
		
	    //refresh();
	    return ad_message;
	}
	*/
	
	/*
	public Boolean createNew(long server_ad_id, String for_hash_id, String message, double reward, long read_expires){
		
		// need to reference AdActivity and mark as delivered
		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    
	    values.put(SERVER_AD_ID, server_ad_id);
	    values.put(FOR_HASH_ID, for_hash_id);
	    values.put(MESSAGE, message);
	    values.put(REWARD, reward);
	    values.put(IS_ENGAGED, 0);
	    values.put(TIME_DELIVERED, TimeManager.getCurrentSyncronizedTimeStamp());
	    values.put(READ_EXPIRES, read_expires);
	    
	    long last_id = -1;
	    
		try {
	    	last_id  = db.insertOrThrow(TABLE, null, values);
	    	if(last_id>0){
	    		ad_activity.createNew((int) last_id, server_ad_id, AdActivityTypes.CACHED);
	    	} else {
	    		if(Globals.DEBUG) Log.v(TAG, "table row NOT saved");
	    	}
	    	if(Globals.DEBUG) Log.v(TAG, "table row saved with returned id of "+last_id+" and server_id of "+server_ad_id);
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
	    
	    return last_id > 0 ? true : false;

	}
	*/
	
	/*
	public void markEngaged(int ad_id){

		// need to reference AdActivity and mark as delivered
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		String where = _ID+"="+ad_id;
	    values.put(IS_ENGAGED, 1);
		
	    try {
	    	
	    	db.update(TABLE, values, where, null);
	    	
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account, message: "+e.getMessage());
	    } finally {
	    	db.close();
	    }
	    
	}
	*/
	
	/*
	 * Is this necessary ?
	 
	public void refresh(){
		// looks for ads that have expired and deletes them
		// look for is_engaged=0 + expired
	}
	*/

   /*
   private void showEvents(Cursor cursor) {
      // Set up data binding
      SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
            R.layout.item, cursor, FROM, TO);
      setListAdapter(adapter);
   }
   */

}