package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;

import android.util.Log;

public class Threads {
	
	private static final String TAG = "VorText: tables.Threads"; // for debug label
	
	public static final String TABLE = "threads"; 
	
	//id, date_created
	
	@DatabaseField(generatedId = true)
	private Integer id;
	public static final String ID = "id";
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Contacts contact;
	public static final String CONTACTS_ID = "contact_id";
	
	@DatabaseField
	private Long date_created;
	public static final String DATE_CREATED = "date_created";
	
	public Threads() {
		if(Globals.DEBUG) Log.v(TAG, "Constructor called");
	}
	
	public Threads(Contacts contact){
		init(contact);
	}
	
	private void init(Contacts contact){
		setContact(contact);
	}

	public Integer getId() {
		return id;
	}

	public Long getDateCreated() {
		return date_created;
	}
	
	public Contacts getContact() {
		return contact;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setDateCreated() {
		this.date_created = TimeManager.getCurrentSyncronizedTimeStamp();
	}
	
	public void setContact(Contacts contact) {
		this.contact = contact;
	}

	/*
	
	public long createNew(){
		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    
	    values.put(DATE_CREATED, System.currentTimeMillis());
	    
	    try {
	    	last_id = db.insertOrThrow(TABLE, null, values);
	    	if(Globals.DEBUG) Log.v(TAG, "table row saved");
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
	    
	    return last_id;
	}
	
	public long getLastInsertId(){
		return last_id;
	}
	
	*/

}
