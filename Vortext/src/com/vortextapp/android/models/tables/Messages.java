package com.vortextapp.android.models.tables;
import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;

import android.util.Log;

public class Messages {
	
	private static final String TAG = "VorText: tables.Messages"; // for debug label
	//private Contacts contacts;
	//private PeopleInThreads people_in_threads;
	
	public static final String TABLE = "messages"; 
	
	@DatabaseField(generatedId = true)
	private Long id;
	public static final String ID = "id";
	
	@DatabaseField(canBeNull = true)
	private Long system_id;
	public static final String SYSTEM_ID = "system_id";
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Threads thread;
	public static final String THREAD_ID = "thread_id";
	
	@DatabaseField(canBeNull = true)
	private Integer system_thread_id;
	public static final String SYSTEM_THREAD_ID = "system_thread_id";
	
	@DatabaseField(foreign = true)
	private Folders folder;
	public static final String FOLDERS_ID = "folder_id";
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Contacts contact;
	public static final String CONTACT_ID = "contact_id";
	
	@DatabaseField
	private Boolean is_read;
	public static final String IS_READ = "is_read";
	
	//@Hai database field for send status
	@DatabaseField
	private String status;
	public static final String STATUS = "status";
	
	@DatabaseField
	private String message;
	public static final String MESSAGE = "message";
	
	@DatabaseField
	private Long date;
	public static final String DATE = "date";
	
	@DatabaseField
	private String ad_text;
	public static final String AD_TEXT = "ad_text";
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true)
	private Ads ad;
	public static final String AD_ID = "ad_id";

	public Messages() {
		if(Globals.DEBUG) Log.v(TAG, "Constructor called");
		//contacts = new Contacts(c);
		//people_in_threads = new PeopleInThreads(c);
		init(null, null, null, null, false,null);
	}
	
	public Messages(Contacts contact, Threads thread, String message, Boolean is_read,String status){
		init(contact, thread, null, message, is_read,status);
	}
	
	public Messages(Contacts contact, Threads thread, Ads ads, String message, Boolean is_read,String status){
		init(contact, thread, ads, message, is_read,status);
	}
	
	public void init(Contacts contact, Threads thread, Ads ad, String message, Boolean is_read,String status){
		setContacts(contact);
		setThreads(thread);
		setAds(ad);
		setMessage(message);
		setIsRead(is_read);
		setStatus(status);
	}

	public Long getId() {
		return id;
	}

	public Long getSystemId() {
		return system_id;
	}

	public Threads getThread() {
		return thread;
	}

	public Integer getSystemThreadId() {
		return system_thread_id;
	}

	public Contacts getContact() {
		return contact;
	}

	public Boolean getIsRead() {
		return is_read;
	}

	public String getMessage() {
		return message;
	}

	public Long getDate() {
		return date;
	}

	public String getAdText() {
		return ad_text;
	}

	public Ads getAd() {
		return ad;
	}
	//@Hai methods for send status
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status)
	{
		this.status = status;
	}	
	
	public Folders getFolders() {
		return folder;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSystemId(Long system_id) {
		this.system_id = system_id;
	}

	public void setThreads(Threads thread) {
		this.thread = thread;
	}

	public void setSystemThreadId(Integer system_thread_id) {
		this.system_thread_id = system_thread_id;
	}

	public void setContacts(Contacts contact) {
		this.contact = contact;
	}

	public void setIsRead(Boolean is_read) {
		this.is_read = is_read;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setDate() {
		this.date = TimeManager.getCurrentSyncronizedTimeStamp();
	}

	public void setAdText(String ad_text) {
		this.ad_text = ad_text;
	}

	public void setAds(Ads ad) {
		this.ad = ad;
	}
	
	public void setFolders(Folders folder) {
		this.folder = folder;
	}
	

	/*

	public Boolean createIncoming(String message, String phone_number, int is_read) {
		int contact_id =  contacts.getContactId(phone_number);
		int thread_id = people_in_threads.getThreadId(contact_id);
		int _is_read = (is_read >=0 && is_read <=1) ? is_read : 0;
		return create(0, thread_id, 0, contact_id, _is_read, message, "", 0);
	}
	*/
	
	/*
	public Boolean createOutgoing() {
		return false;
	}
	
	/*public Boolean createDraft() {
		return false;
	}*/
	
	/*
	private Boolean create(int system_id, int thread_id, int system_thread_id, 
							int contact_id, int is_read, String message, String ad_text, int ad_id){

		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    long row_id = -1;
	    
	    // TODO need system id and thread id
	    values.put(SYSTEM_ID, system_id);
	    values.put(THREAD_ID, thread_id);
	    values.put(SYSTEM_THREAD_ID, system_thread_id);

	    values.put(CONTACT_ID, contact_id);
	    values.put(IS_READ, is_read);
	    values.put(MESSAGE, message);
	    
	    values.put(DATE, TimeManager.getCurrentSyncronizedTimeStamp());

	    values.put(AD_TEXT, ad_text);
	    values.put(AD_ID, ad_id);
	    
	    try {
	    	row_id = db.insertOrThrow(TABLE, null, values);
	    	if(Globals.DEBUG) Log.v(TAG, "table row saved");
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
	    
	    if(row_id<0){
	    	return false;
	    } else {
	    	return true;
	    }

	}
	*/

	/**
	 * Must close cursor! Not sure if there is an elegant way to do that. . . 
	 * @param id
	 * @return
	 */
	/*
	public CursorWrapper getThread(int id){
		SQLiteDatabase db = this.getReadableDatabase();
		String where = THREAD_ID+"="+id;
	    Cursor c = db.query(TABLE, COLUMNS, where, null, null, null, ORDER_BY);
	    CursorWrapper cursor = new CursorWrapper(c);

	    return cursor;

	}
	*/
	
	/**
	 * Must close cursor!
	 * @return
	 */
	/*
	public CursorWrapper getThreads(){
		SQLiteDatabase db = this.getReadableDatabase();
		String where = null;
	    Cursor c = db.query(TABLE, COLUMNS, where, null, THREAD_ID, null, ORDER_BY);
	    CursorWrapper cursor = new CursorWrapper(c);
	    return cursor;
	}
	*/
	
	/*
	public Boolean update(int id){
		
		return false;
		
		// TODO still need to flesh this out
		/*
		// Insert a new record into the Events data source.
	    // You would do something similar for delete and update.
	    SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
		
		int updated_rows = 0;
		String where = "contact_id="+contact_id;
		
		values.put(IS_LOGGED_IN, false);
		
		try {
			updated_rows = db.update(TABLE, values, where, null);
			if(Globals.DEBUG) Log.v(TAG, "IS_LOGGED_IN was set to false in database");
		} catch(Error e) {
	    	if(Globals.DEBUG) Log.v(TAG, "could not change user account login");
		} finally {
			db.close();
		}

		if(updated_rows>0){ 
			return true;
		} else {
			if(Globals.DEBUG) Log.v(TAG, "could not change user account login");
			return false;
		}
		
		
	}
	*/

   /*
   private void showEvents(Cursor cursor) {
      // Set up data binding
      SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
            R.layout.item, cursor, FROM, TO);
      setListAdapter(adapter);
   }
   */

}