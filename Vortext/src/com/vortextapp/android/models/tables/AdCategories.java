/*
 * Ad Categories
 */

package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;

import android.util.Log;

public class AdCategories {
	
	private static final String TAG = "VorText: tables.AdCategories"; // for debug label
	
	public static final String TABLE = "ad_categories"; 
	
	@DatabaseField(generatedId = true)
	private Integer id;
	public static final String ID = "id";
	
	@DatabaseField
	private String name;
	public static final String NAME = "name";
	
	public AdCategories() {
		init("undefined");
	}
	
	public AdCategories(String name){
		init(name);
	}
	
	private void init(String name){
		if(Globals.DEBUG) Log.v(TAG, "initalized");
		setName(name);
	}
	
	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

}
