/*
 * CREATE  TABLE IF NOT EXISTS `mydb`.`ad_activity_types` (
  `id` INT NOT NULL ,
  `title` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
  ENGINE = InnoDB
 */

package com.vortextapp.android.models.tables;

public final class AdActivityTypes { 

	public static final Integer CACHED = 1; 
	public static final Integer VIEWED = 2; 
	public static final Integer CLICKED = 3; 
	
	// Future types...
	public static final Integer FORWARDED = 4; 
	public static final Integer SENT = 5; 
	public static final Integer EXPIRED = 6; 

}
