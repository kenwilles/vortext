package com.vortextapp.android.models.tables;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;

import android.util.Log;

public class Folders {
	
	private static final String TAG = "VorText: tables.Folders"; // for debug label

	public static final int DEFAULT_PRIORITY = 1;

	public static final String TABLE = "folders"; 
	
	@DatabaseField(generatedId = true)
	private Integer id;
	public static final String ID = "id";
	
	@DatabaseField
	private String folder_name;
	public static final String FOLDER_NAME = "folder_name";
	
	@DatabaseField
	private Integer priority;
	public static final String PRIORITY = "priority";
	
	public Folders() {
		//createNew("default");
		if(Globals.DEBUG) Log.v(TAG, "Constructor called");
		init("undefined");
	}
	
	public Folders(String folder_name){
		init(folder_name);
	}
	
	private void init(String folder_name){
		setFolderName(folder_name);
	}

	public Integer getId() {
		return id;
	}

	public String getFolderName() {
		return folder_name;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setFolderName(String folder_name) {
		this.folder_name = folder_name;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}


	/*
	
	public long createNew(String name){
		return createNew(name, DEFAULT_PRIORITY);
	}
	
	public long createNew(String name, int priority){
		//TODO need to check that name is safe, ie not blank and handle error properly
		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    
	    values.put(FOLDER_NAME, name);
	    values.put(PRIORITY, priority);
	    
	    try {
	    	last_id = db.insertOrThrow(TABLE, null, values);
	    	if(Globals.DEBUG) Log.v(TAG, "table row saved");
	    } catch(Error e){
	    	if(Globals.DEBUG) Log.v(TAG, "table could not insert new account");
	    } finally {
	    	db.close();
	    }
	    
	    return last_id;
	}
	*/

}
