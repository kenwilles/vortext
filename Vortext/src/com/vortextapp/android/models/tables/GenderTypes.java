package com.vortextapp.android.models.tables;

public class GenderTypes { 
	
	// gender types
	public static final String MALE = "male";
	public static final String FEMALE = "female";
	public static final String UNKNOWN_GENDER = "unknown";

}
