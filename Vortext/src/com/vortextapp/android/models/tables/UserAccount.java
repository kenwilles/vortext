/**
 * There should only be one UserAccount entry and save is what creates everything in the db structure
 */

package com.vortextapp.android.models.tables;

//import com.vortextapp.android.config.Database;
//import com.vortextapp.android.config.Globals;

import com.j256.ormlite.field.DatabaseField;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;

import android.util.Log;

public class UserAccount {
	
	private static final String TAG = "VorText: tables.UserAccount"; // for debug label

	/*
	private AdActivity ad_activity;
	private AdCategories ad_categories;
	private Ads ads;
	private CategoriesInContact categories_in_contact;
	private Contacts contacts;
	private EarningsLog earnings_log;
	private EarningsReport earnings_report;
	private Folders folders;
	private Messages messages;
	private PeopleInThreads people_in_threads;
	private Threads threads;
	*/
	
	public static final String TABLE = "user_account"; 
	
	@DatabaseField(generatedId = true)
	private Integer id;
	public static final String ID = "id";
	
	@DatabaseField(foreign = true)
	private Contacts contact;
	public static final String CONTACT_ID = "contact_id";
	
	@DatabaseField
	private String hash_id;
	public static final String HASH_ID = "hash_id";
	
	@DatabaseField
	private Long login_time;
	public static final String LOGIN_TIME = "login_time";
	
	@DatabaseField
	private Boolean is_logged_in;
	public static final String IS_LOGGED_IN = "is_logged_in";
	
	@DatabaseField
	private String email;
	public static final String EMAIL = "email";

	public UserAccount() {
		
		/*ad_activity = new AdActivity(c);
		ad_categories = new AdCategories(c);
		ads = new Ads(c);
		categories_in_contact = new CategoriesInContact(c);
		contacts = new Contacts(c);
		earnings_log = new EarningsLog(c);
		earnings_report = new EarningsReport(c);
		folders = new Folders(c);
		messages = new Messages(c);
		people_in_threads = new PeopleInThreads(c);
		threads = new Threads(c);
		*/
		
		init(null, null, null);
		
		if(Globals.DEBUG) Log.v(TAG, "Constructor called");
	}
	
	public UserAccount(String hash_id, String email, Contacts contact){
		init(hash_id, email, contact);
	}
	
	public void init(String hash_id, String email, Contacts contact){
		setHashId(hash_id);
		setEmail(email);
		setLoginTime();
		setContact(contact);
		setIsLoggedIn(true);
	}

	public Integer getId() {
		return id;
	}

	public Contacts getContact() {
		return contact;
	}

	public String getHashId() {
		return hash_id;
	}

	public Long getLoginTime() {
		return login_time;
	}

	public Boolean getIsLoggedIn() {
		return is_logged_in;
	}

	public String getEmail() {
		return email;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setContact(Contacts contact) {
		this.contact = contact;
	}

	public void setHashId(String hash_id) {
		this.hash_id = hash_id;
	}

	public void setLoginTime() {
		this.login_time = TimeManager.getCurrentSyncronizedTimeStamp();
	}

	public void setIsLoggedIn(Boolean is_logged_in) {
		this.is_logged_in = is_logged_in;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * this is the first time the database could potentially be created
	 * UserAccount onCreate is called automatically upon first init, all the
	 * rest have to be called manually
	 */
	//
	/*
	public void onCreate(SQLiteDatabase db) {

		//synchronized (this) {
			db.execSQL(TABLE_CREATE);
			//this.wait();
		//}
		
		//synchronized (ad_activity){
			ad_activity.onCreate(ad_activity.getWritableDatabase());
			//ad_activity.close();
		//}
		
		//synchronized (ad_categories){
			ad_categories.onCreate(ad_categories.getWritableDatabase());
			//ad_categories.close();
		//}
		
		//synchronized (ads){
			ads.onCreate(ads.getWritableDatabase());
			//ads.close();
		//}
		
		//synchronized (categories_in_contact){
			categories_in_contact.onCreate(categories_in_contact.getWritableDatabase());
			//categories_in_contact.close();
		//}
		
		//synchronized (contacts){
			contacts.onCreate(contacts.getWritableDatabase());
			//contacts.close();
		//}
		
		//synchronized (earnings_log){
			earnings_log.onCreate(earnings_log.getWritableDatabase());
			//earnings_log.close();
		//}
		
		//synchronized (earnings_report){
			earnings_report.onCreate(earnings_report.getWritableDatabase());
			//earnings_report.close();
		//}
		
		//synchronized (folders){
			folders.onCreate(folders.getWritableDatabase());
			//folders.close();
		//}
		
		//synchronized (messages){
			messages.onCreate(messages.getWritableDatabase());
			//messages.close();
		//}
		
		//synchronized (people_in_threads){
			people_in_threads.onCreate(people_in_threads.getWritableDatabase());
			//people_in_threads.close();
		//}
		
		//synchronized (threads){
			threads.onCreate(threads.getWritableDatabase());
			//threads.close();
		//}
		
		if(Globals.DEBUG) Log.v(TAG, "database created");
		
	}
	*/
	
	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	//
	//public void onUpgrade(SQLiteDatabase db, int old_version, int new_version) {
		// TODO need create alter script here
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE);
		//onCreate(db);
	//}
	
	//@SuppressWarnings("unused")
	/*
	public Boolean isLoggedIn(){
		
		if(Globals.IS_LOGIN_SKIPPED==false){
			
			if(isLoginSaved()==true){
				return checkLoginStatus();			
			} else {
				return false;
			}
			
		} else {
			return true;
		}
		
	}
	*/
	
	/*
	public Boolean login(String hash_id, String email){
		
		Boolean result = false;
		
		//TODO need to add support for when users have logged out 
		//but record is still active with only IS_LOGGED_IN set to false
		
		if(isLoginSaved()==false){
			result = save(hash_id, email);
			return result;
		} else {
			result = updateToLoggedIn(hash_id);
			return result;
		}

	}
	*/
	
	/*
	private Boolean checkLoginStatus(){
		SQLiteDatabase db = this.getReadableDatabase();
		String where = IS_LOGGED_IN+"=1";
	    Cursor c = db.query(TABLE, COLUMNS, where, null, null, null, ORDER_BY);
	    CursorWrapper cursor = new CursorWrapper(c);

	    if(cursor.getCount()>0){
	    	cursor.close();
	    	return true;
	    } else {
	    	cursor.close();
	    	return false;
	    }
	    
	}
	*/
	
	/*
	private Boolean isLoginSaved(){
		
		// TODO need to check for session details more carefully
		
		SQLiteDatabase db = this.getReadableDatabase();
		//query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
	    Cursor c = db.query(TABLE, COLUMNS, null, null, null, null, ORDER_BY);
	    CursorWrapper cursor = new CursorWrapper(c);

	    if(cursor.getCount()>0){
	    	cursor.close();
	    	return true;
	    } else {
	    	cursor.close();
	    	return false;
	    }

	}
	*/
	
	/**
	 * Save user account to table if it hasn't been already. This should only be called if 
	 * user successfully authenticated through REST call. 
	 * @param hash_id
	 * @param email
	 * @return 
	 */
	/*
	private Boolean save(String hash_id, String email) {
		
		Boolean is_login_saved = false;
					
		// Insert a new record into the Events data source.
	    // You would do something similar for delete and update.
		
		int contact_id = contacts.createContactOwner(hash_id, email);
		
	    SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    long result_id = 0;
	    
	    //TODO create contact_id
	    values.put(CONTACT_ID, contact_id);
	    values.put(HASH_ID, hash_id);
	    values.put(LOGIN_TIME, System.currentTimeMillis());
	    values.put(IS_LOGGED_IN, 1);
	    values.put(EMAIL, email);	    
	    
	    try {
	    	result_id = db.insertOrThrow(TABLE, null, values);
	    	is_login_saved = true;
	    	if(Globals.DEBUG) Log.v(TAG, "UserAccount table row saved, result_id: "+result_id);
	    } catch(Error e){
	    	is_login_saved = false;
	    	if(Globals.DEBUG) Log.v(TAG, "UserAccount table could not insert new account");
	    } finally {
	    	db.close();
	    }		    

		return is_login_saved;
		
	}
	*/
	
	/*
	private Boolean updateToLoggedIn(String hash_id){
		
		SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
		
		int updated_rows = 0;
		String where = HASH_ID+"="+hash_id;
		
		values.put(IS_LOGGED_IN, false);

		try {
			updated_rows = db.update(TABLE, values, where, null);
			if(Globals.DEBUG) Log.v(TAG, "IS_LOGGED_IN was set to false in database");
		} catch(Error e) {
	    	if(Globals.DEBUG) Log.v(TAG, "could not change user account login");
		} finally {
			db.close();
		}

		if(updated_rows>0){ 
			return true;
		} else {
			if(Globals.DEBUG) Log.v(TAG, "could not change user account login");
			return false;
		}
	}
	*/
	
	/*
	public Boolean logout(int contact_id){
		return updateToLoggedOut(contact_id);
	}
	*/
	
	/*
	private Boolean updateToLoggedOut(int contact_id){
		
	    SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues values = new ContentValues();
		
		int updated_rows = 0;
		String where = CONTACT_ID+"="+contact_id;
		
		values.put(IS_LOGGED_IN, false);
		
		try {
			updated_rows = db.update(TABLE, values, where, null);
			if(Globals.DEBUG) Log.v(TAG, "IS_LOGGED_IN was set to false in database");
		} catch(Error e) {
	    	if(Globals.DEBUG) Log.v(TAG, "could not change user account login");
		} finally {
			db.close();
		}

		if(updated_rows>0){ 
			return true;
		} else {
			if(Globals.DEBUG) Log.v(TAG, "could not change user account login");
			return false;
		}
		
	}
	*/

   /*
   private void showEvents(Cursor cursor) {
      // Set up data binding
      SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
            R.layout.item, cursor, FROM, TO);
      setListAdapter(adapter);
   }
   */

}