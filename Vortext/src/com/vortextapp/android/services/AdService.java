package com.vortextapp.android.services;
 
import java.util.List;

import org.json.JSONException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
 
import com.google.gson.Gson;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.libraries.RESTClient;
import com.vortextapp.android.libraries.RESTResponder;
import com.vortextapp.android.models.AdsModel;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.models.data.Ad;
 
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
 
public class AdService extends Service {
     
    private static final String TAG = "VorText: AdService"; // for debug label
    private final IBinder mBinder = new MyBinder();
    private PreferencesModel prefs = PreferencesModel.getInstance();
    private AdsModel ads_model;
    private Response rest_responder;
    private Context context;
    private int count_ads_available;
 
    
    public int onStartCommand(Intent intent, int flags, int startId) {
    	//android.os.Debug.waitForDebugger();
    	if(Globals.DEBUG) Log.v(TAG, "started");
    	
        context = this.getApplicationContext();
        if (context == null){
        	if(Globals.DEBUG) Log.v(TAG, "context is null");
        	stopSelf();}
        else{
    	//ads_model = new AdsModel(context);
    	ads_model = new AdsModel(context);
        if(prefs.isLoggedIn()==true){
        	//android.os.Debug.waitForDebugger();
            update();
        } else {
        	stopSelf();
        }}
        
        return Service.START_NOT_STICKY;
         
    }
 
    
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }
     
    
    public void onDestroy() {
        super.onDestroy();
        //android.os.Debug.waitForDebugger();
        ads_model.close();
        if(Globals.DEBUG) Log.v(TAG, "destroyed");
    }
 
    public class MyBinder extends Binder {
        AdService getService() {
        	//android.os.Debug.waitForDebugger();
        	if(Globals.DEBUG) Log.v(TAG, "In binder");
            return AdService.this;
        }
    }
     
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void update(){
    	//android.os.Debug.waitForDebugger();
    	ads_model.clearExpired();
    	//just being carefull if hash_id is not found
         String my_hash_id = prefs.getAccountHashId();
         count_ads_available = ads_model.getUnreadAdsForHashId(my_hash_id).size();
 		if((my_hash_id != null) && (!my_hash_id.equals("0")) && (!my_hash_id.equals(""))){
 			count_ads_available = ads_model.getUnreadAdsForHashId(my_hash_id).size();
 		}else{
 			count_ads_available = ads_model.getUnreadAds().size();
 		}
         
         
        if(count_ads_available<50){ // don't get more if we don't need to
            
            String qty = Globals.AD_DELIVER_QTY; //TODO this will need to get dynamically set depending on how many text messages they send on average. 
            //android.os.Debug.waitForDebugger();
            //String service = URLCollection.API + "get_ads/";
            //String service = "portal/api10/get_ads/";
            MultiValueMap parts = new LinkedMultiValueMap();
            parts.add("hash_id", prefs.getAccountHashId());
            parts.add("qty", qty);
            parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);
             
            rest_responder = new Response();
            RESTClient client = new RESTClient(rest_responder, parts);
            if (client.isOnline()) 
            	//android.os.Debug.waitForDebugger();
            	//client.execute(service);
            	if(Globals.DEBUG) Log.v(TAG, "About to execute Rest");
            	client.execute("https://www.rocketlinkmobile.com/portal/api10/get_ads/");
        }
    }
    
    public void close() {
    	//android.os.Debug.waitForDebugger();
        ads_model.close();
    }
     
    private class Response implements RESTResponder {
        public void handleRESTResponse(String response) {
        	//android.os.Debug.waitForDebugger();
        	if(Globals.DEBUG) Log.v(TAG, "Received Rest respond");
            Gson gson = new Gson();
            AdsResponse ads_from_server = null;
            try{
            ads_from_server = gson.fromJson(response, AdsResponse.class);
            } finally
            {}
            if (ads_from_server != null){
            if(ads_from_server.wereAdsFound()==true){
                ads_model.insertNewAds(ads_from_server.ads);
                if  ((count_ads_available+ads_from_server.countAds())>=50){               
                prefs.setNeedAdsImmediately(false);
                //android.os.Debug.waitForDebugger();
                }
                prefs.setAreReceiptsSynced(false); // this marks it for synchronization later in 5 mins
                if(Globals.DEBUG) Log.v(TAG, "AdsResponse "+ads_from_server.wereAdsFound()+" ad pieces array length "+ads_from_server.ads.size());
    			
            } else {
                //android.os.Debug.waitForDebugger();
            	if(Globals.DEBUG) Log.v(TAG, "No Ads were found from server, response "+response);
            }}
            
            stopSelf(); // we are done for now
            
        }
    }
     
    public class AdsResponse {
         
        public List<Ad> ads;
        private int success;        
         
        public Boolean wereAdsFound() {
            if(success==1){
                return true;
            } else {
                return false;
            }
        }
        public int countAds(){       	
        	return (ads.size()>0? ads.size():0);
        	
        }
         
    }
 
 
}