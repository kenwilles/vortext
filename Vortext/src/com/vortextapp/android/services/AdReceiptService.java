package com.vortextapp.android.services;
 
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
 
import com.google.gson.Gson;
import com.vortextapp.android.VorTextApp;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.libraries.RESTClient;
import com.vortextapp.android.libraries.RESTResponder;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.AdsModel;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.models.data.AdReceipt;
import com.vortextapp.android.models.tables.AdActivity;
import com.vortextapp.android.models.tables.Ads;
 
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
 
public class AdReceiptService extends Service {
     
    private static final String TAG = "VorText: AdReceiptService"; // for debug label
    private PreferencesModel prefs = PreferencesModel.getInstance();
    private Response rest_responder;
    private final IBinder mBinder = new MyBinder();
    List<AdActivity> ad_activities;
    private AdsModel ads_model;
    private Context context;
 
    
    public int onStartCommand(Intent intent, int flags, int startId) {
    	//android.os.Debug.waitForDebugger();
    	if(Globals.DEBUG) Log.v(TAG, "started");

        context = this.getApplicationContext();
    	if (context == null){
        	if(Globals.DEBUG) Log.v(TAG, "context is null");
        	stopSelf();// don't start service if context is null
        }
        else{
        //ads_model = new AdsModel(context);
        	ads_model = new AdsModel(context);
        
        ad_activities = ads_model.getUnsynchronizedActivities();
        //android.os.Debug.waitForDebugger();
        
        if(Globals.DEBUG) Log.v(TAG, "checking for receipts to post...");
        
        if(ad_activities.isEmpty() == true){
        	prefs.setAreReceiptsSynced(true);
        	if(Globals.DEBUG) Log.v(TAG, "no receipts to post");
        	stopSelf(); // we are done for now
        } else {
        	if(Globals.DEBUG) Log.v(TAG, "found receipts, updating");
        	prefs.setAreReceiptsSynced(false);
            update();	
        }}
        
        return Service.START_NOT_STICKY;
        
    }
 
    
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }
     
    
    public void onDestroy() {
        super.onDestroy();
        //android.os.Debug.waitForDebugger();
        close();
        if(Globals.DEBUG) Log.v(TAG, "destroyed");
    }
 
    public class MyBinder extends Binder {
        AdReceiptService getService() {
            return AdReceiptService.this;
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public void update(){
    	//android.os.Debug.waitForDebugger();	
		List<AdReceipt> receipts = new ArrayList<AdReceipt>();
		
		for (AdActivity activity : ad_activities) {
	    	//TODO eventually when opt-in stuff works, we'll need to specify different hash ids ...
	    	// CACHED, VIEWED, CLICKED
			Ads myAd = activity.getAd();
			if (myAd != null){
			Integer advertID = myAd.getAdvertiserId();		
	    	AdReceipt receipt = new AdReceipt(	prefs.getAccountHashId(), 
	    										prefs.getAccountHashId(),
												activity.getServerAdId(),
												advertID,
												activity.getActivityTypeId(),
												TimeManager.getTimeStampForServer(activity.getDate())
											 );
	    	receipts.add(receipt);}

    	}
		//android.os.Debug.waitForDebugger();
    	Gson gson = new Gson();
		String json_receipts = gson.toJson(receipts);

    	//String service = URLCollection.API + "post_ad_receipts/";
        MultiValueMap parts = new LinkedMultiValueMap();
        parts.add("hash_id", prefs.getAccountHashId());
        parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);
        
        parts.add("receipts", json_receipts); // array of AdReceipt
         
        rest_responder = new Response();
        RESTClient client = new RESTClient(rest_responder, parts);
        if (client.isOnline())
        	//client.execute(service);
        	client.execute("https://www.rocketlinkmobile.com/portal/api10/post_ad_receipts/");

    }
 
    public void close() {
    	ads_model.close();
    }
     
    private class Response implements RESTResponder {
        public void handleRESTResponse(String response){
        	//android.os.Debug.waitForDebugger();
        	if(Globals.DEBUG) Log.v(TAG, response);
             
            Gson gson = new Gson();
            ReceiptResponse receipt_response = gson.fromJson(response, ReceiptResponse.class);
            //if(Globals.DEBUG) Log.v(TAG, "AdsResponse "+o.wereAdsFound()+" ad pieces array length "+o.ads.size());
 
            if(receipt_response.wasPostSuccessful()==true){
            	if(Globals.DEBUG) Log.v(TAG, "post was successful");
                // TODO this really needs to be more bullet proof
                boolean result = ads_model.setActivitiesToSynchronized(ad_activities);
                
            } else {
                //
            	if(Globals.DEBUG) Log.e(TAG, "No receipts were posted to server");
            }
            
            stopSelf(); // we are done for now

        }
    }
     
    public class ReceiptResponse {
         
        private int success;
         
        public Boolean wasPostSuccessful() {
            if(success==1){
                return true;
            } else {
                return false;
            }
        }
         
    }
     
     
 
}