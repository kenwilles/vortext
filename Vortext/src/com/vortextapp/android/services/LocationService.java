package com.vortextapp.android.services;
 
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
 
import com.google.gson.Gson;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.libraries.RESTClient;
import com.vortextapp.android.libraries.RESTResponder;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.AdsModel;
import com.vortextapp.android.models.PreferencesModel;
 
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
 
public class LocationService extends Service {
 
    private static final String TAG = "VorText: LocationService"; // for debug label
    public static final float HOUR_TIME_ELAPSED_UNTIL_POST_AUTHORIZED = .2f; // every 12 minutes in the hour
    private final IBinder mBinder = new MyBinder();
     
    // Acquire a reference to the system Location Manager
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Response rest_responder;
    private PreferencesModel prefs;
    private Context context;
 
    
    public int onStartCommand(Intent intent, int flags, int startId) {
         
        context = this.getApplicationContext();
//        if (context == null){
//    		context = this;
//    }
//        if (context == null){
//    		context = getBaseContext()	;
//    }
        //if (context == null){
    	if (context == null){
        	if(Globals.DEBUG) Log.v(TAG, "context is null");
        	stopSelf();// don't start service if context is null
        }
        else{

            prefs = PreferencesModel.getInstance();
            if(prefs.isLoggedIn()==true ){
            	if(Globals.DEBUG) Log.v(TAG, "started");
                //android.os.Debug.waitForDebugger();
                setTimeStarted(TimeManager.getCurrentSyncronizedTimeStamp());
                startLocationService();
            } else {
            	stopSelf(); // we are done for now
            }
             
            return Service.START_NOT_STICKY;
        }
        return Service.START_NOT_STICKY;
    }
 
    
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }
     
    
    public void onDestroy() {
        super.onDestroy();
    }
 
    public class MyBinder extends Binder {
        LocationService getService() {
            return LocationService.this;
        }
    }
 
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void postLocation(double lat, double lon){
        //android.os.Debug.waitForDebugger();
    	if(Globals.DEBUG) Log.v(TAG, "post location (lat) "+lat+" (lon) "+lon);
         
        //String service = URLCollection.API + "post_location/";
        //String service = "portal/api10/post_location/";
        MultiValueMap parts = new LinkedMultiValueMap();
         
        String hash_id = prefs.getAccountHashId();
         
        parts.add("hash_id", hash_id);
        parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);
        String llat = String.valueOf(lat);
        String llong = String.valueOf(lon);
        parts.add("lat", String.valueOf(lat)); // this cannot be double
        parts.add("lon", String.valueOf(lon)); // this cannot be double
     
        rest_responder = new Response();
        RESTClient client = new RESTClient(rest_responder, parts);
        if (client.isOnline()) 	
        	//client.execute(service);
        	//android.os.Debug.waitForDebugger();
        	client.execute("https://www.rocketlinkmobile.com/portal/api10/post_location/");
 
    }
     
    private void startLocationService(){
        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
 
            public void onProviderEnabled(String provider) {
            	if(Globals.DEBUG) Log.v(TAG, "onProviderEnabled "+provider);
            }
 
            public void onProviderDisabled(String provider) {
            	if(Globals.DEBUG) Log.v(TAG, "onProviderDisabled "+provider);
            }
 
            
            public void onLocationChanged(Location location) {
                //android.os.Debug.waitForDebugger();
                if(isLocationPostUpdateAllowable()){
                    postLocation(location.getLatitude(), location.getLongitude());
                    setTimeStarted(TimeManager.getCurrentSyncronizedTimeStamp());
                }
            }
 
            
            public void onStatusChanged(String provider, int status, Bundle extras) {
                // TODO Auto-generated method stub
            	if(Globals.DEBUG) Log.v(TAG, "onStatusChanged "+provider+status);
            }
          };
         
        // Register the listener with the Location Manager to receive location updates
        
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);      
        //@Hai Check to make sure GPS is enable
        /*if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
    	    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    	}
    	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);*/
       try { 
    	   //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    	   if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
       	    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
       	}
       	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
       } catch (IllegalArgumentException e){
           if(Globals.DEBUG) Log.e(TAG, e.getLocalizedMessage());
       } catch (SecurityException e){
           if(Globals.DEBUG) Log.e(TAG, e.getLocalizedMessage());
       }
       
       
         /*
        try {
            geoLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(geoLocation != null){
                postLocation(geoLocation.getLatitude(), geoLocation.getLongitude());
            }
        } catch (IllegalArgumentException e){
            if(Globals.DEBUG) Log.e(TAG, e.getLocalizedMessage());
        } catch (SecurityException e){
            if(Globals.DEBUG) Log.e(TAG, e.getLocalizedMessage());
        }*/
    }
     
    private class Response implements RESTResponder {
        public void handleRESTResponse(String response){ 
            Gson gson = new Gson();
            LocationResponse location_response = gson.fromJson(response, LocationResponse.class);
            //android.os.Debug.waitForDebugger();
            if(location_response.getStatus()==true){
            	if(Globals.DEBUG) Log.v(TAG, "Location successfully posted");
            } else {
            	if(Globals.DEBUG) Log.e(TAG, "Location was NOT posted");
            }
            
            stopSelf(); // we are done for now
            
        }
    }
     
    public class LocationResponse {     
        private String success = null;
        public Boolean getStatus(){
            if(success.equals("1")){
                return true;
            } else {
                return false;
            }
        }
    }
     
    private void setTimeStarted(long new_time){
        //android.os.Debug.waitForDebugger();
        long time_marker = prefs.getMarkedTime();
        float hours_elapsed = TimeManager.getHourDifference(new_time, time_marker); 
        if(hours_elapsed > HOUR_TIME_ELAPSED_UNTIL_POST_AUTHORIZED){
            prefs.markCurrentTime();  
        }
 
    }
     
    private Boolean isLocationPostUpdateAllowable(){
        //android.os.Debug.waitForDebugger();
        long now = TimeManager.getCurrentSyncronizedTimeStamp();
        long then = prefs.getMarkedTime();
        float elapsed = TimeManager.getHourDifference(now, then);
         
        if(prefs.isFreshInstall()==true){
            elapsed = HOUR_TIME_ELAPSED_UNTIL_POST_AUTHORIZED+0.2f; // the very first time
        }
         
        if(elapsed > HOUR_TIME_ELAPSED_UNTIL_POST_AUTHORIZED){
            return true;
        } else {
            return false;
        }
    }
     
}