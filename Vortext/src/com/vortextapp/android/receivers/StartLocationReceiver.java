package com.vortextapp.android.receivers;

import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.services.LocationService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartLocationReceiver extends BroadcastReceiver{
	
	
	public void onReceive(Context context, Intent intent) {
		PreferencesModel prefs = PreferencesModel.getInstance();
		if(prefs.areAdsEnabled()==true && prefs.isLoggedIn()==true && context != null){
			Intent service = new Intent(context, LocationService.class);
			context.startService(service);
		}
	}
}
