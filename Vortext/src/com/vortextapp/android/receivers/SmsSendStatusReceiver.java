package com.vortextapp.android.receivers;

//import com.vortextapp.android.activities.MessageThreads;

import com.vortextapp.android.config.Globals;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class SmsSendStatusReceiver extends BroadcastReceiver{
	
	public static final String TAG = "VorText: SmsSendStatusReceiver";
	public static final String ERROR_CODE = "errorCode";
	public static  int errorcode;
	
	
	public void onReceive(Context context, Intent intent) {
		
		//android.os.Debug.waitForDebugger();
		String error = intent.getStringExtra(ERROR_CODE);
		this.errorcode = getResultCode();
		
		if(error!=null){
			
			if(Globals.DEBUG) Log.e(TAG, "message not sent, error code: "+error);
			
			//Toast.makeText(context, "Error sending message: "+error, Toast.LENGTH_LONG);

		} else {
			
			if(Globals.DEBUG) Log.v(TAG, "message was sent!");

			//Toast.makeText(context, "Message successfully sent", Toast.LENGTH_SHORT);
			
			/*Intent threads_intent = new Intent();
			threads_intent.setClass(context, MessageThreads.class);
			context.startActivity(threads_intent);
			*/

		}
		
	}
	public int geterror()
	{
		return this.errorcode;
	}
}
