package com.vortextapp.android.receivers;

import providers.ContactsOnPhone;

import com.vortextapp.android.activities.MessageThreads;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.SMS;
import com.vortextapp.android.models.MessageModel;
import com.vortextapp.android.models.PreferencesModel;
//import com.vortextapp.android.services.SmsReceiverService;
import com.vortextapp.android.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsReceiver extends BroadcastReceiver {
	
  private static final String TAG = "VorText: SmsReceiver"; // for debug label
  private ContactsOnPhone contacts;
  private PreferencesModel prefs;

  
  public void onReceive(Context context, Intent intent) {
	  
	if(Globals.DEBUG) Log.v(TAG, "SMSReceiver: onReceive()");
	prefs = PreferencesModel.getInstance();
	if(prefs.isLoggedIn()==true){
		
		contacts = new ContactsOnPhone(context.getContentResolver());
		
		Bundle extras = intent.getExtras();

	    if (extras == null)
	        return;

	    Object[] pdus = (Object[]) extras.get("pdus");

	    for (int i = 0; i < pdus.length; i++) {
	        SmsMessage message = SmsMessage.createFromPdu((byte[]) pdus[i]); 
	        String fromAddress = message.getOriginatingAddress();
	        String fromAddressName = contacts.getContactName(fromAddress);
	        String messageBody = message.getMessageBody().toString();

	        if(Globals.DEBUG) Log.i(TAG, "From: " + fromAddress + " message: " + messageBody);
	        
	        // store message in db
	        Boolean result = storeMessage(context, fromAddress, messageBody);
	        
	        // notify if that's ok
	        if(result==true && prefs.areNotificationsEnabled()==true){
	        	addNotification(context, fromAddress, fromAddressName, messageBody);
	        }
	        
	    }
		
		// TODO this could also start a background service as shown below...
		
	    //intent.setClass(context, SmsReceiverService.class);
	    //intent.putExtra("result", getResultCode());

	    /*
	     * This service will process the activity and show the popup (+ play notifications)
	     * after it's work is done the service will be stopped.
	     */
	    //SmsReceiverService.beginStartingService(context, intent);
	}
	//abortBroadcast();

  }
  
  private Boolean storeMessage(Context context, String fromAddress, String message){
	  //store in db
	  MessageModel messages = MessageModel.getInstance();
	  Boolean result = messages.insertNewIncoming(fromAddress, message, false,true);
	  return result;
  }
  
  private void addNotification(Context context, String fromAddress, String fromAddressName, String message) {
	
	  int notificationId = prefs.getNotificationId();
	  prefs.incrementNotificationCount();

	  if(Globals.DEBUG) Log.v(TAG, "addNotification notificationId: " + notificationId);
		  
	  // Package status bar notification
	  // http://developer.android.com/guide/topics/ui/notifiers/notifications.html	
	  String ns = Context.NOTIFICATION_SERVICE;
	  NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(ns);
		
	  int icon = R.drawable.ic_notification;
	  long when = System.currentTimeMillis();
	  
	  //android.os.Debug.waitForDebugger();
	  String notification_message = "";
	  String notification_message_bar = "";
	  String from = "";
	  int notification_count = prefs.getNotificationCount();
	  if(prefs.getNotificationCount() > 1){
		  from = "New VorTexts";
		  notification_message = prefs.getNotificationCount()+" unread VorTexts";
		  notification_message_bar = fromAddressName+": "+message;
	  } else {
		  from = fromAddressName;
		  notification_message = from+": "+message;
		  notification_message_bar = notification_message;
	  }
	  
	  Notification notification = new Notification(icon, notification_message_bar, when);
	  //PendingIntent contentIntent = createDisplayMessageIntent(context, fromAddressName, message, notificationId);
	  //notification.setLatestEventInfo(context, fromAddressName, message, contentIntent);
	  
	  PendingIntent contentIntent = createDisplayMessageIntent(context, from, notification_message, notificationId);
	  notification.setLatestEventInfo(context, from, notification_message, contentIntent);
	  
	  notification.defaults = Notification.DEFAULT_ALL;
	  notification.flags = Notification.FLAG_AUTO_CANCEL;
	  mNotificationManager.notify(notificationId, notification);
		
      /*Notification.Builder notification = new Notification.Builder(context)
          .setTicker(message)
          .setWhen(System.currentTimeMillis())
          .setContentTitle(fromAddress)
          .setContentText(message)
          .setSmallIcon(R.drawable.stat_notify_sms)
          .setContentIntent(createDisplayMessageIntent(context, fromAddress, message,
                  notificationId));
      */

  }

  private PendingIntent createDisplayMessageIntent(Context context, String fromAddress, String message, int notificationId) {
      // Trigger the main activity to fire up a dialog that shows the received messages
      Intent di = new Intent();
      di.setClass(context, MessageThreads.class);
      di.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
      
      di.putExtra(SMS.FROM_ADDRESS_EXTRA, fromAddress);
      di.putExtra(SMS.MESSAGE_EXTRA, message);
      di.putExtra(SMS.NOTIFICATION_ID_EXTRA, notificationId);

      // This line is needed to make this intent compare differently than the other intents
      // created here for other messages. Without this line, the PendingIntent always gets the
      // intent of a previous message and notification.
      di.setType(Integer.toString(notificationId));

      PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, di, 0);
      return pendingIntent;

  }

}