package com.vortextapp.android.receivers;

import java.util.Calendar;

import com.vortextapp.android.config.Globals;
import com.vortextapp.android.models.PreferencesModel;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScheduleServicesReceiver extends BroadcastReceiver {
	
	private static final String TAG = "VorText: ScheduleServicesReceiver"; // for debug label
	private static final Integer EVERY_5_MIN = 1000 * 60 * 5;
	private static final Integer EVERY_2_MIN = 1000 * 60 * 2;
	private static final Integer EVERY_1_MIN = 1000 * 60 * 1;
	private static final Integer EVERY_30_SEC = 1000 * 30;
	private PreferencesModel prefs;
	
	
	public void onReceive(Context context, Intent intent) {
		//android.os.Debug.waitForDebugger();
		//if(intent.hasCategory("android.intent.category.ALTERNATIVE")){
			initServices(context);
			if(Globals.DEBUG) Log.v(TAG, "onReceive");
		//}
		
	}
	
	private void initServices(Context context){
		//android.os.Debug.waitForDebugger();
		PreferencesModel prefs = PreferencesModel.getInstance();
        
        Intent location_intent = new Intent(context, StartLocationReceiver.class);
        Intent ad_intent = new Intent(context, StartAdReceiver.class);
        Intent ad_receipt_intent = new Intent(context, StartAdReceiptReceiver.class);
        
        // Fire at beginning
        //////////////////////////////////////
        Log.v(TAG, "FIRE services");
        context.sendBroadcast(location_intent); 
        context.sendBroadcast(ad_intent); 
        //////////////////////////////////////
        
        AlarmManager alarm_location_service = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pending_location_service = PendingIntent.getBroadcast(context, 0, location_intent, PendingIntent.FLAG_CANCEL_CURRENT);
         
        AlarmManager alarm_ad_service = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pending_ad_service = PendingIntent.getBroadcast(context, 0, ad_intent, PendingIntent.FLAG_CANCEL_CURRENT);
        
        AlarmManager alarm_ad_receipt_service = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pending_ad_receipt_service = PendingIntent.getBroadcast(context, 0, ad_receipt_intent, PendingIntent.FLAG_CANCEL_CURRENT);        
         
        // Start 30 seconds after boot completed
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 30);
        //Longer wait for location, to save battery
        Calendar cal2 = Calendar.getInstance();
        cal2.add(Calendar.MINUTE, 5);
 
        // Fetch every 30 seconds
        // InexactRepeating allows Android to optimize the energy consumption
        alarm_location_service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(), AlarmManager.INTERVAL_HOUR, pending_location_service);
        alarm_ad_service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_HOUR, pending_ad_service);
        alarm_ad_receipt_service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), EVERY_2_MIN, pending_ad_receipt_service);

	}
	
}