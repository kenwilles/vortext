package com.vortextapp.android.receivers;

import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.services.AdService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartAdReceiver extends BroadcastReceiver{
	
	
	public void onReceive(Context context, Intent intent) {
		PreferencesModel prefs = PreferencesModel.getInstance();
		if(prefs.areAdsEnabled()==true && prefs.isLoggedIn()==true && context != null){
 			Intent service = new Intent(context, AdService.class);
			context.startService(service);
		}
	}
}
