package com.vortextapp.android.activities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.vortextapp.android.R;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.SimpleSmsSender;
import com.vortextapp.android.models.DatabaseHelper;
import com.vortextapp.android.models.EarningsModel;
import com.vortextapp.android.models.MessageModel;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.models.tables.Messages;
import com.vortextapp.android.receivers.SmsSendStatusReceiver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ComposeMessage extends Activity {

	private static final String TAG = "VorText: ComposeMessage"; // for debug
																	// label

	protected static final int CONTACT_PICKER_RESULT = 0;
	public MessageModel messages;

	String phoneNumber = "";
	private DatabaseHelper databaseHelper = null;
	private ProgressDialog progress_window;
	//ImageView contactPic;
	Uri u;
	public TextView earnings, earningsPercentage;
	RelativeLayout earningsPercentageLayout;

	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		PreferencesModel prefs = PreferencesModel.getInstance();
		if (prefs.isLoggedIn() == true) {

			messages = MessageModel.getInstance();
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.compose_message);

			// TODO when opt-in is enabled, enable this code
			// Button leftButton = (Button)findViewById(R.id.leftButton);
			// leftButton.setOnClickListener(leftButtonListener);
			// Button rightButton = (Button)findViewById(R.id.rightButton);
			// rightButton.setOnClickListener(rightButtonListener);
			ImageView sendButton = (ImageView) findViewById(R.id.sendMessage);
			sendButton.setOnClickListener(sendButtonListener);
			Button chooseButton = (Button) findViewById(R.id.choose);
			chooseButton.setOnClickListener(chooseButtonListener);

			ImageView vortextLogo = (ImageView) findViewById(R.id.vortextLogo);
			vortextLogo.setOnClickListener(vortextLogoListener);

			// contactPic = (ImageView)findViewById(R.id.contactPic);

			// setAd();

		} else {
			Toast.makeText(getApplicationContext(),
					"You must login first to compose messages",
					Toast.LENGTH_LONG).show();
			Intent intent = new Intent();
			intent.setClass(this, SignupOrLogin.class);
			startActivity(intent);
		}
		
		Button coin=(Button)findViewById(R.id.coin);
        coin.setOnClickListener(earningsButtonListener);
		
        earningsPercentageLayout=(RelativeLayout)findViewById(R.id.percentageLayout);
        earningsPercentageLayout.setOnClickListener(earningsButtonListener);
        
        earnings=(TextView)findViewById(R.id.earnings);
        earnings.setOnClickListener(earningsButtonListener);
        
        earningsPercentage=(TextView)findViewById(R.id.earningsPercentage);
        earningsPercentage.setOnClickListener(earningsButtonListener);
        
        setEarningsDisplay();

	}
	
private View.OnClickListener earningsButtonListener = new View.OnClickListener() {

		
		public void onClick(View v) {
			Intent intent = new Intent();
		    intent.setClass(v.getContext(), PaymentOverview.class);
		    v.getContext().startActivity(intent);
		}

	};
private void setEarningsDisplay(){
		
		EarningsModel cash = new EarningsModel(this);
		double total = (int)((cash.getTotalEarnings())*100);
//		DecimalFormat df = new DecimalFormat("##0.00");
//		String pretty_total = df.format(total);
		earnings.setText(String.valueOf((int)total));
		earningsPercentage.setText(String.valueOf((int)total));
		
		Double percentage=(total/Globals.MINIMUM_CLAIM)*100;
		if(percentage>100)
			percentage=100d;
		double progressBarWidth=getResources().getDimension(R.dimen.progress_layout_width)-(2*getResources().getDimension(R.dimen.progress_layout_padding));
		double padding=progressBarWidth-(progressBarWidth*percentage/100);
//		new AlertDialog.Builder(this).setMessage(
//				"total width: "+progressBarWidth+"\n"+
//				"padding: "+(int)padding).create().show();
		
		earningsPercentageLayout.setPadding(0, 0, (int)padding, 0);
		
		cash.close();
	}

	/*
	 * TODO enable this when opt-in feature is implemented private void nextAd()
	 * {
	 * 
	 * setAd();
	 * 
	 * if(position<total_ads-1){ ++position; }
	 * 
	 * }
	 * 
	 * private void prevAd(){
	 * 
	 * setAd();
	 * 
	 * if(position>0){ --position; } else { clearAd(); } }
	 * 
	 * private void setAd(){ TextView adTextView =
	 * (TextView)findViewById(R.id.adTextView);
	 * adTextView.setText(SampleAds.collection[position]); }
	 * 
	 * private void clearAd(){ TextView adTextView =
	 * (TextView)findViewById(R.id.adTextView); adTextView.setText(""); }
	 * 
	 * private View.OnClickListener leftButtonListener = new
	 * View.OnClickListener() {
	 * 
	 *  public void onClick(View v) { prevAd(); } };
	 * 
	 * private View.OnClickListener rightButtonListener = new
	 * View.OnClickListener() {
	 * 
	 *  public void onClick(View v) { nextAd(); } };
	 */

	private View.OnClickListener chooseButtonListener = new View.OnClickListener() {

		
		public void onClick(View v) {
			startPick();
			// Toast.makeText(v.getContext(), "Choose button coming soon",
			// Toast.LENGTH_LONG);
		}
	};

	private void startPick() {

		// startActivityForResult(new Intent(Intent.ACTION_PICK, new
		// Uri("content://contacts")), PICK_CONTACT_REQUEST);

		Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
				Contacts.CONTENT_URI);
		this.startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);
		if (Globals.DEBUG)
			Log.v(TAG, "startPick");
	}

	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (Globals.DEBUG)
			Log.v(TAG, "onActivityResult called");
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case CONTACT_PICKER_RESULT:
				Cursor cursor = null;
				String phone = "";
				List<String> allNumbers = new ArrayList<String>();
				try {
					Uri result = data.getData();
					if (Globals.DEBUG)
						Log.v(TAG, "Got a contact result: " + result.toString());

					// get the contact id from the Uri
					String id = result.getLastPathSegment();

					// query for everything phone
					cursor = getContentResolver().query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?", new String[] { id }, null);

					// String columns[] = cursor.getColumnNames();

					int phoneIndex = cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
					// ContactsContract.CommonDataKinds

					// @Hai: get all the possible numbers
					if (cursor.moveToFirst()) {
						while (true) {
							phoneNumber = cursor.getString(phoneIndex);
							allNumbers.add(cursor.getString(phoneIndex));
							Uri person = ContentUris.withAppendedId(
									ContactsContract.Contacts.CONTENT_URI,
									Long.parseLong(id));
							u = Uri.withAppendedPath(
									person,
									ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
							
							Uri uri2 = ContentUris.withAppendedId(
									ContactsContract.Contacts.CONTENT_URI,
									Long.parseLong(phoneNumber));
							Intent intent = new Intent(Intent.ACTION_CALL);
							intent.setData(Uri.parse(uri2.toString()));
							this.startActivity(intent);
							if (cursor.isLast())
								break;
							else
								cursor.moveToNext();
						}

						if (Globals.DEBUG)
							Log.v(TAG, "Got phone: " + phone);
					} else {
						if (Globals.DEBUG)
							Log.w(TAG, "No results");
					}
				} catch (Exception e) {
					if (Globals.DEBUG)
						Log.e(TAG, "Failed to get phone data", e);
				} finally {
					if (cursor != null) {
						cursor.close();
					}
					final EditText phoneText = (EditText) findViewById(R.id.phoneText);

//					if (u != null) {
//						// do something with the uri if needed
//						// contactPic.setImageURI(u);
//					} else {
//						//contactPic.setImageResource(R.drawable.ic_contact);
//					}
					// @Hai: If there is more than one number show options to
					// select
					final CharSequence[] items = allNumbers
							.toArray(new String[allNumbers.size()]);
					AlertDialog.Builder builder = new AlertDialog.Builder(
							ComposeMessage.this);
					builder.setTitle("Please select a number");
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int item) {
									String selectedNumber = items[item]
											.toString();
									selectedNumber = selectedNumber.replace(
											"-", "");
									phoneText.setText(selectedNumber);
								}
							});
					AlertDialog alert = builder.create();
					if (allNumbers.size() > 1) {
						alert.show();
					} else {
						phone = phoneNumber.toString();
						phone = phone.replace("-", "");
						phoneText.setText(phone);
						if (phone.length() == 0) {
							Toast.makeText(this, "No phone found for contact.",
									Toast.LENGTH_LONG).show();
						}
					}

				}

				break;
			}

		} else {
			if (Globals.DEBUG)
				Log.w(TAG, "Warning: activity result not ok");
		}
	}

	// @Hai thread for checking status of the sending message
	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(this,
					DatabaseHelper.class);
		}
		return databaseHelper;
	}

	private class CheckSentMessageStatus extends
			AsyncTask<String, Integer, String> {
		private Context context;
		private int errorcode;
		SmsSendStatusReceiver status;
		private Long message_ID;
		private String error_string;
		private String sending_number;

		// private Boolean need_refresh;

		public CheckSentMessageStatus(Context c) {
		}

		
		protected void onPreExecute() {

		}

		protected String doInBackground(String... id) {
			// send to sms sender
			sending_number = id[0];
			phoneNumber = sending_number;
			message_ID = messages.insertNewOutgoing(sending_number, id[1]);
			// need_refresh = true;
			
			// Update the imageurl to the contact
			messages.updateContactImage(u, phoneNumber,false);

			return "";
		}

		
		protected void onPostExecute(String result) {

			
			status = new SmsSendStatusReceiver();
			errorcode = status.geterror();

			// Set Error String
			if (Globals.DEBUG)
				Log.v(TAG, "got the error code " + errorcode);
			switch (errorcode) {
			case 0:
				Toast.makeText(getBaseContext(),
						"Message sent to " + sending_number, Toast.LENGTH_SHORT)
						.show();
				// Toast.makeText(getBaseContext(), "SMS sent",
				// Toast.LENGTH_SHORT).show();
				error_string = "Sent";
				// need_refresh = false;
				break;
			case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				Toast.makeText(getBaseContext(),
						"Couldn't send to " + sending_number,
						Toast.LENGTH_SHORT).show();
				// Toast.makeText(getBaseContext(), "Generic failure",
				// Toast.LENGTH_SHORT).show();
				error_string = "Generic failure";
				break;
			case SmsManager.RESULT_ERROR_NO_SERVICE:
				Toast.makeText(getBaseContext(),
						"Couldn't send to " + sending_number,
						Toast.LENGTH_SHORT).show();
				// Toast.makeText(getBaseContext(), "No service",
				// Toast.LENGTH_SHORT).show();
				error_string = "No service";
				break;
			case SmsManager.RESULT_ERROR_NULL_PDU:
				Toast.makeText(getBaseContext(),
						"Couldn't send to " + sending_number,
						Toast.LENGTH_SHORT).show();
				// Toast.makeText(getBaseContext(), "Null PDU",
				// Toast.LENGTH_SHORT).show();
				error_string = "No service";
				break;
			case SmsManager.RESULT_ERROR_RADIO_OFF:
				Toast.makeText(getBaseContext(),
						"Couldn't send to " + sending_number,
						Toast.LENGTH_SHORT).show();
				// Toast.makeText(getBaseContext(), "Radio off",
				// Toast.LENGTH_SHORT).show();
				error_string = "No service";
				break;
			default:
				Toast.makeText(getBaseContext(),
						"Message sent to " + sending_number, Toast.LENGTH_SHORT)
						.show();
				// Toast.makeText(getBaseContext(), "SMS sent",
				// Toast.LENGTH_SHORT).show();
				error_string = "Sent";
				// need_refresh = false;
				break;
			}
			if (Globals.DEBUG)
				Log.v(TAG, "got the error string " + error_string);
			// Update the coresponding message'status in the database
			List<Messages> message_list = new ArrayList<Messages>();
			Messages messages2 = new Messages();
			messages2.setId(message_ID);
			messages2.setIsRead(true);
			Dao<Messages, Long> messagesDao;
			if (Globals.DEBUG)
				Log.v(TAG, "about to update the database for ID: " + message_ID);
			try {

				messagesDao = getHelper().getMessagesDao();
				message_list = messagesDao.queryForMatching(messages2);
				ListIterator<Messages> message_itr = message_list
						.listIterator();
				while (message_itr.hasNext()) {

					Messages m = message_itr.next();
					m.setStatus(error_string);
					messagesDao.update(m);
					message_itr.set(m);
					if (Globals.DEBUG)
						Log.v(TAG, "done update" + message_ID + error_string);

				}
				if (Globals.DEBUG)
					Log.v(TAG, "done update" + message_ID);
				// Refresh the message list
				// if (need_refresh)
				// messagesObserver.startNotify();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}

	}

	private View.OnClickListener sendButtonListener = new View.OnClickListener() {

		
		public void onClick(View v) {


			TextView phoneText = (TextView) findViewById(R.id.messageBox);
			String destination = phoneText.getText() + "";
			//check for empty number
			if (!destination.equals("")){
				progress_window = ProgressDialog.show(ComposeMessage.this, "",
						"Sending message...", true);
				EditText messageBox = (EditText) findViewById(R.id.messageBox);
				// TextView adTextView = (TextView)findViewById(R.id.adTextView);
				String msg = messageBox.getText().toString();
				// +"\n\n"+adTextView.getText().toString();
				// Toast.makeText(v.getContext(),
				// "message sent to "+phoneText.getText().toString(),
				// Toast.LENGTH_SHORT).show();

				// send to sms sender
				SimpleSmsSender sender = new SimpleSmsSender(v.getContext());
				sender.send(destination, msg);

				// TODO this should really be recorded when we know what happens to
				// the message
				// Long message_ID = messages.insertNewOutgoing(destination, msg);

				// Go back to message threads
				// Intent intent_back = new Intent();
				// intent_back.setClass(v.getContext(), MessageThreads.class);
				// startActivity(intent_back);
				Intent returnIntent = new Intent();
				returnIntent .putExtra("result",RESULT_OK);
				setResult(RESULT_OK,returnIntent);
				
				// stop progress window
				progress_window.dismiss();
				
				finish();
				CheckSentMessageStatus checkstatus = new CheckSentMessageStatus(
						v.getContext());
				checkstatus.execute(new String[] { destination, msg });
				
			}
			else{
				Toast.makeText(getBaseContext(),
						"Receiver can't be empty!", Toast.LENGTH_SHORT)
						.show();
			}		
		}
	};

	private View.OnClickListener vortextLogoListener = new View.OnClickListener() {

		
		public void onClick(View v) {
			
			Intent intent = new Intent();
			intent.setClass(v.getContext(), MessageThreads.class);
			startActivity(intent);
			finish();
		}
	};

	
	protected void onStop() {
		super.onStop();
		// this is called when the contact picker is launched.
	}

	
	protected void onDestroy() {
		super.onDestroy();
		messages.close();
	}

	
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	
	public boolean onOptionsItemSelected(MenuItem item) {
		//:TODO find a better way to accomplish this
		//This code only works with higher adk
		//switch (item.getItemId()) {
		//case R.id.settings:
			startActivity(new Intent(this, Preferences.class));
			return true;
		//}
		//return false;
	}

}
