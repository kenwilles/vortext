package com.vortextapp.android.activities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Observable;
import java.util.Observer;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.vortextapp.android.R;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.SimpleSmsSender;
import com.vortextapp.android.models.DatabaseHelper;
import com.vortextapp.android.models.EarningsModel;
import com.vortextapp.android.models.MessageModel;
import com.vortextapp.android.models.MessagesObserver;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.models.tables.Messages;
import com.vortextapp.android.models.tables.Threads;
import com.vortextapp.android.receivers.SmsSendStatusReceiver;

public class MessageThread extends ListActivity {

	private static final String TAG = "VorText: MessageThread"; // for debug
	private MessageModel messages;
	MessageThreadAdapter adapter;
	String phone_number;
	private ProgressDialog progress_window;
	private DatabaseHelper databaseHelper = null;
		
	private Integer thread_id;
	public ListWatch message_list_watcher;
	public TextView earnings, earningsPercentage;
	RelativeLayout earningsPercentageLayout;
	private MessagesObserver messagesObserver = MessagesObserver.getInstance();

	
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		PreferencesModel prefs = PreferencesModel.getInstance();
		if(prefs.isLoggedIn()==true){

			requestWindowFeature(Window.FEATURE_NO_TITLE);
	
			setContentView(R.layout.message_thread);
			prefs.resetNotificationCount();
			if(Globals.DEBUG) Log.v(TAG, "onCreate called");
			
			Bundle extras = getIntent().getExtras();
			thread_id = extras.getInt("threadId"); // used for finding conversation list
			phone_number = extras.getString("phoneNumber"); // used to reply
			
			//Get name
			TextView contact_name = (TextView)findViewById(R.id.contact_name);
			contact_name.setText(extras.getString("contactName") );
			contact_name.setVisibility(View.VISIBLE);
	
			if(Globals.DEBUG) Log.v(TAG, "thread id was set to "+thread_id);		
			if(Globals.DEBUG) Log.v(TAG, "phone number that was set "+phone_number);
			
			messages = MessageModel.getInstance();
			Threads conversation_thread = new Threads();
			conversation_thread.setId(thread_id);
			// get messages with this thread
			// update all messages in this thread as read
			// increment money if there were ads in messages
			// lots of stuff to do here
			messages.setThreadMessages(conversation_thread);
	
			adapter = new MessageThreadAdapter(this, R.layout.message_detail_from_me, messages.list);
			setListAdapter(adapter);
			
			message_list_watcher = new ListWatch(); // this is supposed to refresh list when changes occur to it
			messagesObserver.addObserver(message_list_watcher);
	
			ImageButton profileFriend = (ImageButton) findViewById(R.id.profileBtn);
			profileFriend.setOnClickListener(profileButtonListener);
			
			ImageView backButton = (ImageView)findViewById(R.id.header_bar).findViewById(R.id.vortextLogo);
			backButton.setOnClickListener(homeButtonListener);
	
			Button coin=(Button)findViewById(R.id.coin);
	        coin.setOnClickListener(earningsButtonListener);
	        
	        earningsPercentageLayout=(RelativeLayout)findViewById(R.id.percentageLayout);
	        earningsPercentageLayout.setOnClickListener(earningsButtonListener);
	        
	        earnings=(TextView)findViewById(R.id.earnings);
	        earnings.setOnClickListener(earningsButtonListener);
	        
	        earningsPercentage=(TextView)findViewById(R.id.earningsPercentage);
	        earningsPercentage.setOnClickListener(earningsButtonListener);
	        
	        setEarningsDisplay();
			
			
			
			/*
			Button leftButton = (Button)findViewById(R.id.leftButton);
	        leftButton.setOnClickListener(leftButtonListener);
	        Button rightButton = (Button)findViewById(R.id.rightButton);
	        rightButton.setOnClickListener(rightButtonListener);
	        */
	        ImageView sendButton = (ImageView)findViewById(R.id.sendMessage);
	        sendButton.setOnClickListener(sendButtonListener);
	        
	        //setAd();
	        
	        
	        //Set listview so that it Always scroll down to bottom
	        ListView list = getListView();
	        list.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
	        list.setStackFromBottom(true);
	        	        
		} else {
			Toast.makeText(getApplicationContext(), "You must login first to see messages", Toast.LENGTH_LONG).show();
			Intent intent = new Intent();
		    intent.setClass(this, SignupOrLogin.class);
		    startActivity(intent);
		}

	}

	
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// This method will be called when an item in the list is selected.
	}

	
	protected void onStop() {
		super.onStop();
		// finish();
	}
	
	
	protected void onDestroy() {
		super.onDestroy();
		messages.close();
	}

	private View.OnClickListener profileButtonListener = new View.OnClickListener() {

		
		public void onClick(View v) {
			Toast.makeText(v.getContext(), "In the future, this button will give you the controls to send opt-in ads with your outgoing personal message",
			Toast.LENGTH_LONG).show();
			 //TelephonyManager lTelephonyManager = (TelephonyManager) VorTextApp.getApp().getSystemService(Context.TELEPHONY_SERVICE);
			 //int call_state = lTelephonyManager.getCallState();
			 //if(Globals.DEBUG) Log.v(TAG, String.valueOf(call_state));
			//Intent intent = new Intent();
			//intent.setClass(v.getContext(), ComposeMessage.class);
			//startActivity(intent);
		}
	};
	
	private View.OnClickListener homeButtonListener = new View.OnClickListener() {

		
		public void onClick(View v) {
			
			Intent intent = new Intent();
			intent.setClass(v.getContext(), MessageThreads.class);
			startActivity(intent);
			finish();
		}
	};

	

	/*
	private void nextAd() {
		
		setAd();
		
		if(position<total_ads-1){
			++position;
		}
		
	}
	
	private void prevAd(){
		
		setAd();
		
		if(position>0){
			--position;
		} else {
			clearAd();
		}
	}
	
	
	private void setAd(){
		TextView adTextView = (TextView)findViewById(R.id.adTextView);
		adTextView.setText(SampleAds.collection[position]);
	}
	
	private void clearAd(){
		TextView adTextView = (TextView)findViewById(R.id.adTextView);
		adTextView.setText("");
	}
	
	
	private View.OnClickListener leftButtonListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			prevAd();
		}
	};
	
	private View.OnClickListener rightButtonListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			nextAd();
		}
	};
	*/
	
	private View.OnClickListener sendButtonListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			
			//progress_window = ProgressDialog.show(MessageThread.this, "", "Sending message...", true);
			
			TextView messageBox = (TextView)findViewById(R.id.messageBox);
			String message = messageBox.getText().toString();
			messageBox.setText("");
			messageBox.clearFocus();	
			String message_Id = messages.insertNewOutgoing(phone_number, message).toString();
			
			
//			SmsSendStatusReceiver status ;
//			status = new SmsSendStatusReceiver();
//			int errorcode = status.geterror();
			if(message!=null){
				//if(message!=""){

					SendMessageTask sendtask = new SendMessageTask(v.getContext(), message);
					sendtask.execute(message_Id);
					


				//}
			}

						       

		}
	};
	
	//@Hai thread for checking status of the sending message
	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
		}
		return databaseHelper;
	}
	
	private class CheckSentMessageStatus extends AsyncTask<String, Integer, String>{
		private Context context;
		private int errorcode;
		SmsSendStatusReceiver status ;
		private Long message_ID;
		private String error_string;
		private Boolean need_refresh;
		private String sending_number;
		
		public CheckSentMessageStatus(Context c){
		}
		

		
	    protected void onPreExecute() {
			
		}
		protected String doInBackground(String... id) {
			 // send to sms sender
			sending_number = id[0];
			//message_ID = messages.insertNewOutgoing(sending_number, id[2]);
			message_ID = Long.parseLong(id[2]);
			need_refresh = true;
			status = new SmsSendStatusReceiver();
			errorcode = status.geterror();
			
			
			return "";
		}
protected void onPostExecute(String result) {
			// stop progress window
			
			//messagesObserver.startNotify();
			//Set Error String
			if(Globals.DEBUG) Log.v(TAG, "got the error code "+errorcode);
			switch (errorcode)
            {
            case 0:
//            	Toast.makeText(getBaseContext(), "Message sent to "+sending_number, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getBaseContext(), "SMS sent", 
//                        Toast.LENGTH_SHORT).show();
                error_string = "Sent";
                need_refresh = false;
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//            	Toast.makeText(getBaseContext(), "Couldn't send to "+sending_number, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getBaseContext(), "Generic failure", 
//                        Toast.LENGTH_SHORT).show();
            	error_string = "Generic failure";
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
//            	Toast.makeText(getBaseContext(), "Couldn't send to "+sending_number, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getBaseContext(), "No service", 
//                        Toast.LENGTH_SHORT).show();
            	error_string = "No service";
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
//            	Toast.makeText(getBaseContext(), "Couldn't send to "+sending_number, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getBaseContext(), "Null PDU", 
//                        Toast.LENGTH_SHORT).show();
                error_string = "No service";
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
//            	Toast.makeText(getBaseContext(), "Couldn't send to "+sending_number, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getBaseContext(), "Radio off", 
//                        Toast.LENGTH_SHORT).show();
                error_string = "No service";
                break;
            default :
//            	Toast.makeText(getBaseContext(), "Message sent to "+sending_number, Toast.LENGTH_SHORT).show();
//              Toast.makeText(getBaseContext(), "SMS sent", 
//                      Toast.LENGTH_SHORT).show();
              error_string = "Sent";
              need_refresh = false;
              break;
            }
			adapter.notifyDataSetChanged();
			if(Globals.DEBUG) Log.v(TAG, "got the error string "+error_string);
			//Update the coresponding message'status in the database
			List<Messages> message_list = new ArrayList<Messages>();
			Messages messages = new Messages();
			messages.setId(message_ID);
			messages.setIsRead(true);
			Dao<Messages, Long> messagesDao;
			if(Globals.DEBUG) Log.v(TAG, "about to update the database for ID: "+ message_ID);
			try {

				messagesDao = getHelper().getMessagesDao();
				message_list = messagesDao.queryForMatching(messages);
				ListIterator<Messages> message_itr = message_list.listIterator();
				while(message_itr.hasNext()){

					Messages m = message_itr.next();
					m.setStatus(error_string);
					messagesDao.update(m);
					message_itr.set(m);
					if(Globals.DEBUG) Log.v(TAG, "done update"+ message_ID+ error_string);

				}
				if(Globals.DEBUG) Log.v(TAG, "done update"+ message_ID);
				//Refresh the message list
				if (need_refresh)
					messagesObserver.startNotify();
					//adapter.notifyDataSetChanged();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}



	}
	
	private class SendMessageTask extends AsyncTask<String, Integer, String> {
		
		private Context context;
		private String message;
		private String message_ID;
		
		
		public SendMessageTask(Context c, String m){
			context = c;
			message = m;
		}
		
		
		protected void onPreExecute(){
			//
		}

		
		protected String doInBackground(String... ID) {
			 // send to sms sender
	    	SimpleSmsSender sender = new SimpleSmsSender(getBaseContext());
	    	sender.send(phone_number, message);
	    	message_ID = ID[0];
			return "";
		}

		
		protected void onPostExecute(String result) {
			// stop progress window
			
//			Long message_ID = messages.insertNewOutgoing(phone_number, message);
			//progress_window.dismiss();
			CheckSentMessageStatus checkstatus = new CheckSentMessageStatus(context);
			checkstatus.execute(new String[]{phone_number,message,message_ID});
		}
		
	}

	
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	
	public boolean onOptionsItemSelected(MenuItem item) {
		//:TODO find a better way to accomplish this
		//This code only works with higher adk
		//switch (item.getItemId()) {
		//case R.id.settings:
			startActivity(new Intent(this, Preferences.class));
			return true;
		//}
		//return false;
	}

	private View.OnClickListener earningsButtonListener = new View.OnClickListener() {

		
		public void onClick(View v) {
			Intent intent = new Intent();
		    intent.setClass(v.getContext(), PaymentOverview.class);
		    v.getContext().startActivity(intent);
		}

	};
	
	private class ListWatch implements Observer { 
		public void update(Observable obj, Object arg) { 
			if(Globals.DEBUG) Log.v(TAG, "Update called on ListWatch");
			
			// refresh
			int milliseconds = 1000;
			try {
				Thread.sleep(milliseconds);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			adapter.clear();
			Threads conversation_thread = new Threads();
			conversation_thread.setId(thread_id);
			messages.setThreadMessages(conversation_thread);
	        adapter = new MessageThreadAdapter(MessageThread.this, R.layout.message_threads_item, messages.list);
	        setListAdapter(adapter);
			adapter.notifyDataSetChanged();
			
			setEarningsDisplay();
			
			
			
			//

		} 
	} 
	
	private void setEarningsDisplay(){
		
		EarningsModel cash = new EarningsModel(this);
		double total = (int)((cash.getTotalEarnings())*100);
//		DecimalFormat df = new DecimalFormat("##0.00");
//		String pretty_total = df.format(total);
		earnings.setText(String.valueOf((int)total));
		earningsPercentage.setText(String.valueOf((int)total));
		
		Double percentage=(total/Globals.MINIMUM_CLAIM)*100;
		if(percentage>100)
			percentage=100d;
		double progressBarWidth=getResources().getDimension(R.dimen.progress_layout_width)-(2*getResources().getDimension(R.dimen.progress_layout_padding));
		double padding=progressBarWidth-(progressBarWidth*percentage/100);
//		new AlertDialog.Builder(this).setMessage(
//				"total width: "+progressBarWidth+"\n"+
//				"padding: "+(int)padding).create().show();
		
		earningsPercentageLayout.setPadding(0, 0, (int)padding, 0);
		
		cash.close();
	}

}
