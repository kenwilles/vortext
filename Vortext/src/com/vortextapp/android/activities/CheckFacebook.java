package com.vortextapp.android.activities;

import com.google.gson.Gson;
import com.vortextapp.android.R;
import com.vortextapp.android.config.Actions;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.libraries.RESTClient;
import com.vortextapp.android.libraries.RESTResponder;
import com.vortextapp.android.models.EarningsModel;
import com.vortextapp.android.models.LoginModel;
import com.vortextapp.android.models.PreferencesModel;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;
import android.util.Log;

public class CheckFacebook extends Activity {

	private static final String TAG = "VorText: CheckFacebook Login"; // for debug label
	private ProgressDialog progress_window;
	private LoginModel session;
	private PreferencesModel prefs = PreferencesModel.getInstance();
	private String email;
	private String facebook_id;
	private Response rest_responder;
	Button reset;

	
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup_or_login);
		email = prefs.getAccountEmail();
		facebook_id = prefs.getAccountFacebookId();
		session = new LoginModel(this);
		startLoginServiceCall(email,facebook_id);
		
	
	}
	
	private void startLoginServiceCall(String email,String facebook_id) {

		String email_url_encoded = null;
		try {
			email_url_encoded = URLEncoder.encode(email,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(Globals.DEBUG) Log.v(TAG, "email_url_encoded: " + email_url_encoded
				+ " facebook_id_encoded: " + facebook_id);
		progress_window = ProgressDialog.show(CheckFacebook.this, "",
				"Logging in, please wait", true);

		MultiValueMap parts = new LinkedMultiValueMap();
		parts.add("email", email_url_encoded);
		parts.add("id", facebook_id);
		parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);

		rest_responder = new Response();
		RESTClient client = new RESTClient(rest_responder, parts);
		if (client.isOnline()) {
			client.execute("https://www.rocketlinkmobile.com/portal/api10/facebook_signup/");
		} else {
			progress_window.dismiss();
			new AlertDialog.Builder(this)
					.setTitle("Connection Error!")
					.setMessage(
							"Please connect your device to the internet for using this function! ")
					.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
						int which) {
						}
					}).show();
		}

	}

	private class Response implements RESTResponder {
		public void handleRESTResponse(String response) {
			// TextView adTextView = (TextView)findViewById(R.id.adTextView);
			// adTextView.setText(result);
			
			Gson gson = new Gson();
			LoginResponse o = new LoginResponse();
			try {
				 o = gson.fromJson(response, LoginResponse.class);
			} catch (Exception e) {
				// TODO: handle exception
				o.login_result = "failed";
			}
			
			if (!o.login_result.toString().equals("failed"))
			{
			if(Globals.DEBUG) Log.v(TAG, "handleRESTResponse LoginResponse getLoginStatus: " + o.getLoginStatus());
			//progress_window.dismiss();
			}else
			{
				progress_window.dismiss();
				Toast.makeText(getApplicationContext(), "Login failed",
						Toast.LENGTH_LONG).show();
				reset.setVisibility(1);
				//goTo("startover");
			}
			if(o.getLoginStatus()){
				
				//TODO need to store hash_id here
				String myHash = o.getHashId();
				Boolean result = session.login(myHash, email);
				if(result==true){
					prefs.saveAsLoggedIn(myHash, email);
					
					// kick the scheduler into gear, assuming first run and that phone has not rebooted, 
					// reboot will register scheduler, so calling this service explicitly is not
					// necessary.
					///////////////////////////////////////////////////////////
					Intent intent = new Intent(Actions.START_SERVICE_SCHEDULE);
					intent.addCategory("android.intent.category.ALTERNATIVE");
					getApplicationContext().sendBroadcast(intent);
					///////////////////////////////////////////////////////////
					progress_window.dismiss();
					Toast.makeText(getApplicationContext(), "Login was successful", Toast.LENGTH_LONG).show();
					//Start syncing the balance with server
					 JSONObject json = getJSONfromURL("https://www.rocketlinkmobile.com/portal/api10/get_current_earnings");
					    int successful= 0;
						try {
							successful = json.getInt("success");
							
							 if(successful ==1){
								 String current_balance_string = json.getString("total");
									Double current_balance = Double.parseDouble(current_balance_string);
								    if(current_balance !=0)
								    {
								    	Context context = getApplicationContext();
								    	if(context == null)
								    		context = getBaseContext()	;
								    	if (context == null){
								    		if(Globals.DEBUG) Log.v(TAG, "context is null");
								    	}else{
								    	EarningsModel cash = new EarningsModel(context);
								    	if (current_balance-cash.getTotalEarnings() >0)
								    	{
								    		cash.add_synced_balance(current_balance);
								    	}
								    	//syncingButton.setText(current_balance_string);
								    	//Toast.makeText(getApplicationContext(), "Successfully synced your balance"+ current_balance_string, Toast.LENGTH_LONG).show();
								    	}}
								    }
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					goTo("messagethreads");
				}else {
					progress_window.dismiss();
					Toast.makeText(getApplicationContext(),
							"Could not store user account in VorText",
							Toast.LENGTH_LONG).show();
				}

			} else {
				progress_window.dismiss();
				Toast.makeText(getApplicationContext(), "Login failed",
						Toast.LENGTH_LONG).show();
				//reset.setVisibility(1);
				//goTo("startover");
			}
			if (progress_window.isShowing())
				progress_window.dismiss();
		}
	}

	public class LoginResponse {

		// good login
		// {"login_result":"true","hash_id":"cc9371a4f04d9c4eae15f8b8d5a31f92"}

		// bad login
		// {"login_result":"false"}

		private String login_result = null;
		private String hash_id = null;

		public Boolean getLoginStatus() {
			if (login_result.equals("true")) {
				
				   
				return true;
			} else {
				return false;
			}
		}
		
		//FUnction to read Json string return from server for balance syncing
		
		public String getHashId() {

			if (hash_id != null) {
				return hash_id;
			} else {
				throw new Error("hash_id not set on LoginResponse!");
			}

		}
	}
		public JSONObject getJSONfromURL(String url){

			//initialize
			InputStream is = null;
			String result = "";
			JSONObject jArray = null;

			//http post
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(url);
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("android", "xJilD83dkcP"));
		        nameValuePairs.add(new BasicNameValuePair("hash_id", prefs.getAccountHashId()));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();

			}catch(Exception e){
				Log.e("log_tag", "Error in http connection "+e.toString());
			}

			//convert response to string
			try{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				result=sb.toString();
			}catch(Exception e){
				Log.e("log_tag", "Error converting result "+e.toString());
			}

			//try parse the string to a JSON object
			try{
		        	jArray = new JSONObject(result);
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data "+e.toString());
			}

			return jArray;
		}
		
		private void goTo(String place) {
			if (place.equals("messagethreads")) {
				Intent intent = new Intent();
				intent.setClass(this, MessageThreads.class);
				startActivity(intent);
			} 
			if(place.equals("reset")){
				Intent intent = new Intent();
				intent.setClass(this, ResetPassword.class);
				startActivity(intent);
			}
				else {
				Intent intent = new Intent();
				intent.setClass(this, SignupOrLogin.class);
				startActivity(intent);
			}
		}
		
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			
			switch(resultCode)
		    {
		    case 2:
		        setResult(2);
		        finish();
		    }
		    super.onActivityResult(requestCode, resultCode, data);
		}

	
}