package com.vortextapp.android.activities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.vortextapp.android.R;
import com.vortextapp.android.models.LoginModel;
import com.vortextapp.android.models.PreferencesModel;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceClickListener;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Preferences extends PreferenceActivity {
	
	private static String APP_ID = "160261344147034";
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		
		// Get the custom preference
		Preference logout = (Preference) findPreference("logOut");
		Preference sendFeedback = (Preference) findPreference("sendFeedback");
		Preference likeFacebook = (Preference) findPreference("likeFacebook");
		Preference faq	= (Preference)findPreference("faq");
		Preference adManage = (Preference)findPreference("adManage");
		mAsyncRunner = new AsyncFacebookRunner(facebook);
		logout.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			public boolean onPreferenceClick(Preference preference) {
				
				
				PreferencesModel prefs = PreferencesModel.getInstance();
				
				LoginModel account = new LoginModel(preference.getContext());
				Boolean result = account.logout();
				
				if(result==true){
					Toast.makeText(getBaseContext(), "Logging out", Toast.LENGTH_SHORT).show();
					//logoutFromFacebook();
					prefs.saveAsLoggedOut();
					Intent intent = new Intent();
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				    intent.setClass(preference.getContext(), SignupOrLogin.class);
				    setResult(2);
				    startActivity(intent);
				    finish();
				    
				} else {
					Toast.makeText(getBaseContext(), "Couldn't logout", Toast.LENGTH_LONG).show();
				}
				
				account.close();
				
				return true;
			}

		});
		
		sendFeedback.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			public boolean onPreferenceClick(Preference preference) {
				
				/* Create the Intent */
				final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

				/* Fill it with Data */
				emailIntent.setType("plain/text");
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"support@rocketlinkmobile.com"});
				emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "VorText Feedback");
				emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");

				/* Send it off to the Activity-Chooser */
				startActivity(Intent.createChooser(emailIntent, "Send feedback with..."));
				
				return true;
				
			}

		});
		
		faq.setOnPreferenceClickListener(new OnPreferenceClickListener(){
			
			
			public boolean onPreferenceClick(Preference preference) {
				final Intent faqIntent = new Intent(Intent.ACTION_VIEW,
						Uri.parse("http://www.rocketlinkmobile.com/vortext/faq"));
						startActivity(faqIntent);
			
			return false;
			}
		});
		
		likeFacebook.setOnPreferenceClickListener(new OnPreferenceClickListener(){
			
			
			public boolean onPreferenceClick(Preference preference) {
				final Intent likeIntent = new Intent(Intent.ACTION_VIEW,
						Uri.parse("http://www.facebook.com/RocketLink")); 
						startActivity(likeIntent);
			
			return false;
			}
		});
		adManage.setOnPreferenceClickListener(new OnPreferenceClickListener(){
			public boolean onPreferenceClick(Preference preference) {
				startManageAds();
				return true;
			
		}
		});
		
		
	}
	private void startManageAds()
	{	
		Intent i = new Intent(getBaseContext(),ManageAds.class);
		startActivity(i);
		
	}	
	
	/**
	 * Function to Logout user from Facebook
	 * */
	public void logoutFromFacebook() {
		mAsyncRunner.logout(this, new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
				Log.d("Logout from Facebook", response);
				if (Boolean.parseBoolean(response) == true) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							
						}

					});

				}
			}

			@Override
			public void onIOException(IOException e, Object state) {
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		switch(resultCode)
	    {
	    case 2:
	        setResult(2);
	        finish();
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}

}