package com.vortextapp.android.activities;

import com.google.gson.Gson;
import com.vortextapp.android.R;
import com.vortextapp.android.activities.CheckFacebook.LoginResponse;
import com.vortextapp.android.config.Actions;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.libraries.RESTClient;
import com.vortextapp.android.libraries.RESTResponder;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.EarningsModel;
import com.vortextapp.android.models.LoginModel;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.receivers.StartLocationReceiver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Login extends Activity {

	private static final String TAG = "VorText: Login"; // for debug label
	private ProgressDialog progress_window;
	private LoginModel session;
	private PreferencesModel prefs = PreferencesModel.getInstance();
	private EditText email;
	private Response rest_responder;
	private EditText password ;
	private EditText confirm_password;
	private ImageView myv; 
	private ProgressBar progressEmail; 
	private Boolean sign_up;
	Button reset;

	
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		if (prefs.isLoggedIn() == false) {
			prefs.saveIsCheckingEmail(false);

			if(Globals.DEBUG) Log.v(TAG, "onCreate called");
			session = new LoginModel(this);

			// settings
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.login);

			// setup events
			email = (EditText) findViewById(R.id.email);
			email.setOnKeyListener(emailKeyListener);
			Button login = (Button) findViewById(R.id.login);
			login.setOnClickListener(loginBtnListener);
			reset = (Button) findViewById(R.id.reset);
			reset.setOnClickListener(resetBtnListener);
			
			//for checking email indicator
			myv = (ImageView) findViewById(R.id.v);
			progressEmail = (ProgressBar)findViewById(R.id.progressEmail);
			sign_up = false;
			
			

			// get email from signup if there is one
			Bundle extras = getIntent().getExtras();
			if (extras != null) {
				String email_from_signup = extras.getString("username");
				email.setText(email_from_signup);
			}
			
			password = (EditText) findViewById(R.id.password);
			password.setOnKeyListener(passwordKeyListener);
			email.setOnFocusChangeListener(checkEmailExistence);
			confirm_password = (EditText) findViewById(R.id.confirm_password);
			confirm_password.setOnKeyListener(passwordConfirmKeyListener);

		} else {
			goTo("messagethreads");
		}

	}
	//function to check if email has already existed 
	//if yes mode: login
	//if no mode: sign up
	private class CheckForEmail extends AsyncTask<String, Void, Boolean> {
	      @Override
	      protected Boolean doInBackground(String... params) {	    	
	    	  prefs.saveIsCheckingEmail(true);
	    	  MultiValueMap parts = new LinkedMultiValueMap();
	    	  parts.add("email", params[0]);
	    	  parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);	             
	            rest_responder = new Response();
	            RESTClient client = new RESTClient(rest_responder, parts);
	            if (client.isOnline()) 
	            	if(Globals.DEBUG) Log.v(TAG, "About to execute Rest");
	            	client.execute("https://www.rocketlinkmobile.com/portal/api10/email_validate/");
	            return true;
	      }      
	      @Override
	      protected void onPostExecute(Boolean result) {  
	    	  prefs.saveIsCheckingEmail(false);	    	  
	      }
	      @Override
	      protected void onPreExecute() {
	      }
	      @Override
	      protected void onProgressUpdate(Void... values) {
	      }
	}
	
	protected void onStop() {
		super.onStop();
		finish();
	}

	protected void onDestroy() {
		super.onDestroy();
		session.close();
	}

	// Create an anonymous implementation of OnClickListener
	private View.OnClickListener loginBtnListener = new View.OnClickListener() {

		
		public void onClick(View v) {
			if (sign_up){
				if (password.getText().toString().equals(confirm_password.getText().toString())){
				if(Globals.DEBUG) Log.v(TAG, "Sing up clicked, starting web call ");
				startSignUpServiceCall();}
				else{
					Toast.makeText(getApplicationContext(), "Passwords don't match!" ,
							Toast.LENGTH_LONG).show();
				}
			}
			else{
			if(Globals.DEBUG) Log.v(TAG, "Login clicked, starting web call ");
			startLoginServiceCall();
			}

		}
	};

	private View.OnClickListener resetBtnListener = new View.OnClickListener() {

		
		public void onClick(View v) {

			goTo("reset");

		}
	};
	
private View.OnKeyListener passwordKeyListener = new View.OnKeyListener() {

		@Override
		public boolean onKey(View arg0, int keyCode, KeyEvent arg2) {
			// TODO Auto-generated method stub
			if (arg2.getAction() == KeyEvent.ACTION_DOWN)
	        {
	            switch (keyCode)
	            {
	                case KeyEvent.KEYCODE_DPAD_CENTER:
	                case KeyEvent.KEYCODE_ENTER:
	                	if (!sign_up){
	                	if(Globals.DEBUG) Log.v(TAG, "Login clicked, starting web call ");
	        			startLoginServiceCall();}
	                    return true;
	                default:
	                    break;
	            }
	        }
			return false;
		}
	};
	
	private View.OnKeyListener passwordConfirmKeyListener = new View.OnKeyListener() {

		@Override
		public boolean onKey(View arg0, int keyCode, KeyEvent arg2) {
			// TODO Auto-generated method stub
			if (arg2.getAction() == KeyEvent.ACTION_DOWN)
	        {
	            switch (keyCode)
	            {
	                case KeyEvent.KEYCODE_DPAD_CENTER:
	                case KeyEvent.KEYCODE_ENTER:
	                	if (sign_up){
	                	if(Globals.DEBUG) Log.v(TAG, "Login clicked, starting web call ");
	        			startLoginServiceCall();}
	                    return true;
	                default:
	                    break;
	            }
	        }
			return false;
		}
	};
	
	private View.OnKeyListener emailKeyListener = new View.OnKeyListener() {		

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			// TODO Auto-generated method stub
			confirm_password.setVisibility(View.GONE);
			sign_up = false;
			return false;
		}
	}; 
	
	//Start checking for email when user is done typing
	private View.OnFocusChangeListener checkEmailExistence = new View.OnFocusChangeListener() {
		public void onFocusChange(View v,boolean hasFocus) {
			if (!hasFocus){
				String entered_email = email.getText().toString().trim() ;
				if(entered_email.length()>0){
			if (prefs.isCheckingEmail()){
				//Toast.makeText(getApplicationContext(), "Still checking email" ,
					//	Toast.LENGTH_LONG).show();			
			}
			else {
				CheckForEmail task = new CheckForEmail();
			    task.execute(new String[] { entered_email });
				//Toast.makeText(getApplicationContext(), "Started a thread to check email" ,
				//		Toast.LENGTH_LONG).show();
				progressEmail.setVisibility(View.VISIBLE);
		    	  myv.setVisibility(View.INVISIBLE);
			}}else{
				Toast.makeText(getApplicationContext(), "Please enter email!" ,
						Toast.LENGTH_LONG).show();
			}}
			
			else{
				myv.setVisibility(View.INVISIBLE);
				progressEmail.setVisibility(View.INVISIBLE);
			}}
	};

	//Recursive method to clear out the activity stack
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    switch(resultCode)
	    {
	    case 2:
	        setResult(2);
	        finish();
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void startLoginServiceCall() {
		String email_url_encoded = null;
		try {
			email_url_encoded = URLEncoder.encode(email.getText().toString(),
					"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String password_url_encoded = null;
		try {
			password_url_encoded = URLEncoder.encode(password.getText()
					.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(Globals.DEBUG) Log.v(TAG, "email_url_encoded: " + email_url_encoded
				+ " password_url_encoded: " + password_url_encoded);

		progress_window = ProgressDialog.show(Login.this, "",
				"Logging in, please wait", true);


		//String service = URLCollection.API + "login/";
		MultiValueMap parts = new LinkedMultiValueMap();
		parts.add("username", email_url_encoded);
		parts.add("password", password_url_encoded);
		parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);

		rest_responder = new Response();
		RESTClient client = new RESTClient(rest_responder, parts);
		if (client.isOnline()) {		
			//client.execute(service);
			client.execute("https://www.rocketlinkmobile.com/portal/api10/login/");
		} else {
			progress_window.dismiss();
			new AlertDialog.Builder(this)
					.setTitle("Connection Error!")
					.setMessage(
							"Please connect your device to the internet for using this function! ")
					.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
						int which) {
						}
					}).show();
		}

	}
	
	private void startSignUpServiceCall() {
		String email_url_encoded = null;
		try {
			email_url_encoded = URLEncoder.encode(email.getText().toString(),
					"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String password_url_encoded = null;
		try {
			password_url_encoded = URLEncoder.encode(password.getText()
					.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(Globals.DEBUG) Log.v(TAG, "email_url_encoded: " + email_url_encoded
				+ " password_url_encoded: " + password_url_encoded);

		progress_window = ProgressDialog.show(Login.this, "",
				"Sign up, please wait", true);
		//String service = URLCollection.API + "login/";
		MultiValueMap parts = new LinkedMultiValueMap();
		parts.add("email", email_url_encoded);
		parts.add("password", password_url_encoded);
		parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);

		rest_responder = new Response();
		RESTClient client = new RESTClient(rest_responder, parts);
		if (client.isOnline()) {		
			//client.execute(service);
			client.execute("https://www.rocketlinkmobile.com/portal/api10/email_signup/");
		} else {
			progress_window.dismiss();
			new AlertDialog.Builder(this)
					.setTitle("Connection Error!")
					.setMessage(
							"Please connect your device to the internet for using this function! ")
					.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
						int which) {
						}
					}).show();
		}

	}

	private class Response implements RESTResponder {
		public void handleRESTResponse(String response) {
			Gson gson = new Gson();
			LoginResponse o = new LoginResponse();
			try {
				 o = gson.fromJson(response, LoginResponse.class);
			} catch (Exception e) {
				// TODO: handle exception
				String exception = e.toString();
				o.login_result = "failed";
				o.existedemail ="failed";
				o.request = "failed";
			}
			
			if (o.request.toString().equals("login") || o.request.toString().equals("singup")){
			if (!o.login_result.toString().equals("failed"))
			{
			if(Globals.DEBUG) Log.v(TAG, "handleRESTResponse LoginResponse getLoginStatus: " + o.getLoginStatus());
			progress_window.dismiss();
			}
			else
			{
				Toast.makeText(getApplicationContext(), "Login failed",
						Toast.LENGTH_LONG).show();
				reset.setVisibility(1);
				//goTo("startover");
			}
			
//*************************************************
//FOR LOGIN and SIGN UP
			if(o.getLoginStatus()){
				
				//TODO need to store hash_id here
				Boolean result = session.login(o.getHashId(), email.getText().toString());
				if(result==true){
					prefs.saveAsLoggedIn(o.getHashId(), email.getText().toString());
					
					// kick the scheduler into gear, assuming first run and that phone has not rebooted, 
					// reboot will register scheduler, so calling this service explicitly is not
					// necessary.
					///////////////////////////////////////////////////////////
					Intent intent = new Intent(Actions.START_SERVICE_SCHEDULE);
					intent.addCategory("android.intent.category.ALTERNATIVE");
					getApplicationContext().sendBroadcast(intent);
					///////////////////////////////////////////////////////////
					
					Toast.makeText(getApplicationContext(), "Login was successful", Toast.LENGTH_LONG).show();
					//Start syncing the balance with server
					 JSONObject json = getJSONfromURL("https://www.rocketlinkmobile.com/portal/api10/get_current_earnings");
					    int successful= 0;
						try {
							successful = json.getInt("success");
							
							 if(successful ==1){
								 String current_balance_string = json.getString("total");
									Double current_balance = Double.parseDouble(current_balance_string);
								    if(current_balance !=0)
								    {
								    	Context context = getApplicationContext();
								    	if(context == null)
								    		context = getBaseContext()	;
								    	if (context == null){
								    		if(Globals.DEBUG) Log.v(TAG, "context is null");
								    	}else{
								    	EarningsModel cash = new EarningsModel(context);
								    	if (current_balance-cash.getTotalEarnings() >0)
								    	{
								    		cash.add_synced_balance(current_balance);
								    	}
								    	//syncingButton.setText(current_balance_string);
								    	//Toast.makeText(getApplicationContext(), "Successfully synced your balance $"+ current_balance_string, Toast.LENGTH_LONG).show();
								    	}}
								    }
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					goTo("messagethreads");
				} else {
					Toast.makeText(getApplicationContext(),
							"Could not store user account in VorText",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(getApplicationContext(), "Login failed",
						Toast.LENGTH_LONG).show();
				reset.setVisibility(1);
				//goTo("startover");
			}}
			else{
//*******************************************
//FOR CHECK EMAIL
			progressEmail.setVisibility(View.INVISIBLE);
			if (o.existedemail.toString().equals("true"))
			{
				Toast.makeText(getApplicationContext(), "Email found, please sign in!",
						Toast.LENGTH_LONG).show();
			if(Globals.DEBUG) Log.v(TAG, "handleRESTResponse LoginResponse getLoginStatus: " + o.getValidationStatus());
			confirm_password.setVisibility(View.GONE);
			myv.setVisibility(View.VISIBLE);
			sign_up = false;
			
			//progress_window.dismiss();
			}else
			{
				Toast.makeText(getApplicationContext(), "New email, please sign up!",
						Toast.LENGTH_LONG).show();
				confirm_password.setVisibility(View.VISIBLE);
				sign_up = true;
			}
			if(o.getValidationStatus()){
		    		  confirm_password.setVisibility(View.GONE );
			
						sign_up = false;}
			else{
		    		  confirm_password.setVisibility(View.VISIBLE);
		    		  sign_up = true;
			}
		    	  
			}
		}
		
	}

	public class LoginResponse {

		// good login
		// {"login_result":"true","hash_id":"cc9371a4f04d9c4eae15f8b8d5a31f92"}

		// bad login
		// {"login_result":"false"}
		
		
		private String existedemail = null;
		private String request = null;

		public Boolean getValidationStatus() {
			if (existedemail.equals("true")) {
				
				   
				return true;
			} else {
				return false;
			}
		}
		

		private String login_result = null;
		private String hash_id = null;

		public Boolean getLoginStatus() {
			if (login_result.equals("true")) {
				
				   
				return true;
			} else {
				return false;
			}
		}
		
		//FUnction to read Json string return from server for balance syncing
		
		public String getHashId() {

			if (hash_id != null) {
				return hash_id;
			} else {
				throw new Error("hash_id not set on LoginResponse!");
			}

		}

		// ///// OLD ->
		// bad login:
		// {"response":{"valid":"false"}}

		// good login:
		// {"response":{"valid":"true"}}

		/*
		 * private Response response = this.new Response();
		 * 
		 * public Boolean getLoginStatus(){ return response.getValid(); }
		 * 
		 * private class Response { private Boolean valid = false; private
		 * Boolean getValid(){ return valid; } }
		 */

	}

	private void goTo(String place) {
		if (place.equals("messagethreads")) {
			Intent intent = new Intent();
			intent.setClass(this, MessageThreads.class);
			startActivity(intent);
		} 
		if(place.equals("reset")){
			Intent intent = new Intent();
			intent.setClass(this, ResetPassword.class);
			startActivity(intent);
		}
			else {
			Intent intent = new Intent();
			intent.setClass(this, SignupOrLogin.class);
			startActivity(intent);
		}
	}

	// //Fucntion to check reachability
	// public static boolean isOnline(Context context) {
	// // First, check we have any sort of connectivity
	// final ConnectivityManager connMgr = (ConnectivityManager)
	// context.getSystemService(Context.CONNECTIVITY_SERVICE);
	// TelephonyManager lTelephonyManager = (TelephonyManager)
	// context.getSystemService(Context.TELEPHONY_SERVICE);
	// final NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
	//
	// if (netInfo != null && netInfo.isConnected()) {
	// // Some sort of connection is open, check if server is reachable
	// int type = lTelephonyManager.getNetworkType();
	// int state = lTelephonyManager.getDataState();
	// // (( lTelephonyManager.getNetworkType() ==
	// TelephonyManager.NETWORK_TYPE_UMTS ||
	// // /* icky... using literals to make backwards compatible with 1.5 and
	// 1.6 */
	// // lTelephonyManager.getNetworkType() == 2 /*HSUPA*/ ||
	// // lTelephonyManager.getNetworkType() == 9 /*HSUPA*/ ||
	// // lTelephonyManager.getNetworkType() == 10 /*HSPA*/ ||
	// // lTelephonyManager.getNetworkType() == 8 /*HSDPA*/ ||
	// // lTelephonyManager.getNetworkType() == 5 /*EVDO_0*/ ||
	// // lTelephonyManager.getNetworkType() == 6 /*EVDO A*/) &&
	// if (lTelephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED)
	// {
	// return true;
	// }
	// //Do some thing here
	// try {
	// // Long time1 = TimeManager.getCurrentSyncronizedTimeStamp();
	// URL url = new URL("http://dev.rocketlinkmobile.com");
	// HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
	// urlc.setRequestProperty("User-Agent", "Android Application");
	// urlc.setRequestProperty("Connection", "close");
	// urlc.setConnectTimeout(15 * 1000); // Thirty seconds timeout in
	// milliseconds
	// try {
	// urlc.connect();
	// } catch (IOException e)
	// {
	// Log.e(TAG, e.getMessage());
	// return false;
	// }
	// // Long time2 = TimeManager.getCurrentSyncronizedTimeStamp();
	// // if(time2-time1 >=10000)
	// // return false;
	// // Map<String, List<String>> head_fields = urlc.getHeaderFields();
	// // String array_string [] =
	// {urlc.getResponseMessage(),urlc.getContent().toString(),urlc.getContentType(),urlc.getContentEncoding(),//,,urlc.getErrorStream().toString()
	// //
	// urlc.getInputStream().toString(),urlc.getRequestMethod()};//,};urlc.getOutputStream().toString()
	// // Boolean array_bool[] =
	// {urlc.getDoInput(),urlc.getDoOutput(),urlc.getInstanceFollowRedirects(),urlc.getUseCaches(),urlc.getDefaultAllowUserInteraction(),urlc.getAllowUserInteraction()};
	// // long array_long[] =
	// {urlc.getExpiration(),urlc.getIfModifiedSince(),urlc.getLastModified(),urlc.getDate()};
	// // int array_int[] =
	// {urlc.getResponseCode(),urlc.getConnectTimeout(),urlc.getContentLength(),urlc.getReadTimeout(),};
	//
	// if (urlc.getResponseCode() == 200 & urlc.getContentLength()!= -1) { //
	// Good response
	// return true;
	// } else { // Anything else is unwanted (login page, unreachable server...)
	// return false;
	// }
	// } catch (IOException e) {
	// Log.e(TAG, e.getMessage());
	// return false;
	// }
	// } else {
	// return false;
	// }
	// }

	// Old way of checking reachability
	// public boolean isOnline(Context context) {
	// WifiManager lWifiManager = (WifiManager)
	// Login.this.getSystemService(Context.WIFI_SERVICE);
	// TelephonyManager lTelephonyManager = (TelephonyManager)
	// Login.this.getSystemService(Context.TELEPHONY_SERVICE);
	//
	// ////////////////////////////
	// // if we have a fast connection (wifi or 3g)
	// boolean wifi_enable = lWifiManager.isWifiEnabled();
	// String info_wifi = lWifiManager.getConnectionInfo().toString();
	// int ip = lWifiManager.getConnectionInfo().getIpAddress();
	// int networktype = lTelephonyManager.getNetworkType();
	// int data_state = lTelephonyManager.getDataState();
	//
	//
	//
	// if( (lWifiManager.isWifiEnabled() && lWifiManager.getConnectionInfo() !=
	// null && lWifiManager.getConnectionInfo().getIpAddress() != 0) ||
	// ( (lTelephonyManager.getNetworkType() ==
	// TelephonyManager.NETWORK_TYPE_UMTS ||
	//
	// /* icky... using literals to make backwards compatible with 1.5 and 1.6
	// */
	// lTelephonyManager.getNetworkType() == 9 /*HSUPA*/ ||
	// lTelephonyManager.getNetworkType() == 10 /*HSPA*/ ||
	// lTelephonyManager.getNetworkType() == 8 /*HSDPA*/ ||
	// lTelephonyManager.getNetworkType() == 5 /*EVDO_0*/ ||
	// lTelephonyManager.getNetworkType() == 6 /*EVDO A*/)
	//
	// && lTelephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED)
	// ){
	// return true;
	// //Do some thing here
	// }
	// try {
	// boolean connected = InetAddress.getByName("google.ca").isReachable(3);
	// if (connected)
	// {
	// return true;
	// }
	// else
	// {return false;}
	// } catch (UnknownHostException e){
	// return false;
	// } catch (IOException e){
	// return false;
	// }
	// }
	public JSONObject getJSONfromURL(String url){

		//initialize
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;

		//http post
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("android", "xJilD83dkcP"));
	        nameValuePairs.add(new BasicNameValuePair("hash_id", prefs.getAccountHashId()));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		}catch(Exception e){
			Log.e("log_tag", "Error in http connection "+e.toString());
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
		}catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		//try parse the string to a JSON object
		try{
	        	jArray = new JSONObject(result);
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data "+e.toString());
		}

		return jArray;
	}

}