package com.vortextapp.android.activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.vortextapp.android.R;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.models.DatabaseHelper;
import com.vortextapp.android.models.EarningsModel;
import com.vortextapp.android.models.MessageModel;
import com.vortextapp.android.models.MessagesObserver;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.models.tables.Messages;
import com.vortextapp.android.models.tables.Threads;

public class MessageThreads extends ListActivity {
	
	private static final String TAG = "VorText: MessageThreads"; // for debug label
	private MessageThreadsAdapter adapter;
	Hashtable<String,String> ActualSender = new Hashtable<String,String>();
	private MessageModel messages;
	public ListWatch message_list_watcher;
	static PreferencesModel prefs = PreferencesModel.getInstance();
	private MessagesObserver messagesObserver = MessagesObserver.getInstance();
//	public Button earningsButton;
	TextView earnings, earningsPercentage;
	RelativeLayout earningsPercentageLayout;
	//for debugging
	//public Button syncingButton;
	
	
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Debug.startMethodTracing("MessageThreads");
		if(prefs.isLoggedIn()==true){
			
			// check if this was started from the notifications bar
			//di.putExtra(SMS.NOTIFICATION_ID_EXTRA, notificationId);
			 //if(getIntent().getExtras() != null){
				 //int notification_id = getIntent().getIntExtra(SMS.NOTIFICATION_ID_EXTRA, 0);
				 //if(notification_id == PreferencesPrivateData.NOTIFICATION_SINGLE_ID) {
			//android.os.Debug.waitForDebugger();		 
			prefs.resetNotificationCount();
				// }
			 //}
		
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        
	        setContentView(R.layout.message_threads);
	        if(Globals.DEBUG) Log.v(TAG, "onCreate called");
	        
	        messages = MessageModel.getInstance();
	        messages.setThreadsMessages();
	 
	        adapter = new MessageThreadsAdapter(this, R.layout.message_threads_item, messages.list);
	        setListAdapter(adapter);
	        
	        message_list_watcher = new ListWatch(); // this is supposed to refresh list when changes occur to it
	        messagesObserver.addObserver(message_list_watcher);
	        
	        LinearLayout composeMessageButton = (LinearLayout) findViewById(R.id.composeBtn);
	        composeMessageButton.setOnClickListener(composeMessageButtonListener);
	        
	        Button bonus=(Button)findViewById(R.id.bonus);
	        bonus.setOnClickListener(bonusButtonListener);
	        
	        Button coin=(Button)findViewById(R.id.coin);
	        coin.setOnClickListener(earningsButtonListener);
	        
	        earningsPercentageLayout=(RelativeLayout)findViewById(R.id.percentageLayout);
	        earningsPercentageLayout.setOnClickListener(earningsButtonListener);
	        
	        earnings=(TextView)findViewById(R.id.earnings);
	        earnings.setOnClickListener(earningsButtonListener);
	        
	        earningsPercentage=(TextView)findViewById(R.id.earningsPercentage);
	        earningsPercentage.setOnClickListener(earningsButtonListener);
	        
	        setEarningsDisplay();
	        
	        //for debug
	        //syncingButton = (Button)findViewById(R.id.button_sync_ballance);
	        //syncingButton.setOnClickListener(syncingButtonListern);
	        
	        
	     // ListActivity has a ListView, which you can get with: 
	    	ListView lv = getListView(); 
	    	// Then you can create a listener like so: 
	    	lv.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener(){ 
	    	         
	    	        public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) { 
	    	        	if(Globals.DEBUG) Log.v(TAG, "I'm suffocating pos "+pos+"id "+id);
	    	           //onLongListItemClick(v,pos,id); 
	    	           return false; 
	    	        } 
	    	}); 
	    	//Enable contextMenu
	    	registerForContextMenu(lv);

	    	//// You then create your handler method: 
	    	//protected void onLongListItemClick(v,pos,id) { 
	    	//      if(Globals.DEBUG) Log.i( TAG, "onLongListItemClick id=" + id ); 
	    	//} 
	    	
	    	//TODO need another variable for this in prefs
	    	//Set version
	    	try {
				prefs.setVersion(this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	if(prefs.hasBetaNoticeShown() == false){
	    		//if betaNotice has been shown, it must be their first time install/fresh install. Thus ContactSyncing is not needed.
	    		Log.v(TAG, "About to show Beta notice n set both ref to true ");
	    		prefs.setAreContactPicturesSynced(true);
	    		

	    		new AlertDialog.Builder(this)

				.setTitle("Thanks for Trying VorText!")

				.setMessage(
						"Please be advised that this app is still in beta. " +
						"If you don't see ads appearing after a day of use, " +
						"there may be no ads available yet for your location. "
						)

				.setNeutralButton("Ok",

				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,

					int which) {

					}

				}).show();
	    		
	    		prefs.setBetaNoticeShown(true);
	    		
	    		
	    	}
	    	else{
	    	boolean are_contact_pictures_synced = prefs.areContactPicturesSynced();
	    	//Syncronize contact pictures for contacts
	    	if(!are_contact_pictures_synced){
	    		new AlertDialog.Builder(this)

				.setTitle("VorText beta!")

				.setMessage(
						"The database needs to be updated. " +
						"Press Ok to begin. " +
						"When done, please logout and in again to get back your past earning."
						)

				.setNeutralButton("Ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
					int which) {
						int size = messages.list.size();
						for (int i = 0;i<size;i++){
							String phone_number = messages.list.get(i).getThread().getContact().getPhone1();
							int number = getContactIDFromNumber(phone_number,getBaseContext());
							Uri person = ContentUris.withAppendedId(
									ContactsContract.Contacts.CONTENT_URI,
									number);
							Uri u = Uri.withAppendedPath(
									person,
									ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
							messages.updateContactImage(u, phone_number,true);
						}
					}
				}).show();
	    		prefs.setAreContactPicturesSynced(true);
	    	}
	    	}
	 
        
		} else {
			Toast.makeText(getApplicationContext(), "You must login first to see messages", Toast.LENGTH_LONG).show();
			Intent intent = new Intent();
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		    intent.setClass(this, SignupOrLogin.class);
		    startActivity(intent);
		    finish();
		}
		
    }
	
	private View.OnClickListener composeMessageButtonListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			Intent intent = new Intent();
		    intent.setClass(v.getContext(), ComposeMessage.class);
		    //startActivity(intent);
		    
			startActivityForResult(intent, 1);
		    //finish();
		    //refresh();
		}
	};
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1){
		if (resultCode == RESULT_OK){
			refresh();}}
		switch(resultCode)
	    {
	    case 2:
	        setResult(2);
	        finish();
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	private View.OnClickListener earningsButtonListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			Intent intent = new Intent();
		    intent.setClass(v.getContext(), PaymentOverview.class);
		    v.getContext().startActivity(intent);
		}
		
	};
	
	private View.OnClickListener bonusButtonListener = new View.OnClickListener() {
		public void onClick(View v) {
			Toast.makeText(v.getContext(), "It does not work yet.", Toast.LENGTH_SHORT).show();
		}
		
	};
	
//	private View.OnClickListener syncingButtonListern = new View.OnClickListener() {
//		
//		
//		public void onClick(View v) {
//
//		    JSONObject json = getJSONfromURL("https://www.rocketlinkmobile.com/portal/api10/get_current_earnings");
//		    int successful= 0;
//			try {
//				successful = json.getInt("success");
//				
//				 if(successful ==1){
//					 String current_balance_string = json.getString("total");
//						float current_balance = Float.parseFloat(current_balance_string);
//					    if(current_balance !=0)
//					    {
//					    	syncingButton.setText(current_balance_string);
//					    	}
//					    }
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		   
//		    }
//		    
//		    
//	};
	
	public static JSONObject getJSONfromURL(String url){

		//initialize
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;

		//http post
		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("android", "xJilD83dkcP"));
	        nameValuePairs.add(new BasicNameValuePair("hash_id", prefs.getAccountHashId()));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		}catch(Exception e){
			Log.e("log_tag", "Error in http connection "+e.toString());
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result=sb.toString();
		}catch(Exception e){
			Log.e("log_tag", "Error converting result "+e.toString());
		}

		//try parse the string to a JSON object
		try{
	        	jArray = new JSONObject(result);
		}catch(JSONException e){
			Log.e("log_tag", "Error parsing data "+e.toString());
		}

		return jArray;
	}
	protected void onListItemClick(ListView l, View v, int position, long id){
		//This method will be called when an item in the list is selected.
		//Toast.makeText(this, "You clicked on id "+adapter.getThreadId(position), Toast.LENGTH_SHORT).show();
		
		Intent intent = new Intent();
	    intent.setClass(v.getContext(), MessageThread.class);
	    intent.putExtra("threadId", adapter.getThreadId(position));
	    intent.putExtra("phoneNumber",adapter.getPhoneNumber(position));
	    intent.putExtra("contactName", messages.list.get(position).getThread().getContact().getFirstName()+ " "+messages.list.get(position).getThread().getContact().getLastName());
	    v.getContext().startActivity(intent);
	    //refresh();
	}
	
    
    protected void onStop() {
        super.onStop();
        //android.os.Debug.waitForDebugger();
        prefs.resetNotificationCount();
       
        //finish();
    }
    
    
    protected void onResume() {
    	super.onResume();
    	refresh();
    }
    
    
    protected void onDestroy(){
    	super.onDestroy();
    	messages.close();
    }
    
	
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item){
		//:TODO find a better way to accomplish this
				//This code only works with higher adk
				//switch (item.getItemId()) {
				//case R.id.settings:
					startActivityForResult(new Intent(this, Preferences.class),1);
					return true;
				//}
				//return false;
	}
	//@Hai: Create context menu to delete thread
    
    public void onCreateContextMenu(ContextMenu menu, View v,
    		ContextMenuInfo menuInfo) {
    	    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
    		String[] menuItems = getResources().getStringArray(R.array.threadMenu); 
			// If later we want more options for context menu then we will use this loop
			//for now just use index 0
			//    		for (int i = 0; i<menuItems.length; i++) {
			//    			menu.add(Menu.NONE, i, i, menuItems[i]);			
			//   	}
    		menu.add(Menu.NONE,0,0,menuItems[0]);
    }
    //@Hai: Handle delete thread if delete is selected
    
    public boolean onContextItemSelected(MenuItem item) {
	    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
		//Delete all the messages
		Threads selected_thread = new Threads();
		int threadID = adapter.getThreadId(info.position);
		if(Globals.DEBUG) Log.v(TAG, "About to delete all messages for threadID "+threadID);
		selected_thread.setId(threadID);
		List<Messages> message_list = new ArrayList<Messages>();
		Messages messages = new Messages();
		messages.setIsRead(null);
		messages.setThreads(selected_thread);

		Dao<Messages, Long> messagesDao;
		Dao<Threads, Integer> threadDao;
		DatabaseHelper databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);

		try{
			messagesDao = databaseHelper.getMessagesDao();
			message_list = messagesDao.queryForMatching(messages);
			ListIterator<Messages> message_itr = message_list.listIterator();
			while(message_itr.hasNext()){
				Messages m = message_itr.next();
				messagesDao.delete(m);

			}
			
			//Delete the thread	
			threadDao = databaseHelper.getThreadsDao();
			threadDao.delete(selected_thread);
			if(Globals.DEBUG) Log.v(TAG, "Deleted all messages for threadID "+threadID);
		} catch (java.sql.SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
	    
		refresh();
    	return true;
    }
	
	private class ListWatch implements Observer { 
		public void update(Observable obj, Object arg) { 
			if(Globals.DEBUG) Log.v(TAG, "Update called on ListWatch");
			//
			refresh();
			//
		} 
	} 
	
	public void refresh(){
		prefs.resetNotificationCount();
		int milliseconds = 10;
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		adapter.clear();
		messages.setThreadsMessages();
        adapter = new MessageThreadsAdapter(this, R.layout.message_threads_item, messages.list);
        setListAdapter(adapter);
		adapter.notifyDataSetChanged();
		
		//setEarningsDisplay(earningsButton);
		//Debug.stopMethodTracing(); 

	}
	
	private void setEarningsDisplay(){
		//checkEarning();
		EarningsModel cash = new EarningsModel(this);
		double total = (int)((cash.getTotalEarnings())*100);
//		DecimalFormat df = new DecimalFormat("##0.00");
//		String pretty_total = df.format(total);
		earnings.setText(String.valueOf((int)total));
		earningsPercentage.setText(String.valueOf((int)total));
		
		Double percentage=(total/Globals.MINIMUM_CLAIM)*100;
		if(percentage>100)
			percentage=100d;
		double progressBarWidth=getResources().getDimension(R.dimen.progress_layout_width)-(2*getResources().getDimension(R.dimen.progress_layout_padding));
		double padding=progressBarWidth-(progressBarWidth*percentage/100);
//		new AlertDialog.Builder(this).setMessage(
//				"total width: "+progressBarWidth+"\n"+
//				"padding: "+(int)padding).create().show();
		
		earningsPercentageLayout.setPadding(0, 0, (int)padding, 0);
		
		cash.close();
	}
	
	private void checkEarning()
	{
		JSONObject j = new JSONObject();
		String hash_id =prefs.getAccountHashId();
		try {
			j.put("android", URLCollection.API_PASSWORD);
			j.put("hash_id", prefs.getAccountHashId());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (j!=null){
			//String url = new URL("https", "www.rocketlinkmobile.com", "443").toString();
			String url = "https://www.rocketlinkmobile.com";
		HttpResponse re;
		String temp = null;
		try {
			re = MessageThreads.doPost(url, j);
			if (re!=null){
				temp = EntityUtils.toString(re.getEntity());
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (temp.compareTo("SUCCESS")==0)
		{
		    Toast.makeText(this, "Sending complete!", Toast.LENGTH_LONG).show();
		}
	}
		
	}
	public static HttpResponse doPost(String url, JSONObject c) throws ClientProtocolException, IOException 
	{
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost request = new HttpPost(url);
	    HttpEntity entity;
	    StringEntity s = new StringEntity(c.toString());
	    s.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
	    entity = s;
	    request.setEntity(entity);
	    HttpResponse response;
	    response = httpclient.execute(request);
	    return response;
	}
	public static int getContactIDFromNumber(String contactNumber,Context context)
	{
	    contactNumber = Uri.encode(contactNumber);
	    int phoneContactID = new Random().nextInt();
	    Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,Uri.encode(contactNumber)),new String[] {PhoneLookup.DISPLAY_NAME, PhoneLookup._ID}, null, null, null);
	        while(contactLookupCursor.moveToNext()){
	            phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(PhoneLookup._ID));
	            }
	        contactLookupCursor.close();

	    return phoneContactID;
	}

}
