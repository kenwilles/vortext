package com.vortextapp.android.activities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

import com.vortextapp.android.models.tables.Ads;

public class AdTextView extends TextView {
	
	
//	private Paint marginPaint;
//	  private Paint linePaint;
//	  private int paperColor;
//	  private float margin;
	
	public Ads ad;
	
	public AdTextView(Context context){
		
		super(context);
		init();
	}
	public AdTextView(Context context, AttributeSet attrs){
		
		super(context, attrs);
		init();
	}
	
	public AdTextView(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
		init();
	}
	
	private void init() {
//	    marginPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//	    marginPaint.setColor(Color.RED);
//	    linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//	    linePaint.setColor(Color.BLUE);
//	    paperColor = Color.YELLOW;
//	    margin = Color.CYAN;
	  }

	  
	  public void onDraw(Canvas canvas) {
//	    canvas.drawColor(paperColor);
//	    canvas.drawLine(0, 0, getMeasuredHeight(), 0, linePaint);
//	    canvas.drawLine(0, getMeasuredHeight(), getMeasuredWidth(),
//	        getMeasuredHeight(), linePaint);
//	    canvas.drawLine(margin, 0, margin, getMeasuredHeight(), marginPaint);
//	    canvas.save();
//	    canvas.translate(margin, 0);
	    super.onDraw(canvas);
	    //canvas.restore();
	  }
	
};
