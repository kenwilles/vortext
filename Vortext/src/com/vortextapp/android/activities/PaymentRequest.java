package com.vortextapp.android.activities;

import java.text.DecimalFormat;

import com.vortextapp.android.R;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.models.EarningsModel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentRequest extends Activity {
	
	private static final String TAG = "VorText: PaymentRequest"; // for debug label
	Button homeButton;
	private String email;
	private Double total;
	Bundle extras;
	EarningsModel cash;

    
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(Globals.DEBUG) Log.v(TAG, "onCreate called");
        
        // settings
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.payment_claim);
        
        extras = getIntent().getExtras();
        setEarningsDisplay();
        setFeedbackText();
        
        homeButton.setOnClickListener(homeButtonListener);
        
//        Button earningsButton = (Button)findViewById(R.id.earningsButton);
//        earningsButton.setEnabled(false);
        
        ImageButton composeMessageButton = (ImageButton) findViewById(R.id.composeBtn);
        composeMessageButton.setEnabled(false);
        Button bonus = (Button) findViewById(R.id.bonus);
        bonus.setOnClickListener(bonusButtonListener);

    }
    
    private void setEarningsDisplay(){
    	homeButton = (Button)findViewById(R.id.return_to_threads);
//    	Button earningsButton = (Button)findViewById(R.id.earningsButton);
//		earningsButton.setText("$0.00");
    	TextView earnings=(TextView)findViewById(R.id.earnings);
    	TextView earningsPercentage=(TextView)findViewById(R.id.earningsPercentage);
    	earnings.setText(String.valueOf(0));
		earningsPercentage.setText(String.valueOf(0));
		
		double progressBarWidth=getResources().getDimension(R.dimen.progress_layout_width)-(2*getResources().getDimension(R.dimen.progress_layout_padding));
		RelativeLayout earningsPercentageLayout=(RelativeLayout)findViewById(R.id.percentageLayout);
		earningsPercentageLayout.setPadding(0, 0, (int)progressBarWidth, 0);
	}
    
    private void setFeedbackText(){
		email = extras.getString("email");
		total = extras.getDouble("total");
		DecimalFormat df = new DecimalFormat("##0.00");
		String pretty_total = df.format(total);
		TextView feedback_text = (TextView)findViewById(R.id.request_payment_feedback);
		String text = feedback_text.getText().toString();
		text = text.replace("$amount", pretty_total);
		text = text.replace("$email", email);
		feedback_text.setText(text);
    }
	
    private View.OnClickListener bonusButtonListener = new View.OnClickListener() {
		public void onClick(View v) {
			Toast.makeText(v.getContext(), "It does not work yet.", Toast.LENGTH_SHORT).show();
		}
		
	};
    
	private View.OnClickListener homeButtonListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			// process claim
			Intent intent = new Intent();
			intent.setClass(v.getContext(), MessageThreads.class);
			v.getContext().startActivity(intent);
			finish();
		}
		
	};
    
    
    protected void onStop() {
        super.onStop();
        finish();
    }
    
    protected void onDestroy() {
		super.onDestroy();
    }
    
    
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item){
		//:TODO find a better way to accomplish this
				//This code only works with higher adk
				//switch (item.getItemId()) {
				//case R.id.settings:
					startActivity(new Intent(this, Preferences.class));
					return true;
				//}
				//return false;
	}
    
}