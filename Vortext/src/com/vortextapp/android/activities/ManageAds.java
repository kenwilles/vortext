package com.vortextapp.android.activities;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.google.gson.Gson;
import com.vortextapp.android.R;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.libraries.RESTClient;
import com.vortextapp.android.libraries.RESTResponder;
import com.vortextapp.android.models.AdsModel;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.services.AdService.AdsResponse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;


public class ManageAds extends Activity {
	private static final String TAG = "VorText: ManageAds";
	private AdsModel ads;
	private ProgressDialog progress_window;
	 long total_ads = 0;
	 private Response rest_responder;
	 long available_ads = 0;
	 long delivered_ads = 0;
	 private  Context context ;//= getApplicationContext();
	 private PreferencesModel prefs = PreferencesModel.getInstance();
	TextView  totalAds,availableAds,deliveredAds;
	Button getAdsButton;
	String my_hash_id;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manage_ads);
		totalAds = (TextView)findViewById(R.id.total_ads);
		availableAds = (TextView)findViewById(R.id.available_ads);
		deliveredAds = (TextView)findViewById(R.id.delivered_ads);
		getAdsButton = (Button)findViewById(R.id.get_ads_button);
		getAdsButton.setOnClickListener(getAdsButtonListener);
		
		context = getApplicationContext();
		if (context == null){
    		context = getBaseContext()	;
		}
		if (context == null){
		context = this;
		}
		
		ads = new AdsModel(context);
		refresh();
		
		
		
	}

	private View.OnClickListener getAdsButtonListener = new View.OnClickListener() {

		
		public void onClick(View v) {
			getAds();
		}
//
	};
	
	public void getAds(){
    	//android.os.Debug.waitForDebugger();
		ads.clearExpired();
		my_hash_id = prefs.getAccountHashId();
		int my_ads_count;
		if((my_hash_id != null) && (!my_hash_id.equals("0")) && (!my_hash_id.equals(""))){
			my_ads_count = ads.getUnreadAdsForHashId(my_hash_id).size();
		}else{
			my_ads_count = ads.getUnreadAds().size();
		}
        if(my_ads_count<50){ // don't get more if we don't need to
            
            String qty = Globals.AD_DELIVER_QTY; //TODO this will need to get dynamically set depending on how many text messages they send on average. 
            //android.os.Debug.waitForDebugger();
            //String service = URLCollection.API + "get_ads/";
            //String service = "portal/api10/get_ads/";
            //android.os.Debug.waitForDebugger();
            MultiValueMap parts = new LinkedMultiValueMap();
            parts.add("hash_id", prefs.getAccountHashId());
            parts.add("qty", qty);
            parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);
             
            rest_responder = new Response();
            RESTClient client = new RESTClient(rest_responder, parts);
    		if (client.isOnline()) {
    			//android.os.Debug.waitForDebugger();
            	//client.execute(service);
            	client.execute("https://www.rocketlinkmobile.com/portal/api10/get_ads/");
            	Toast.makeText(getApplicationContext(), "More ads are being downloaded!", Toast.LENGTH_LONG).show();

			refresh();
    		} else {//
    			Toast.makeText(getApplicationContext(), "Needs internet connection!", Toast.LENGTH_LONG).show();
    		}
        }
        else
        {
        	Toast.makeText(getApplicationContext(), "Already have enough ads!", Toast.LENGTH_LONG).show();
        }
    }
	
	public void refresh(){
		my_hash_id = prefs.getAccountHashId();
		if((my_hash_id != null) && (!my_hash_id.equals("0")) && (!my_hash_id.equals(""))){
			delivered_ads = ads.getReadAdsNumberForHashId(my_hash_id);
			available_ads = ads.getUnreadAdsNumberForHashId(my_hash_id);
		}else{
			delivered_ads = ads.getReadAdsNumber();
			available_ads = ads.getUnreadAdsNumber();
		}
		
		total_ads =  delivered_ads + available_ads;
		totalAds.setText("Total ads: " + String.valueOf(total_ads));
		availableAds.setText("Available: " + String.valueOf(available_ads));
		deliveredAds.setText("Delivered: " + String.valueOf(delivered_ads));
	}
	
	private class Response implements RESTResponder {
        public void handleRESTResponse(String response){
             
            Gson gson = new Gson();
            AdsResponse ads_from_server = gson.fromJson(response, AdsResponse.class);
 
            if(ads_from_server.wereAdsFound()==true){
            	ads.insertNewAds(ads_from_server.ads);
                prefs.setAreReceiptsSynced(false); // this marks it for synchronization later in 5 mins
                if  ((available_ads+ads_from_server.countAds())>=50){ 
                prefs.setNeedAdsImmediately(false);
                }
                if(Globals.DEBUG) Log.v(TAG, "AdsResponse "+ads_from_server.wereAdsFound()+" ad pieces array length "+ads_from_server.ads.size());
    			
            } else {
                //
            	if(Globals.DEBUG) Log.v(TAG, "No Ads were found from server, response "+response);
            }
            
            //stopSelf(); // we are done for now
            
        }
    }

}
