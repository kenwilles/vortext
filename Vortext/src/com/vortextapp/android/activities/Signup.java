package com.vortextapp.android.activities;

import com.vortextapp.android.R;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Signup extends Activity {
	
	private static final String TAG = "VorText: Signup"; // for debug label
	private URLCollection service_url;
	private WebView webView;
	private String service_path;
	private final Handler handler = new Handler();

    
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(Globals.DEBUG) Log.v(TAG, "onCreate called");
        
        // settings
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.signup);
        webView = (WebView) findViewById(R.id.web_view);
        
        service_url = new URLCollection();
        service_path = "vortext/signup/";
        
        openBrowser();

    }
    
    
    protected void onStop() {
        super.onStop();
        finish();
    }
    
    protected void onDestroy() {
		super.onDestroy();
    }
    
    private void openBrowser(){
    	
		String loc = service_url.getServiceURL(service_path);
		if(Globals.DEBUG) Log.v(TAG, "service path location"+loc);
		
		webView.setWebViewClient(new SignupWebViewClient());
    	
    	webView.getSettings().setJavaScriptEnabled(true);
    	//webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
    	webView.addJavascriptInterface(new AndroidBridge(), "android");
    	webView.loadUrl(loc);
    	
    }
    
    /*
	 * This is used by javascript after signup is successful
	 */
    private class AndroidBridge {
    	@SuppressWarnings("unused")
		public void notifySuccessfulSignup(final String arg){ 
    		// must be final
    		handler.post(new Runnable(){
    			public void run(){
    				if(Globals.DEBUG) Log.v(TAG, "notifySuccessfulSignup for "+arg);
        			Intent intent = new Intent();
        			intent.putExtra("username", arg);
        		    intent.setClass(Signup.this.getBaseContext(), Login.class);
        		    startActivity(intent);
        		}
    		});
    	}
    }
    
    private class SignupWebViewClient extends WebViewClient {
        
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    
}