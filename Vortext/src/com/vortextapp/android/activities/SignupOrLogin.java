package com.vortextapp.android.activities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.Facebook.DialogListener;
import com.vortextapp.android.R;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.models.LoginModel;
import com.vortextapp.android.models.PreferencesModel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.util.Log;

public class SignupOrLogin extends Activity {
	
	private static final String TAG = "VorText: Signup or Login"; // for debug label
	private PreferencesModel prefs = PreferencesModel.getInstance();
	private URLCollection service_url;
	// Your Facebook APP ID
		private static String APP_ID = "160261344147034"; // Replace with your App ID

		// Instance of Facebook Class
		private Facebook facebook = new Facebook(APP_ID);
		private AsyncFacebookRunner mAsyncRunner;
		String FILENAME = "AndroidSSO_data";
		private SharedPreferences mPrefs ;
		private PreferencesModel mPrefs2 =  PreferencesModel.getInstance();
		private ProgressDialog progress_window;
		private LoginModel session;
		private String email;
		private String name;
		private String facebook_id;

    
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        
        if(Globals.DEBUG) Log.v(TAG, "onCreate called");
        
        // settings
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        try {
			if(prefs.isLoggedIn()==true){
				
				Intent intent = new Intent();
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    intent.setClass(this, MessageThreads.class);
			    startActivity(intent);
			    if(Globals.DEBUG) Log.v(TAG, "Already logged in");
			    
			} else {
				
				// view
			    setContentView(R.layout.signup_or_login);
			    
			    // setup events
			    Button login = (Button)findViewById(R.id.login);
			    login.setOnClickListener(loginBtnListener);
			    Button signup = (Button)findViewById(R.id.signup);
			    signup.setOnClickListener(signupBtnListener);
			    
			    //Stop all the running thread might be from logging out
			    stopThread();
			    service_url = new URLCollection();
			    if(Globals.DEBUG) Log.v(TAG, "Not logged in");
			    session = new LoginModel(this);
				mAsyncRunner = new AsyncFacebookRunner(facebook);
			    
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    
    
    protected void onStop() {
        super.onStop();
        finish();
    }
    
    protected void onDestroy() {
		super.onDestroy();
    }
    
    // Create an anonymous implementation of OnClickListener
    private View.OnClickListener loginBtnListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			
			Intent intent = new Intent();

		    intent.setClass(v.getContext(), Login.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    startActivity(intent);
		    //finish();
			
		}
	};
	
	// Create an anonymous implementation of OnClickListener
    private View.OnClickListener signupBtnListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {

			//String service_path = "vortext/signup/";
			//String loc = service_url.getServiceURL(service_path);
			//if(Globals.DEBUG) Log.v(TAG, "service path location"+loc);
			//progress_window = ProgressDialog.show(SignupOrLogin.this, "",
			//		"Logging in, please wait", true);
			//loginToFacebook();
			//CheckForDone task = new CheckForDone();
		    //task.execute(new String[] { "" });
			Intent intent = new Intent();

		    intent.setClass(v.getContext(), FacebookLogin.class);
			//intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    startActivity(intent);
			
		}
	};
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    switch(resultCode)
	    {
	    case 2:
	        setResult(2);
	        finish();
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * Function to login into facebook
	 * */
	@SuppressWarnings("deprecation")
	public void loginToFacebook() {

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);

		if (access_token != null) {
			
			getProfileInformation();
			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) {
			facebook.authorize(this,
					new String[] { "email", "publish_stream" },
					new DialogListener() {

						@Override
						public void onCancel() {
							// Function to handle cancel event
						}

						@Override
						public void onComplete(Bundle values) {
							// Function to handle complete event
							// Edit Preferences and update facebook acess_token
							SharedPreferences.Editor editor = mPrefs.edit();
							editor.putString("access_token",
									facebook.getAccessToken());
							editor.putLong("access_expires",
									facebook.getAccessExpires());
							editor.commit();

							getProfileInformation();
							//
						}
						@Override
						public void onError(DialogError error) {
							// Function to handle error

						}

						@Override
						public void onFacebookError(FacebookError fberror) {
							// Function to handle Facebook errors

						}

					});
		}
	}

	/**
	 * Get Profile information by making request to Facebook Graph API
	 * */
	public void getProfileInformation() {
		mAsyncRunner.request("me", new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
				Log.d("Profile", response);
				String json = response;
				try {
					// Facebook Profile JSON data
					JSONObject profile = new JSONObject(json);
					
					// getting name of the user
					name = profile.getString("name");
					
					// getting email of the user
					email = profile.getString("email");
					
					facebook_id = profile.getString("id");
					//startLoginServiceCall(email,facebook_id);
					mPrefs2.saveAsFBLoggedIn(facebook_id,email);
					    //finish();
					
									
					
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
				//startLoginServiceCall(email,facebook_id);
			}
			

			@Override
			public void onIOException(IOException e, Object state) {
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
		

		
	}
	private void goToNext() {
			progress_window.dismiss();
			Intent intent = new Intent();
			Context context = getApplicationContext();
	    	if(context == null)
	    		context = getBaseContext()	;
	    	if (context == null){
	    		context = this;
	    	}
			intent.setClass(context, CheckFacebook.class);
			if (context != null){
				setResult(2);
			    startActivity(intent);
			}
		
	}
	
	private class CheckForDone extends AsyncTask<String, Void, String> {

	      @Override
	      protected String doInBackground(String... params) {
	            while (!mPrefs2.isFBLoggedIn()) {
	                try {
	                    Thread.sleep(1000);
	                } catch (InterruptedException e) {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
	                }
	            }
	            try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            return null;
	      }      

	      @Override
	      protected void onPostExecute(String result) {  
	    	  
	    	  goToNext();
	      }

	      @Override
	      protected void onPreExecute() {
	      }

	      @Override
	      protected void onProgressUpdate(Void... values) {
	      }
	}
	public void stopThread(){
		  if(Thread.currentThread()!=null){
			  Thread.currentThread().interrupt();
		  }
		}
	
	
    
}