package com.vortextapp.android.activities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vortextapp.android.R;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;
import com.vortextapp.android.libraries.RESTClient;
import com.vortextapp.android.libraries.RESTResponder;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.AdsModel;
import com.vortextapp.android.models.EarningsModel;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.models.data.AdReceipt;
import com.vortextapp.android.models.tables.AdActivity;

public class PaymentOverview extends Activity {
	
	private static final String TAG = "VorText: PaymentOverview"; // for debug label
	Button claimButton;
	Button historyPaymentsButton;
	private ProgressDialog progress_window;
	private ProgressDialog progress_window_receipts;
	private ReceiptResponder receipt_responder;
	private PaymentResponder payment_responder;
	private double total;
	EarningsModel cash;
	private AdsModel ads_model;
	List<AdActivity> ad_activities;
	private PreferencesModel prefs = PreferencesModel.getInstance();

    
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(Globals.DEBUG) Log.v(TAG, "onCreate called");
        
        // settings
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.payment_overview);
        
        // create receipts from ad activity
        ads_model = new AdsModel(this);
        
        ImageButton composeMessageButton = (ImageButton) findViewById(R.id.composeBtn);
        composeMessageButton.setOnClickListener(composeMessageButtonListener);
        Button bonus = (Button) findViewById(R.id.bonus);
        bonus.setOnClickListener(bonusButtonListener);
        
        setEarningsDisplay();

    }
    
    private void setEarningsDisplay(){
    	
    	claimButton = (Button)findViewById(R.id.claim_payment);
    	historyPaymentsButton = (Button)findViewById(R.id.view_payments);
    	
    	
//    	Button earningsButton = (Button)findViewById(R.id.earningsButton);
    	TextView earnings=(TextView)findViewById(R.id.earnings);
    	TextView earningsPercentage=(TextView)findViewById(R.id.earningsPercentage);
    	
    	TextView earningsTotal = (TextView)findViewById(R.id.current_earnings_total);
    	TextView minimumClaimText = (TextView)findViewById(R.id.if_nothing_to_claim_text);
    	
		cash = new EarningsModel(this);
		total = (int)((cash.getTotalEarnings())*100);
		
		if(total >= Globals.MINIMUM_CLAIM){
			minimumClaimText.setVisibility(TextView.INVISIBLE); // hide warning text if above threshold
			minimumClaimText.setHeight(0);
			claimButton.setOnClickListener(claimButtonListener); // if we can claim then lets listen to touch
			
		} else {
			claimButton.setEnabled(false);
			//historyPaymentsButton.setEnabled(false);
		}
	
		// TODO need to implement history
		historyPaymentsButton.setOnClickListener(historyButtonListener);
		
//		DecimalFormat df = new DecimalFormat("##0.00");
//		String pretty_total = df.format(total);
		
		earningsTotal.setText(String.valueOf((int)total)+ " cents");
		earnings.setText(String.valueOf((int)total));
		earningsPercentage.setText(String.valueOf((int)total));
//		String minimum_claim = df.format(Globals.MINIMUM_CLAIM);
		TextView minimum_warning = (TextView)findViewById(R.id.if_nothing_to_claim_text);
		String minimum_warning_text = minimum_warning.getText().toString();
		minimum_warning.setText(minimum_warning_text.replace("$amount", String.valueOf(Globals.MINIMUM_CLAIM)));
//		earningsButton.setEnabled(false);
		
		Double percentage=(total/Globals.MINIMUM_CLAIM)*100;
		if(percentage>100)
			percentage=100d;
		double progressBarWidth=getResources().getDimension(R.dimen.progress_layout_width)-(2*getResources().getDimension(R.dimen.progress_layout_padding));
		double padding=progressBarWidth-(progressBarWidth*percentage/100);
		
		RelativeLayout earningsPercentageLayout=(RelativeLayout)findViewById(R.id.percentageLayout);
		earningsPercentageLayout.setPadding(0, 0, (int)padding, 0);

	}
    
    private View.OnClickListener claimButtonListener = new View.OnClickListener() {
    	
    	@SuppressWarnings({ "unchecked", "rawtypes" })
		
		public void onClick(View v) {
			// process claim
    		Context context = v.getContext();
			if (context == null)
				context = PaymentOverview.this.getApplicationContext();
			if (context == null)
				context = PaymentOverview.this.getBaseContext();
			// make sure all the receipts are posted to server first
    		progress_window_receipts = ProgressDialog.show(context,"","Synchronizing activity, please wait...",true);
    		
    		// create receipts from ad activity
        	ad_activities = ads_model.getUnsynchronizedActivities();

        	if(ad_activities.isEmpty() == true){
        		progress_window_receipts.dismiss();
        		processPayment();
        	} else {
        		processRemainingReceipts(ad_activities);
        	}
			
			

		}
		
	};
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void processRemainingReceipts(List<AdActivity>ad_activities){
		List<AdReceipt> receipts = new ArrayList<AdReceipt>();
		
		for (AdActivity activity : ad_activities) {
	    	//TODO eventually when opt-in stuff works, we'll need to specify different hash ids ...
	    	// CACHED, VIEWED, CLICKED
	    	AdReceipt receipt = new AdReceipt(	prefs.getAccountHashId(), 
	    										prefs.getAccountHashId(),
												activity.getServerAdId(),
												activity.getAd().getAdvertiserId(),
												activity.getActivityTypeId(),
												TimeManager.getTimeStampForServer(activity.getDate())
											 );
	    	receipts.add(receipt);

    	}
		
    	Gson gson = new Gson();
		String json_receipts = gson.toJson(receipts);

    	//String service = URLCollection.API + "post_ad_receipts/";
        MultiValueMap parts = new LinkedMultiValueMap();
        parts.add("hash_id", prefs.getAccountHashId());
        parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);
        
        parts.add("receipts", json_receipts); // array of AdReceipt
         
        receipt_responder = new ReceiptResponder();
        RESTClient client = new RESTClient(receipt_responder, parts);
        if (client.isOnline()){
        	client.execute("https://www.rocketlinkmobile.com/portal/api10/post_ad_receipts/");
        	//client.execute(service);
        } else {
        	new AlertDialog.Builder(PaymentOverview.this)

			.setTitle("Network Error")

			.setMessage("VorText could not establish a connection to the internet to process this request.")

			.setNeutralButton("OK",

			new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					
				}

			}).show();
        }
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void processPayment(){
		// start web service call in a blocking way (for now) use loading screen to indicate progress
		progress_window = ProgressDialog.show(PaymentOverview.this,"","Sending payment request, please wait...",true);
		//String service = URLCollection.API + "post_payment_request/";

		MultiValueMap parts = new LinkedMultiValueMap();
		parts.add("hash_id", prefs.getAccountHashId());
		parts.add("total", ((Double)total).toString());
		parts.add(URLCollection.API_USERNAME, URLCollection.API_PASSWORD);
		
		payment_responder = new PaymentResponder();
		RESTClient client = new RESTClient(payment_responder, parts);
		if (client.isOnline()){
			client.execute("https://www.rocketlinkmobile.com/portal/api10/post_payment_request");
			//client.execute(service);
		} else {
			new AlertDialog.Builder(PaymentOverview.this)

			.setTitle("Network Error")

			.setMessage("VorText could not establish a connection to the internet to process this request.")

			.setNeutralButton("OK",

			new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					
				}

			}).show();
		}
	}
	
	private View.OnClickListener bonusButtonListener = new View.OnClickListener() {
		public void onClick(View v) {
			Toast.makeText(v.getContext(), "It does not work yet.", Toast.LENGTH_SHORT).show();
		}
		
	};
	
	private View.OnClickListener composeMessageButtonListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			Intent intent = new Intent();
			Context context = v.getContext();
			if (context == null)
				context = PaymentOverview.this.getApplicationContext();
			if (context == null)
				context = PaymentOverview.this.getBaseContext();
		    intent.setClass(context, ComposeMessage.class);
		    startActivity(intent);
		    //finish();
		}
	};
	
	private View.OnClickListener historyButtonListener = new View.OnClickListener() {
		
		
		public void onClick(View v) {
			Context context = v.getContext();
			if (context == null)
				context = PaymentOverview.this.getApplicationContext();
			if (context == null)
				context = PaymentOverview.this.getBaseContext();
			Toast.makeText(context, "You will be able to see a history of payments in a future release", Toast.LENGTH_LONG).show();
			// Intent intent = new Intent();
			// intent.setClass(v.getContext(), PaymentHistory.class);
			// v.getContext().startActivity(intent);
		}
		
	};
    
    
    protected void onStop() {
        super.onStop();
        cash.close();
        ads_model.close();
        finish();
    }

    protected void onDestroy() {
		super.onDestroy();
    }
    
    private class ReceiptResponder implements RESTResponder {
   
		public void handleRESTResponse(String response){
			if(Globals.DEBUG) Log.v(TAG, response);
             
            Gson gson = new Gson();
            ReceiptResponse receipt_response = gson.fromJson(response, ReceiptResponse.class);
            //if(Globals.DEBUG) Log.v(TAG, "AdsResponse "+o.wereAdsFound()+" ad pieces array length "+o.ads.size());
 
            if(receipt_response.wasPostSuccessful()==true){
            	if(Globals.DEBUG) Log.v(TAG, "post was successful");
                // TODO this really needs to be more bullet proof
                boolean result = ads_model.setActivitiesToSynchronized(ad_activities);
                
                progress_window_receipts.dismiss();
                
                // Let's process payment now
                processPayment();
                
            } else {
                //
            	progress_window_receipts.dismiss();
            	new AlertDialog.Builder(PaymentOverview.this)

				.setTitle("Synchronization Error")

				.setMessage("Earning activity could not be synchronized for payment request--please try again later.")

				.setNeutralButton("OK",

				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						
					}

				}).show();
            	if(Globals.DEBUG) Log.e(TAG, "No receipts were posted to server");
            }

        }
    }
     
    public class ReceiptResponse {
         
        private int success;
         
        public Boolean wasPostSuccessful() {
            if(success==1){
                return true;
            } else {
                return false;
            }
        }
         
    }
    
    private class PaymentResponder implements RESTResponder {
		public void handleRESTResponse(String response){
			if(Globals.DEBUG) Log.v(TAG, response);
			
			Gson gson = new Gson();
			PaymentResponse payment_response = gson.fromJson(response, PaymentResponse.class);
			//if(Globals.DEBUG) Log.v(TAG, "AdsResponse "+o.wereAdsFound()+" ad pieces array length "+o.ads.size());

			if(payment_response.wasPostSuccessful()==true){
				if(Globals.DEBUG) Log.v(TAG, "post was successful");
				
				// record it on our end. 
			    cash.compileEarnings();
			    
				Intent intent = new Intent();
				intent.setClass(PaymentOverview.this, PaymentRequest.class);
				
				//TODO Left off here and need to work on earnings model
				intent.putExtra("email", prefs.getAccountEmail());
			    intent.putExtra("total", total);
				
				PaymentOverview.this.startActivity(intent);
				progress_window.dismiss();
				PaymentOverview.this.finish();
				
			} else {
				//
				progress_window.dismiss();
				
				new AlertDialog.Builder(PaymentOverview.this)

				.setTitle("Payment Error")

				.setMessage("Payment request not successful, please try again later")

				.setNeutralButton("OK",

				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						
					}

				}).show();
				
				if(Globals.DEBUG) Log.e(TAG, "No receipts were posted to server");
			}
		}
	}

    public class PaymentResponse {

    	private int success = 0;

		public Boolean wasPostSuccessful() {
			if(success==1){
				return true;
			} else {
				return false;
			}
		}

	}

    
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item){
		//:TODO find a better way to accomplish this
				//This code only works with higher adk
				//switch (item.getItemId()) {
				//case R.id.settings:
					startActivity(new Intent(this, Preferences.class));
					return true;
				//}
				//return false;
	}

}