package com.vortextapp.android.activities;

import java.util.Date;
import java.util.List;

import com.vortextapp.android.R;
import com.vortextapp.android.VorTextApp;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.libraries.TimeManager;
import com.vortextapp.android.models.AdsModel;
import com.vortextapp.android.models.PreferencesModel;

import com.vortextapp.android.models.tables.AdActivity;
import com.vortextapp.android.models.tables.AdActivityTypes;
import com.vortextapp.android.models.tables.Ads;
import com.vortextapp.android.models.tables.Messages;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;

import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class MessageThreadAdapter extends ArrayAdapter<Messages> {
	
	private static final String TAG = "VorText: MessageThreadAdapter"; // for debug
	private final Context context;
	private List<Messages> messages;
	public AdTextView adText;
	
	private PreferencesModel prefs = PreferencesModel.getInstance();

	public MessageThreadAdapter(Context context, int textViewResourceId, List<Messages> list) {
		super(context, textViewResourceId, list);
		// TODO Auto-generated constructor stub
		this.messages = list;
		this.context = context;
		
	}
	
	 static class ViewHolder {
		    public TextView field_message;
		    public TextView field_time;
		    public TextView field_ad;
		    LinearLayout image;
		    //public String first_name;
		  }
	
    public View getView(int position, View convertView, ViewGroup parent) {
    		Messages myMessages = messages.get(position);
    		View rowView = convertView;
    		ViewHolder viewHolder; 
            if (rowView == null){
    		LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		rowView = vi.inflate(R.layout.message_detail_from_me, null);
            viewHolder = new ViewHolder();
            viewHolder.field_message = (TextView) rowView.findViewById(R.id.recent_message_in_thread_other);
            viewHolder.field_time = (TextView) rowView.findViewById(R.id.recent_message_time_in_thread_other);
            viewHolder.field_ad = (TextView) rowView.findViewById(R.id.ad_space_other);
            viewHolder.image = (LinearLayout)  rowView.findViewById(R.id.message_area_in_thread_other);
            //viewHolder.first_name = messages.get(position).getContact().getFirstName();
        	rowView.setTag(viewHolder);	
        	}
            viewHolder = (ViewHolder) rowView.getTag();
            adText = (AdTextView) rowView.findViewById(R.id.ad_space_other);
            adText.ad = myMessages.getAd();
    		adText.setOnClickListener(adTextListener);
    		if(!myMessages.getContact().getFirstName().equals("Me")){
    			viewHolder.image.setBackgroundResource(R.drawable.their);
    				((RelativeLayout) rowView).setGravity(Gravity.LEFT);				
    			
    		}
    		else {
    			viewHolder.image.setBackgroundResource(R.drawable.mine);
				((RelativeLayout) rowView).setGravity(Gravity.RIGHT);	
    		}
            //Messame
    		viewHolder.field_message.setText(myMessages.getMessage());
            //TIME
            	String status = myMessages.getStatus();
                if (status.equalsIgnoreCase("Sending") || status.equalsIgnoreCase("Sent") || status.equalsIgnoreCase("Recieved"))
                {
            	Date timestamp = new Date();
            	timestamp.setTime(myMessages.getDate());
            	//for now take out the year
            	SimpleDateFormat formatter = new SimpleDateFormat("MMM d, h:mm a");
                //String time = DateFormat.getDateTimeInstance().format(timestamp);
            	String time = formatter.format(timestamp);
            	viewHolder.field_time.setText(time);
                }
                else
                	viewHolder.field_time.setText("Not sent!");
             //Ads   
            
            	String ad_text = myMessages.getAdText();
            	if(ad_text != null ){
            		viewHolder.field_ad.setVisibility(View.VISIBLE);
            		viewHolder.field_ad.setText("*"+ad_text+"*");
            		viewHolder.field_ad.setTextSize(9);
            	} else {
            		viewHolder.field_ad.setVisibility(View.GONE);
            	}
            

            return rowView;

	}
	
	private View.OnClickListener adTextListener = new View.OnClickListener() {

		
		public void onClick(View v) {			
			
			//@SuppressWarnings("unused")

			//int sel_start = adText.getSelectionStart();
			//if(Globals.DEBUG) Log.i(TAG, "sel_start "+sel_start);
			
			AdTextView atv = (AdTextView) v;
			Ads a = (Ads) atv.ad;

			AdActivity ad_activity = new AdActivity(a, a.getServerAdId(), AdActivityTypes.CLICKED, 
					TimeManager.getCurrentSyncronizedTimeStamp(), false);
			
			AdsModel ads = new AdsModel(context);
			ads.recordAdActivity(ad_activity);
			prefs.setAreReceiptsSynced(false);
			
			// TODO eventually maybe ClickableSpan approach would be more accurate
			//http://stackoverflow.com/questions/1697084/handle-textview-link-click-in-my-android-app
			//if(adText.getSelectionStart()==-1 && adText.getSelectionEnd()==-1){
				//if(Globals.DEBUG) Log.i(TAG, "clicked outside of hyperlink");
            //} else {
            	// clicked something that was hyperlinked
            	//if(Globals.DEBUG) Log.i(TAG, "clicked on hyperlink of some sort");
           // }


		}
	};
	
	public  Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        int height = output.getHeight();
        int width = output.getWidth();
        int density = output.getDensity();
        int some = output.getRowBytes();
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 6;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
	
}