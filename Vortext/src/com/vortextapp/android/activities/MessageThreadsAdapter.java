package com.vortextapp.android.activities;

import java.util.Date;
import java.util.List;
import java.util.Random;


import com.vortextapp.android.R;
import com.vortextapp.android.activities.MessageThreadAdapter.ViewHolder;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.models.tables.Messages;
import com.vortextapp.android.models.tables.Threads;
import com.vortextapp.android.libraries.*;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;




import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MessageThreadsAdapter extends ArrayAdapter<Messages> implements OnClickListener {
	
	public static final String TAG = "MessageThreadsAdapter";
	private final Context context;
	private List<Messages> messages;
	private QuickAction quickAction ;
	private String my_position;

	public MessageThreadsAdapter(Context context, int textViewResourceId, List<Messages> list) {
		super(context, textViewResourceId, list);
		// TODO Auto-generated constructor stub
		this.messages = list;
		this.context = context;
	}
	 static class ViewHolder {
		    public TextView contact_name;
		    public TextView recent_message_time;
		    public TextView lastest_message;
		    public RelativeLayout thread_item;
		    public ImageView contact_image;
		  }
	
	
    public View getView( int position, View convertView, ViewGroup parent) {
    		Messages myMessages = messages.get(position);
    		View rowView = convertView;
    		ViewHolder viewHolder; 
    		if (rowView == null){
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = vi.inflate(R.layout.message_threads_item, null);
            viewHolder = new ViewHolder();
            viewHolder.thread_item = (RelativeLayout) rowView.findViewById(R.id.thread_item);
            viewHolder.contact_name = (TextView) rowView.findViewById(R.id.contact_name);
            viewHolder.recent_message_time = (TextView) rowView.findViewById(R.id.recent_message_time);   			     
            viewHolder.lastest_message = (TextView)rowView.findViewById(R.id.lastest_message);
            viewHolder.contact_image = (ImageView)rowView.findViewById(R.id.contact_image);        
            rowView.setTag(viewHolder);	
        	}
    		viewHolder = (ViewHolder) rowView.getTag();
            //save position to the tag of the image for using later when image is clicked
            viewHolder.contact_image.setTag(new Integer(position));
            viewHolder.contact_image.setOnClickListener(this);
            if (viewHolder.contact_name != null) {
            	StringBuilder sb = new StringBuilder();
            	String first_name = myMessages.getThread().getContact().getFirstName();
            	String last_name = myMessages.getThread().getContact().getLastName();
            	String image_url = myMessages.getThread().getContact().getPicture();
            	if(last_name == null){
            		sb.append(first_name);
            	} else {
            		sb.append(first_name+" "+last_name);
            	}
            	String message_to_show = myMessages.getMessage();   
            	
            	viewHolder.contact_name.setText(sb);
            	//set image for contact
            	if (image_url != null)
            	{
            	Bitmap bitmap = null;
				try {
					bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), Uri.parse(image_url));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				viewHolder.contact_image.setImageURI(Uri.parse(image_url));
				if (bitmap != null){
					viewHolder.contact_image.setImageBitmap(getRoundedCornerBitmap(bitmap, 10));
				}else{
					Uri mine_uri = Uri.parse(image_url);
					viewHolder.contact_image.setImageURI(mine_uri);
				}
            	}
				int app_version = getAppVersionCode(getContext(), "com.vortextapp");
            	//make sure the image is not empty
            	if(viewHolder.contact_image.getDrawable() == null) viewHolder.contact_image.setImageResource(R.drawable.friend_icon);	
            	//set the latest message to be shown
            	viewHolder.lastest_message.setText(message_to_show);
            }


            if(viewHolder.recent_message_time != null){
            	Date timestamp = new Date();
            	timestamp.setTime(myMessages.getDate());
            	SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, ''yy - h:mm a");
            	String time = formatter.format(timestamp);
            	viewHolder.recent_message_time.setText(time);
            }
            
            if(myMessages.getIsRead()==false){
            	viewHolder.thread_item.setBackgroundColor(Globals.GREEN);
            }else{
            	viewHolder.thread_item.setBackgroundColor(0xfff);
            }
            
            
            
            
            
          //action id
			final int ID_CALL_PHONE     = 1;
			final int ID_VIEW_CONTACT   = 2;
            //@Hai
            //For pop up menu
            ActionItem callItem 	= new ActionItem(ID_CALL_PHONE, "Call", this.context.getResources().getDrawable(android.R.drawable.ic_menu_call));
    		ActionItem viewItem 	= new ActionItem(ID_VIEW_CONTACT, "View", this.context.getResources().getDrawable(android.R.drawable.ic_menu_info_details));
    		
    		//use setSticky(true) to disable QuickAction dialog being dismissed after an item is clicked
            callItem.setSticky(true);
            viewItem.setSticky(true);
            
            //create QuickAction. Use QuickAction.VERTICAL or QuickAction.HORIZONTAL param to define layout 
            //orientation
    		quickAction = new QuickAction(this.getContext(), QuickAction.HORIZONTAL);
    		
    		
    		//add action items into QuickAction
    		 quickAction.addActionItem(callItem);
    		quickAction.addActionItem(viewItem);
    		
    		//handle onlick
    		quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {			
			
			public void onItemClick(QuickAction source, int pos, int actionId) {				
				ActionItem actionItem = quickAction.getActionItem(pos);
				String selected_phone_number =  messages.get(Integer.parseInt(my_position)).getThread().getContact().getPhone1();
				String prefix = selected_phone_number.substring(0, 1); 
				if (prefix.equals("+")){
					selected_phone_number = selected_phone_number.substring(2);
				}
				
				
				
				//phoneUtil.format(num.iterator(), PhoneNumberFormat.NATIONAL);
				
				//Handle the Calling action
				if (actionId == ID_CALL_PHONE) {
					
					Toast.makeText(context, "Calling "+ selected_phone_number, Toast.LENGTH_SHORT).show();
					String uri = "tel:" + selected_phone_number.trim();
					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse(uri));
					context.startActivity(intent);
				} else if (actionId == ID_VIEW_CONTACT) {
				//Handle the Viewing contact action
					
					
					Intent intent = new Intent(Intent.ACTION_VIEW);
					int contact_ID = getContactIDFromNumber(selected_phone_number,context);
					if(contact_ID >=0){
					intent.setData(Uri.parse("content://com.android.contacts/contacts/"+String.valueOf(contact_ID)));
					//Toast.makeText(context, person +" \n "+uri, Toast.LENGTH_SHORT).show();
					context.startActivity(intent);
					}
					else{
						Toast.makeText(context,"Could not find contact in phone's local contact!", Toast.LENGTH_SHORT).show();
					}
				} 
			}
		});
            
            
            
            return rowView;

	}
	
	public Integer getThreadId(int position){

		return messages.get(position).getThread().getId();
	}
	
	public String getPhoneNumber(int position){

		return messages.get(position).getThread().getContact().getPhone1();
	}
	
	public static String truncate(String value, int length)
	{
	  if (value != null && value.length() > length){
	    value = value.substring(0, length);
	  }
	  return value+"...";
	}
	public  Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
	        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
	                .getHeight(), Config.ARGB_8888);
	        int height = output.getHeight();
	        int width = output.getWidth();
	        int density = output.getDensity();
	        int some = output.getRowBytes();
	        Canvas canvas = new Canvas(output);

	        final int color = 0xff424242;
	        final Paint paint = new Paint();
	        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	        final RectF rectF = new RectF(rect);
	        final float roundPx = 6;

	        paint.setAntiAlias(true);
	        canvas.drawARGB(0, 0, 0, 0);
	        paint.setColor(color);
	        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

	        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	        canvas.drawBitmap(bitmap, rect, rect, paint);

	        return output;
	    }
	public static int getAppVersionCode(Context context, String appPackageName) {
	    if ( context!= null )
	    {
	        try {
	            return context.getPackageManager().getPackageInfo(appPackageName, 0).versionCode;
	        } catch (NameNotFoundException e) {
	            // App not installed!
	        }
	    }
	    return -1;
	}

	
	public void onClick(final View v) {
		// TODO Auto-generated method stub
		quickAction.show(v);
		//extract position from image's tag
		my_position = v.getTag().toString();
//		new AlertDialog.Builder(v.getContext())
//	    .setTitle("Contact Image Pressed")
//	    .setMessage("Choose action!")
//	    .setPositiveButton("Call number", new DialogInterface.OnClickListener() {
//	        public void onClick(DialogInterface dialog, int which) { 
//	        	String posted_by = "111-333-222-4";
//
//	        	 String uri = "tel:" + posted_by.trim() ;
//	        	 Intent intent = new Intent(Intent.ACTION_CALL);
//	        	 intent.setData(Uri.parse(uri));
//	        	 v.getContext().startActivity(intent);
//	        }
//	     })
//	    .setNegativeButton("View Contact", new DialogInterface.OnClickListener() {
//	        public void onClick(DialogInterface dialog, int which) { 
//	            // do nothing
//	        }
//	     })
//	     .show();
		
	}
	
	public static int getContactIDFromNumber(String contactNumber,Context context)
	{
	    contactNumber = Uri.encode(contactNumber);
	    int phoneContactID = new Random().nextInt();
	    Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,Uri.encode(contactNumber)),new String[] {PhoneLookup.DISPLAY_NAME, PhoneLookup._ID}, null, null, null);
	        while(contactLookupCursor.moveToNext()){
	            phoneContactID = contactLookupCursor.getInt(contactLookupCursor.getColumnIndexOrThrow(PhoneLookup._ID));
	            }
	        contactLookupCursor.close();

	    return phoneContactID;
	}
}