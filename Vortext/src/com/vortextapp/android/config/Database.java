package com.vortextapp.android.config;

/**
 * Constants for database
 * 
 * @author Ken Willes
 *
 */
public final class Database {
	
	public static final String NAME = "vortext";
	public static final int VERSION = 2;
	public static final String TECHNOLOGY = "ormlite"; //built on top of sqlite
	
	private Database(){
		throw new AssertionError();
	}
}
