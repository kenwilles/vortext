package com.vortextapp.android.config;

/**
 * Constants for program
 * 
 * @author Ken Willes
 *
 */
public final class Globals {
	
	public static int LOGLEVEL = 0;
	public static boolean WARN = LOGLEVEL > 1;
	public static boolean DEBUG = LOGLEVEL > 0;
	
	public static final String ENVIRONMENT = "production"; // development or production
	public static final Boolean IS_LOGIN_SKIPPED = false; // development or production
	public static final String PRODUCTION = "production";
	public static final String DEVELOPMENT = "development";

	public static final String BKS_PASSWORD = "rockets5uc3";
	public static final String SUPPORT_EMAIL = "support@rocketlinkmobile.com";
		
	public static final int RED = 0xffa81237;
	public static final int BLUE = 0xff83c8ef;
	public static final int GREEN = 0xff7cbf32;
	
	public static final int MINIMUM_CLAIM = 1500;
	public static final String AD_DELIVER_QTY = "50";

	// TODO server time origin
	public static final String SERVER_TIME_ORIGIN = "GMT";
	
	private Globals(){
		throw new AssertionError();
	}
}
