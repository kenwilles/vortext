package com.vortextapp.android.config;

/**
 * Intent filter actions for program
 * 
 * @author Ken Willes
 *
 */
public final class Actions {
	
	// intent filters
	public static final String LOGIN 					= "com.vortextapp.android.config.Actions.LOGIN";
	public static final String VIEW_SINGLE_THREAD		= "com.vortextapp.android.config.Actions.VIEW_SINGLE_THREAD";
	public static final String COMPOSE 					= "com.vortextapp.android.config.Actions.COMPOSE";
	public static final String MESSAGE_RECEIVED 		= "com.vortextapp.android.config.Actions.MESSAGE_RECEIVED";
	public static final String START_SERVICE_SCHEDULE 	= "com.vortextapp.android.config.Actions.START_SERVICE_SCHEDULE";
	
	private Actions(){
		throw new AssertionError();
	}
}
