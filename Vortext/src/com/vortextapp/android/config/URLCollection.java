package com.vortextapp.android.config;

import java.util.HashMap;
import java.util.Map;

import android.util.Log;

public class URLCollection {
	
	public static final String API_USERNAME = "android";
	public static final String API_PASSWORD = "xJilD83dkcP";
	public static final String API = "portal/api10/";
	
	public static final int GET = 0;
	public static final int POST = 1;
	
	private Map<String, URL> urls;
	//private String mode;
	private URL url;
	private static final String TAG = "VorText: URLCollection"; // for debug label
	
	public URLCollection(){
		urls = new HashMap<String, URL>();
		urls.put("https_production", new URL("https", "www.rocketlinkmobile.com", "443"));
		//urls.put("http_production", new URL("http", "www.rocketlinkmobile.com", "80"));
		urls.put("http_development", new URL("http", "192.168.1.2", "80"));
		//urls.put("http_development", new URL("http", "dev.rocketlinkmobile.com", "80"));
		setURLService();
	}
	
	@SuppressWarnings("unused")
	private void setURLService(){
		if(Globals.ENVIRONMENT==Globals.PRODUCTION){
			url = urls.get("https_production");
			if(Globals.DEBUG) Log.v(TAG, "setURLService to https_production");
		} else {
			// must be development
			url = urls.get("http_development");
			if(Globals.DEBUG) Log.v(TAG, "setURLService to http_development");
		}
	}
	
	public String getServiceURL(String service_path){
		url.setService(service_path);
		//if(Globals.DEBUG) Log.v(TAG, "getServiceURL "+url.getURL());
		return url.getURL();
	}
		
	
}
