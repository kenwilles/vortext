package com.vortextapp.android.config;

import android.util.Log;

public final class URL {
	
	private static final String TAG = "VorText: URL"; // for debug label
	
	private String protocol;
	private String domain;
	private String port;
	private String service_path;
	
	public URL(String protocol, String domain, String port){
		init(protocol, domain, port, "/");
	}
	
	public URL(String protocol, String domain, String port, String service_path){
		init(protocol, domain, port, service_path);
	}
	
	public void init(String protocol, String domain, String port, String service_path){
		this.protocol = protocol;
		this.domain = domain;
		this.port = port; 
		this.service_path = service_path;
	}

	public String getProtocol() {
		return protocol;
	}

	public String getDomain() {
		return domain;
	}

	public String getPort() {
		return port;
	}

	public String getServiceRoot() {
		return service_path;
	}
	
	public void setService(String service_path){
		this.service_path += service_path;
	}
	
	public String getURL(){
		String u = protocol+"://"+domain+":"+port+service_path;
		//if(Globals.DEBUG) Log.v(TAG, "getURL: "+u);
		return u;
	}

	
}
