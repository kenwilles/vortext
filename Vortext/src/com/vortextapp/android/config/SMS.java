package com.vortextapp.android.config;

import android.net.Uri;

/**
 * Config Constants for SMS
 * 
 * @author Ken Willes
 *
 */
public final class SMS {
	
	public static final String THREADS = "conversations";
	public static final String THREAD = "conversation";
	
	// Content URIs for SMS app, these may change in future SDK
    public static final Uri MMS_SMS_CONTENT_URI = Uri.parse("content://mms-sms/");
    public static final Uri THREAD_ID_CONTENT_URI = Uri.withAppendedPath(MMS_SMS_CONTENT_URI, "threadID");
    public static final Uri CONVERSATION_CONTENT_URI = Uri.withAppendedPath(MMS_SMS_CONTENT_URI, "conversations");
    
    public static final String SMSTO_URI = "smsto:";
    public static final String UNREAD_CONDITION = "read=1";

    public static final Uri SMS_CONTENT_URI = Uri.parse("content://sms");
    public static final Uri SMS_INBOX_CONTENT_URI = Uri.withAppendedPath(SMS_CONTENT_URI, "inbox");

    public static final Uri MMS_CONTENT_URI = Uri.parse("content://mms");
    public static final Uri MMS_INBOX_CONTENT_URI = Uri.withAppendedPath(MMS_CONTENT_URI, "inbox");

    public static final String SMSMMS_ID = "_id";
    public static final String SMS_MIME_TYPE = "vnd.android-dir/mms-sms";
    public static final int READ_THREAD = 1;
    
    // Message types
    public static final int MESSAGE_TYPE_MESSAGE = 2;
    public static final int MESSAGE_TYPE_SMS = 1;
    public static final int MESSAGE_TYPE_MMS = 2;

    public static final int CONTACT_PHOTO_PLACEHOLDER = android.R.drawable.ic_dialog_info;

    // The max size of either the width or height of the contact photo
    public static final int CONTACT_PHOTO_MAXSIZE = 1024;
    
 // http://android.git.kernel.org/?p=platform/frameworks/base.git;a=blob;f=core/java/android/provider/Telephony.java
    public static final String REPLY_PATH_PRESENT = "reply_path_present";
    public static final String SERVICE_CENTER = "service_center";
    // public static final String DATE = "date";
    /**
     * The thread ID of the message
     * <P>
     * Type: INTEGER
     * </P>
     */
    public static final String THREAD_ID = "thread_id";

    /**
     * The address of the other party
     * <P>
     * Type: TEXT
     * </P>
     */
    public static final String ADDRESS = "address";

    /**
     * The person ID of the sender
     * <P>
     * Type: INTEGER (long)
     * </P>
     */
    public static final String PERSON_ID = "person";

    /**
     * The date the message was sent
     * <P>
     * Type: INTEGER (long)
     * </P>
     */
    public static final String DATE = "date";

    /**
     * Has the message been read
     * <P>
     * Type: INTEGER (boolean)
     * </P>
     */
    public static final String READ = "read";

    /**
     * The TP-Status value for the message, or -1 if no status has been received
     */
    public static final String STATUS = "status";
    public static final int STATUS_NONE = -1;
    public static final int STATUS_COMPLETE = 0;
    public static final int STATUS_PENDING = 64;
    public static final int STATUS_FAILED = 128;

    /**
     * The subject of the message, if present
     * <P>
     * Type: TEXT
     * </P>
     */
    public static final String SUBJECT = "subject";

    /**
     * The body of the message
     * <P>
     * Type: TEXT
     * </P>
     */
    public static final String BODY = "body";

    public static final String TYPE = "type";

    public static final int MESSAGE_TYPE_ALL    = 0;
    public static final int MESSAGE_TYPE_INBOX  = 1;
    public static final int MESSAGE_TYPE_SENT   = 2;
    public static final int MESSAGE_TYPE_DRAFT  = 3;
    public static final int MESSAGE_TYPE_OUTBOX = 4;
    public static final int MESSAGE_TYPE_FAILED = 5; // for failed outgoing messages
    public static final int MESSAGE_TYPE_QUEUED = 6; // for messages to send later
    
 // http://android.git.kernel.org/?p=platform/packages/apps/Mms.git;a=blob;f=src/com/android/mms/transaction/MessageStatusReceiver.java
    public static final String MESSAGING_STATUS_RECEIVED_ACTION =
      "com.android.mms.transaction.MessageStatusReceiver.MESSAGE_STATUS_RECEIVED";
    public static final String MESSAGING_PACKAGE_NAME = "com.android.mms";
    public static final String MESSAGING_STATUS_CLASS_NAME =
      MESSAGING_PACKAGE_NAME + ".transaction.MessageStatusReceiver";
    public static final String MESSAGING_RECEIVER_CLASS_NAME =
      MESSAGING_PACKAGE_NAME + ".transaction.SmsReceiver";
    public static final String MESSAGING_CONVO_CLASS_NAME = "com.android.mms.ui.ConversationList";
    public static final String MESSAGING_COMPOSE_CLASS_NAME = "com.android.mms.ui.ComposeMessageActivity";
    
    // http://android.git.kernel.org/?p=platform/packages/apps/Mms.git;a=blob;f=src/com/android/mms/transaction/SmsReceiverService.java
    public static final String MESSAGE_SENT_ACTION = "com.android.mms.transaction.MESSAGE_SENT";
    
    public static final String FROM_ADDRESS_EXTRA = "com.vortextapp.android.SMS_FROM_ADDRESS";
    public static final String MESSAGE_EXTRA = "com.vortextapp.android.SMS_MESSAGE";
    public static final String NOTIFICATION_ID_EXTRA = "com.vortextapp.android.NOTIFICATION_ID";
	
	private SMS(){
		throw new AssertionError();
	}
}
