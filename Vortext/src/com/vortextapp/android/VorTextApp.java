package com.vortextapp.android;

//import com.vortextapp.android.config.Actions;

import com.vortextapp.android.models.PreferencesModel;

import android.app.Application;
//import android.content.Intent;

public class VorTextApp extends Application {
	
	public static final String TAG = "VorTextApp";
    static VorTextApp v;
    public PreferencesModel prefs; 

    
    public void onCreate() {
        super.onCreate();
        v = this;
        initServices();
        prefs = PreferencesModel.getInstance(); // exist once
    }
    
    private void initServices(){
    	//How to start scheduler if boot isn't working
    	//This is now called in login so this shouldn't be necessary
    	//Intent intent = new Intent(Actions.START_SERVICE_SCHEDULE);
    	//intent.addCategory("android.intent.category.ALTERNATIVE");
    	//getApplicationContext().sendBroadcast(intent);
    }

    public static VorTextApp getApp() {
        return v;
    }

}
