package com.vortextapp.android.libraries;

import java.util.ArrayList;

import com.vortextapp.android.config.Globals;
import com.vortextapp.android.models.PreferencesModel;
import com.vortextapp.android.receivers.SmsSendStatusReceiver;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class SimpleSmsSender {
	
	private PreferencesModel prefs = PreferencesModel.getInstance();
	public static final String TAG = "VorText: SimpleSmsSender";
	private Context context;
	private SmsManager smsAgent;
	
	public SimpleSmsSender(Context c){
		context = c;
	}
	
	public void send(String address, String message){
		if(prefs.isLoggedIn()==true){
			if(Globals.DEBUG) Log.v(TAG, "onStartCommand");

			smsAgent = SmsManager.getDefault();
			ArrayList<String> messages = smsAgent.divideMessage(message);
			
			//Send empty message also
			if(message.length() >= 0){
				if(messages.size() > 1){
					sendMultiPartMessage(address, messages);
				} else {
					sendMessage(address, message);
				}
			} else {
				//Toast.makeText(context, "There was no message present to send!", Toast.LENGTH_SHORT);
				if(Globals.DEBUG) Log.e(TAG, "no message characters to send");
			}
			
		}
	}
	
	private void sendMessage(String address, String message){
		
		if(Globals.DEBUG) Log.v(TAG, "sendMessage");
		if (message.length() == 0)
			message = " ";
		// TODO the pending intents don't work, not sure why but messages are being sent
		Intent intent = new Intent(context, SmsSendStatusReceiver.class);
		//PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		
		

		smsAgent.sendTextMessage(address, null, message, sentIntent, null);

	}
	
	private void sendMultiPartMessage(String address, ArrayList<String> messages){
		
		if(Globals.DEBUG) Log.v(TAG, "sendMultiPartMessage");
		
		// TODO the pending intents don't work, not sure why but messages are being sent
		ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
		for(String msg : messages){
			Intent intent = new Intent(context, SmsSendStatusReceiver.class);
			PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			sentIntents.add(sentIntent);
		}
		smsAgent.sendMultipartTextMessage(address, null, messages, sentIntents, null);
	}

}
