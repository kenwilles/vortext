package com.vortextapp.android.libraries;

import com.vortextapp.android.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;

import org.apache.http.HttpResponse;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import android.content.Context;

public class SSLConnection extends DefaultHttpClient {
	
	final Context context;
	
	public SSLConnection(Context context) {
	      this.context = context;
	}
	
	
	  protected ClientConnectionManager createClientConnectionManager() {
		
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", createAdditionalCertsSSLSocketFactory(), 443));

		// and then however you create your connection manager, I use ThreadSafeClientConnManager
		final HttpParams params = new BasicHttpParams();
		//
		final ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(params,schemeRegistry);
		
		return cm;
		
	  }

	  protected SSLSocketFactory createAdditionalCertsSSLSocketFactory() {
		  
		    try {
		        final KeyStore ks = KeyStore.getInstance("BKS");

		        // the bks file we generated above
		        final InputStream in = context.getResources().openRawResource(R.raw.rocketlinkmobile2);  
		        try {
		            // don't forget to put the password used above in strings.xml/mystore_password
		            ks.load(in, context.getString( R.string.app_cert_password ).toCharArray());
		        } finally {
		            in.close();
		        }

		        return new AdditionalKeyStoresSSLSocketFactory(ks);

		    } catch( Exception e ) {
		        throw new RuntimeException(e);
		    }
		  
	  }
	  
	  public static String request(HttpResponse response){
	        String result = "";
	        try{
	            InputStream in = response.getEntity().getContent();
	            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	            StringBuilder str = new StringBuilder();
	            String line = null;
	            while((line = reader.readLine()) != null){
	                str.append(line + "\n");
	            }
	            in.close();
	            result = str.toString();
	        }catch(Exception ex){
	            result = "Error";
	        }
	        return result;
	    }
	  
}
