package com.vortextapp.android.libraries;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.vortextapp.android.VorTextApp;
import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.URLCollection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Credentials;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
//import android.util.Base64;
import android.util.Log;

public class RESTClient extends AsyncTask <String, Integer, String> {

	private static final String TAG = "VorText: RESTClient"; // for debug label
	private URLCollection service_url;
	private String url;
	private RESTResponder responder;
	private int transfer_type;
	private MultiValueMap post_data;
	private ErrorHandler error_handler;
	private Context context;
	
	public RESTClient(RESTResponder responder){
		
		this.responder = responder;
		service_url = new URLCollection();
		url = ""; // will get assigned later
		setTransferType(URLCollection.GET);
		error_handler = new ErrorHandler();

	}
	
	public RESTClient(RESTResponder responder, MultiValueMap post_data){
		
		this.post_data = post_data;
		this.responder = responder;
		service_url = new URLCollection();
		url = ""; // will get assigned later
		setTransferType(URLCollection.POST);
		error_handler = new ErrorHandler();

	}
	
	
	protected void onPreExecute (){
		// show some sort of progress window here. 
	}
	
	private void setTransferType(int type){
		if (type==URLCollection.POST){
			transfer_type=URLCollection.POST;
		} else {
			transfer_type = URLCollection.GET;
		}
	}
	
	
	protected String doInBackground(String... service_urls) {
		
		
		//SimpleClientHttpRequestFactory s = new SimpleClientHttpRequestFactory() {
			
		//	@SuppressWarnings("unused")
		//	Map<String,List<String>> requestProps;
			
        //    
        //    protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
        //        super.prepareConnection(connection, httpMethod);
                
				//requestProps = connection.getRequestProperties();
                
                //String authorisation = URLCollection.API_USERNAME + ":" + URLCollection.API_PASSWORD;
                //byte[] encodedAuthorisation = Base64.encode(authorisation.getBytes(), Base64.DEFAULT);
				//connection.setRequestProperty("Authorization", "Basic " + new String(encodedAuthorisation));
                //requestProps = connection.getRequestProperties();
                
                
                
        //    }
        //};	
		
		String result = "";
		//android.os.Debug.waitForDebugger();
		//url = service_url.getServiceURL(service_urls[0]);
		url = service_urls[0];
		// TODO make this dynamic
		//url= "https://www.rocketlinkmobile.com/portal/api10/login";
		//if(Globals.DEBUG) Log.v(TAG, "doInBackground url "+url);
		
		RestTemplate restTemplate = new RestTemplate();
		
		if(transfer_type==URLCollection.POST){
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
			messageConverters.add(new FormHttpMessageConverter());
			messageConverters.add(new StringHttpMessageConverter());
			restTemplate.setMessageConverters(messageConverters);

			restTemplate.setErrorHandler(error_handler);
			result = restTemplate.postForObject(url, post_data, String.class);
			
		} else {
			restTemplate.setErrorHandler(error_handler);
			result = restTemplate.getForObject(url, String.class);
			
		}
		
		//if(Globals.DEBUG) Log.v(TAG, "doInBackground result "+result);
		return result;
	}
	
	protected void onProgressUpdate(Integer... progress) {
        // you could show something here with internet progress or whatever
    }

    protected void onPostExecute(String result) {
    	
    	if(Globals.DEBUG) Log.v(TAG, "onPostExecute result "+result);
    	responder.handleRESTResponse(result);
	}
    
    private class ErrorHandler extends DefaultResponseErrorHandler {
    	
    	
    	public boolean hasError(ClientHttpResponse response){
    		
    		String error_msg = "";
    		
    		try {
				super.hasError(response);
				error_msg = response.getStatusText()+" - ";
				error_msg += response.getStatusCode().value();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if(Globals.DEBUG) Log.e(TAG, "ErrorHandler called, service url "+url+", with response "+error_msg);
			}
			
    		try {
    			InetAddress.getByName("google.ca").isReachable(3);
    			if(Globals.DEBUG) Log.e(TAG, "You are online!");
    			} catch (UnknownHostException e){
    				e.printStackTrace();
    				if(Globals.DEBUG) Log.e(TAG, "You are not online!"+ e.getMessage());
    			} catch (IOException e){
    				e.printStackTrace();
    				if(Globals.DEBUG) Log.e(TAG, "You are not online!"+ e.getMessage());
    			}
    		return false;
    	
    	}
    	
    	
    }
  //Fucntion to check reachability 
  	public  boolean isOnline() {
  	    //  First, check we have any sort of connectivity
  	    final ConnectivityManager connMgr = (ConnectivityManager) VorTextApp.getApp().getSystemService(Context.CONNECTIVITY_SERVICE);
  	    TelephonyManager lTelephonyManager = (TelephonyManager) VorTextApp.getApp().getSystemService(Context.TELEPHONY_SERVICE);
  	    final NetworkInfo netInfo = connMgr.getActiveNetworkInfo();

  	    if (netInfo != null && netInfo.isConnected() && lTelephonyManager.getCallState()==TelephonyManager.CALL_STATE_IDLE) {
  	        //  Some sort of connection is open, check if server is reachable
  	    	int type = lTelephonyManager.getNetworkType();
  	    	int data_state = lTelephonyManager.getDataState();
  	    	
  	    	if  (lTelephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED) {
  	    			 			return true;
  	    			 		    }
//  	        try {
//  	            URL url = new URL(service_url.getServiceURL(""));
//  	            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
//  	            urlc.setRequestProperty("User-Agent", "Android Application");
//  	            urlc.setRequestProperty("Connection", "close");
//  	            urlc.setConnectTimeout(15 * 1000); // Thirty seconds timeout in milliseconds
//  	            try {
//  	            urlc.connect();
//  	            } catch (IOException e)
//  	            {
//  	            	if(Globals.DEBUG) Log.e(TAG, e.getMessage());
//  		            return false;
//  	            }
//  	            
//  	            if (urlc.getResponseCode() == 200 &  urlc.getContentLength()!= -1) { // Good response
//  	                return true;
//  	            } else { // Anything else is unwanted (login page, unreachable server...)
//  	                return false;
//  	            }
//  	        } catch (IOException e) {
//  	        	if(Globals.DEBUG) Log.e(TAG, e.getMessage());
//  	            return false;
//  	        }
  	    	
  	    	
  	       
  	    }
  	  return netInfo != null && netInfo.isConnected();	
  	}
}


