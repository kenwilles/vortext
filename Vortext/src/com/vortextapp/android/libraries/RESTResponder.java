package com.vortextapp.android.libraries;

public interface RESTResponder {
	public void handleRESTResponse(String response);
}
