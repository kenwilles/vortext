package com.vortextapp.android.libraries;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.vortextapp.android.config.Globals;

import android.util.Log;

//import com.vortextapp.android.config.Globals;

public class TimeManager {
	
	private static final String TAG = "VorText: libraries.TimeManager"; // for debug label
	
	public TimeManager(){
		
	}
	
	public static long getCurrentSyncronizedTimeStamp(){
		long current_time = System.currentTimeMillis();
		//if(Globals.DEBUG) Log.v(TAG, "returning getSyncronizedTimeStamp "+current_time);
		return current_time;
		
	}
	
	/**
	 * Converts java current time to seconds for PHP server
	 * @return time since epoch in seconds not milliseconds. PHP does not like to work with milliseconds, just seconds. 
	 */
	public static long getTimeStampForServer(){
		long current_time = (long) Math.round(System.currentTimeMillis()*.001);
		//if(Globals.DEBUG) Log.v(TAG, "returning getTimeStampForServer "+current_time);
		return current_time;
	}
	
	/**
	 * Converts java current time to seconds for PHP server
	 * @return time since epoch in seconds not milliseconds. PHP does not like to work with milliseconds, just seconds. 
	 */
	public static long getTimeStampForServer(Long time_ago){
		long current_time = (long) Math.round(time_ago*.001);
		//if(Globals.DEBUG) Log.v(TAG, "returning getTimeStampForServer "+current_time);
		return current_time;
	}
	
	/**
	 * Converts seconds into milliseconds. 
	 * @param s this incoming string should be in seconds and will get converted 
	 * @return converted seconds into milliseconds
	 */
	public static long convertServerTimeToTimestamp(String s){
		
		/*SimpleDateFormat date_formatter = new SimpleDateFormat();
		long server_time = 0;
		try {
			Date timestamp = date_formatter.parse(s);
			server_time = timestamp.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		long milliseconds_since_epoch = Long.parseLong(s)*1000; // convert incoming timestamp seconds to milliseconds
		Date server_time = new Date(milliseconds_since_epoch);
		return server_time.getTime(); 
	}
	
	public static float getHourDifference(long milliseconds1, long milliseconds2){
		
		float diff = milliseconds1-milliseconds2;
		diff = diff / (1000 * 60 * 60);
		
		return diff;
		
	}
	
	public static float getOneHourAgo(){
		long current_time = System.currentTimeMillis() - (1000 * 60 * 60);
		if(Globals.DEBUG) Log.v(TAG, "current timestamp "+TimeManager.getCurrentSyncronizedTimeStamp()+", One hour ago timestamp: "+current_time);
		return current_time;
	}
	
	public static int getCurrentDay(){
		
		Calendar calendar = Calendar.getInstance();
		int weekday = calendar.get(Calendar.DAY_OF_WEEK);
		if(Globals.DEBUG) Log.v(TAG, "day of week integer is "+weekday);
		return weekday;

	}
	
}
