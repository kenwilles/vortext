/**
 * retrieves contacts from phone using native content provider
 */

package providers;

import com.vortextapp.android.config.Globals;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;

public class ContactsOnPhone {
	
	private static final String TAG = "VorText: providers.ContactsOnPhone"; // for debug label
	
	private ContentResolver resolver;
	
	public ContactsOnPhone(ContentResolver r){
		//
		resolver = r;
	}
	
	/*
	// not ready for this method and depreciated stuff needs upgrading. 
	public String getPersonById(String id) {
		
		Uri uri = Uri.parse("content://com.android.contacts/contacts");
		uri = Uri.withAppendedPath(uri, id);
		 
		String projection[] = new String[] { PeopleColumns.DISPLAY_NAME };
	    Cursor cursor = resolver.query(uri, projection,null, null, null);

	    if (cursor != null) {
	      try {
	        if (cursor.getCount() > 0) {
	          cursor.moveToFirst();
	          String name = cursor.getString(0);
	          if(Globals.DEBUG)  Log.v(TAG, "Contact Display Name: " + name);
	          return name;
	        }
	      } finally {
	        cursor.close();
	      }
	    }
	    
	    return "<Name>";

	  }
	*/

	public String getContactName(String number) {

		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
		String cols[] = new String[] { PhoneLookup.DISPLAY_NAME };
		String where = PhoneLookup.NUMBER + "=" + number;
		Cursor cursor = resolver.query(uri, cols, where, null, null);

		String name = null;

		if (cursor != null) {
			try {
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					name = cursor.getString(cursor.getColumnIndexOrThrow(PhoneLookup.DISPLAY_NAME));
				}
			} catch(Error e){
				if(Globals.DEBUG) Log.e(TAG, "could not get contact name, error: "+e);
			}
			finally {
				cursor.close();
			}
		}
		
		if(name==null){
			name = number;
			return name;
		} else {
			// return whatever was matched
			return name;
		}

	}
}
