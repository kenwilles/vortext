package providers;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.vortextapp.android.config.Globals;
import com.vortextapp.android.config.SMS;
import com.vortextapp.android.models.SampleAds;

/**
 * 
 * 
 * @author Ken Willes
 *
 */
public class SmsDBConnector {
	
	private static final String TAG = "VorText: providers.SmsDBConnector"; // for debug label
	
	public ArrayList<SmsMmsMessage> list = new ArrayList<SmsMmsMessage>();
	private String type;
	private long thread_id;
	private ContactsOnPhone contacts;
	private ContentResolver resolver;
	
	//	public SmsDB(){
	//		
	//	}
	
	public SmsDBConnector(ContentResolver resolver, String type){
		init(resolver, type, -1);
	}
	
	public SmsDBConnector(ContentResolver resolver, String type, long thread_id){
		init(resolver, type, thread_id);
	}
	
	private void init(ContentResolver resolver, String type, long thread_id){
		
		this.resolver = resolver;
		contacts = new ContactsOnPhone(this.resolver);
		
		this.type = type;
		
		if(thread_id>=0){
			this.thread_id = thread_id;
		} else {
			this.thread_id = -1;
		}
		
	}

	/**
	 * Sets message thread list for message thread overview or single thread
	 * 
	 */
	public void setMessages() {
		
		long messageId;
	    long threadId;
	    String address;
	    String from;
	    String to;
	    String contact_name;
	    long timestamp;
	    String body;
	    SmsMmsMessage message;
		
		String[] projection = null;
		Uri uri = null;
		String where = null;
		String where_args[] = null;
		String sort = "date DESC";
		
		if(Globals.DEBUG) Log.v(TAG, "SMS.THREAD: "+SMS.THREAD+" type="+type);
		
		if(type==SMS.THREAD){
			projection = new String[]{"_id", "thread_id",
				"address", "person", "date", "body"};
			sort = "date ASC";
		    uri = SMS.CONVERSATION_CONTENT_URI;
			uri = Uri.withAppendedPath(uri, String.valueOf(thread_id));
			if(Globals.DEBUG) Log.v(TAG, "content Uri: "+uri);
			
		} else {
			//must be SMS.THREADS
			projection = new String[]{"*"};
			uri = SMS.CONVERSATION_CONTENT_URI;
			if(Globals.DEBUG) Log.v(TAG, "content Uri: "+uri);
		}		

		Cursor cursor = resolver.query(uri, projection, where, where_args, sort);
		String[] cols = cursor.getColumnNames();		

	    if (cursor != null) {
	        try {
	            int count = cursor.getCount();
	            if (count > 0) {
	                while (cursor.moveToNext()) {
	                	
	                	for(String one : cols){
	                		if(Globals.DEBUG) Log.v(TAG, "col "+one+", val "+cursor.getString(cursor.getColumnIndexOrThrow(one)));
	            		}
	                	if(Globals.DEBUG) Log.v(TAG, "==========");
	                	
	                    messageId = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
	                    threadId = cursor.getLong(cursor.getColumnIndexOrThrow("thread_id"));
	                    address = cursor.getString(cursor.getColumnIndexOrThrow("address"));
	                    
	                    to = ""; //cursor.getString(cursor.getColumnIndexOrThrow("to"));
	                    
	                    from = cursor.getString(cursor.getColumnIndexOrThrow("person")); //contacts.getPersonById(String.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("person"))));
	                    if(from==null){
	                    	from = "Me";
	                    }
	                    
	                    contact_name = contacts.getContactName(cursor.getString(cursor.getColumnIndexOrThrow("address")));
	                    
	                    timestamp = cursor.getLong(cursor.getColumnIndexOrThrow("date"));
	                    if(!from.equals("Me")){
	                    	body = cursor.getString(cursor.getColumnIndexOrThrow("body"))+"\n\n"+SampleAds.getRandom();
	                    } else {
	                    	body = cursor.getString(cursor.getColumnIndexOrThrow("body"));
	                    }
	                    message = new SmsMmsMessage(messageId, threadId, address, from, to, contact_name, timestamp, body, SMS.MESSAGE_TYPE_SMS);
	                    message.setNotify(false);
	                    list.add(message);

	                }
	            }
	        } finally {
	            cursor.close();
	        }
	    }
	    
	}
	
}
