package providers;

public class SmsMmsMessage {
	
	public final Long message_id; 
    public final Long thread_id;
    public final String address;
    public final String from;
    public final String to;
    public final String contact_name;
    public final Long timestamp;
    public final String body;
	
	public SmsMmsMessage(Long message_id, Long thread_id, String address, String from, String to, String contact_name, Long timestamp, String body, int message_type){
		this.message_id = message_id;
		this.thread_id = thread_id;
		this.address = address;
		this.from = from;
		this.to = to;
		this.timestamp = timestamp;
		this.body = body;
		this.contact_name = contact_name;
	}

	public void setNotify(boolean b) {
		// TODO Auto-generated method stub
		
	}
	
	
	public String toString(){
		return getClass().getName() + "[" +
        "message_id=" + message_id + ", " +
        "thread_id=" + thread_id + ", " +
        "address=" + address + ", " +
        "from=" + from + ", " +
        "to=" + to + ", " +
        "timestamp=" + timestamp + ", " +
        "body=" + body + ", " + "]";
		//return this.body;
	}
}
